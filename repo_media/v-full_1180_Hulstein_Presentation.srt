1
00:00:00,800 --> 00:00:06,080
Hello everyone, my name is Vanessa 
Peña-Araya and she is Anastasia Bezerianos,  

2
00:00:06,080 --> 00:00:08,720
and we are going to present Geo-Storylines.

3
00:00:10,720 --> 00:00:16,400
Storylines are a compact way to visualize how the 
relationships between people evolve over time.

4
00:00:17,040 --> 00:00:22,080
In this Storylines of the Lord of the Rings we 
can see four lines that represent the hobbits.

5
00:00:22,640 --> 00:00:27,680
They start out together but later in the 
story they split up so their lines diverge.

6
00:00:29,360 --> 00:00:32,720
Both in real-world datasets and in stories  

7
00:00:32,720 --> 00:00:36,080
relationships are usually 
linked to one or more locations.

8
00:00:36,720 --> 00:00:39,520
However, designs that combine Storylines with  

9
00:00:41,200 --> 00:00:44,960
geographical space mostly use 
abstract representation of places,  

10
00:00:44,960 --> 00:00:49,200
so we cannot know what is the geographical 
relationship between the places involved.

11
00:00:50,960 --> 00:00:55,600
For example, in the Lord of the Rings the 
locations are grey counters with names.

12
00:00:56,320 --> 00:01:02,400
But if we look at the map we can now see 
more information: the hobbits travel in  

13
00:01:02,400 --> 00:01:06,720
completely different directions which 
give us a better idea of their journey.

14
00:01:07,760 --> 00:01:14,000
Our goal with Geo-Storylines is to combine maps 
and stories to show the dynamics of entities'  

15
00:01:14,640 --> 00:01:17,120
relationships in their geospatial context

16
00:01:19,200 --> 00:01:24,000
The design process of Geo-Storylines started 
with a series of generative workshops  

17
00:01:24,000 --> 00:01:29,040
in which participants created hand-draw 
visualizations of relationships, time and space.

18
00:01:29,760 --> 00:01:32,640
Through bottom-up card-sorting of these sketches,  

19
00:01:32,640 --> 00:01:37,920
we identified six clusters capable 
of incorporating maps and Storylines.

20
00:01:39,760 --> 00:01:43,840
We organized these sketches in a 
design space along three dimensions.

21
00:01:44,960 --> 00:01:49,440
First, how to represent the space: 
do they use one map or several.

22
00:01:50,880 --> 00:01:56,320
Second, how they represent time. Similar than 
before we can have one timeline or several.

23
00:01:57,680 --> 00:02:01,600
And third, how these two components 
are integrated in the visualization.

24
00:02:03,600 --> 00:02:08,400
We then analyzed these designs in their 
legibility and their ability to scale well  

25
00:02:08,400 --> 00:02:12,560
to large datasets and extracted 
the three most promising designs.

26
00:02:13,120 --> 00:02:15,680
Anastasia will give you more 
details about each of them.

27
00:02:17,760 --> 00:02:19,200
So these are the three designs.

28
00:02:19,200 --> 00:02:21,040
Let's look at them one by one.

29
00:02:21,040 --> 00:02:22,960
The first one is Coordinated Views.

30
00:02:23,520 --> 00:02:26,880
This design places the overall 
Storyline on the right,  

31
00:02:26,880 --> 00:02:29,840
and then we have the map 
and these are side by side.

32
00:02:30,480 --> 00:02:34,480
The highlighted locations that are 
seen here, in orange in the map  

33
00:02:34,480 --> 00:02:40,800
are shown for the specific relationship that 
is closest to the left edge of the Storyline.

34
00:02:40,800 --> 00:02:43,120
But let's look this a bit in action.

35
00:02:44,720 --> 00:02:48,640
As the user scrolls, the highlighted 
locations are dynamically updated and  

36
00:02:48,640 --> 00:02:53,360
linked with a line to the relationship 
that is closest to the timeline.

37
00:02:54,160 --> 00:02:57,680
Here we can see a few people getting 
selected and the Storyline and the  

38
00:02:57,680 --> 00:03:03,840
locations of the relationships 
get highlighted and filtered out.

39
00:03:06,560 --> 00:03:08,240
The second design is Map Glyphs.

40
00:03:09,040 --> 00:03:13,760
Here, we have one Storyline that 
takes up the entire space, and we have  

41
00:03:13,760 --> 00:03:18,000
one map glyph per relationship 
internally in the timeline.

42
00:03:18,000 --> 00:03:22,720
The locations associated with that relationship 
are highlighted in the mini maps with orange.

43
00:03:24,000 --> 00:03:29,280
So let's see an implementation we scroll to 
see the entire story line with the map leaves

44
00:03:31,440 --> 00:03:33,840
And here we start selecting people and we see that  

45
00:03:33,840 --> 00:03:37,840
all relationships and maps that do 
not involve them start to fade out.

46
00:03:40,800 --> 00:03:42,800
Finally, the third design is Time Glyphs.

47
00:03:43,440 --> 00:03:46,640
In this design, we have on the 
right several storyline glyphs,  

48
00:03:46,640 --> 00:03:52,480
one per location, that are linked with gray 
lines to the corresponding locations on the map.

49
00:03:52,480 --> 00:03:57,600
So each individual storyline glyph only shows 
the people and relationships related to that  

50
00:03:57,600 --> 00:04:03,360
one location let's see it now in action we 
can scroll vertically to see all storylines.

51
00:04:07,920 --> 00:04:11,280
And we can select a location to 
bring their storyline into focus  

52
00:04:11,920 --> 00:04:15,920
if we select a person we see this 
person highlighted in all storylines.

53
00:04:20,000 --> 00:04:23,120
To evaluate these three designs, 
we conducted two evaluations.

54
00:04:23,680 --> 00:04:26,640
First we ran a user study with 18 participants,  

55
00:04:26,640 --> 00:04:30,080
using different geo'temporal patterns 
that participants had to identify.

56
00:04:30,720 --> 00:04:32,480
And we complemented these results with  

57
00:04:32,480 --> 00:04:36,320
two feedback sessions with domain 
experts and real-world datasets.

58
00:04:37,600 --> 00:04:41,680
In our user study, we compared the 
three designs using five tasks.

59
00:04:41,680 --> 00:04:45,040
Two were baseline tasks of 
identifying location and dates,  

60
00:04:45,040 --> 00:04:48,480
and three were tasks that 
focus on geo-spatial patterns.

61
00:04:48,480 --> 00:04:53,200
For example, the distance of relationships, 
direction of movement, adjacency of movement, etc.

62
00:04:54,000 --> 00:04:56,160
But let's look at one task in detail.

63
00:04:56,800 --> 00:04:59,840
So here, we will see a direction task.

64
00:04:59,840 --> 00:05:02,560
Our participant is trying to select a few people  

65
00:05:02,560 --> 00:05:06,240
and is trying to identify the direction 
they followed in their relationship.

66
00:05:06,240 --> 00:05:09,840
So in this case, we have two people 
that move from north to south.

67
00:05:11,440 --> 00:05:13,520
Afterwards, after answering their question,  

68
00:05:13,520 --> 00:05:17,760
they had to to self-report their confidence 
and the perceived easiness of the task.

69
00:05:20,800 --> 00:05:24,480
We also conducted two feedback 
sessions with domain experts.

70
00:05:24,480 --> 00:05:30,160
First, with seven investigative journalism 
practitioners we got feedback about their  

71
00:05:30,160 --> 00:05:33,200
overall interest in combining storylines with maps  

72
00:05:33,200 --> 00:05:38,240
in order to analyze the movements and 
relationship of politicians for their articles.

73
00:05:38,240 --> 00:05:42,640
And then, in a second workshop, with three 
practitioners we explored their own data  

74
00:05:42,640 --> 00:05:46,080
using the three final Geo-Storylines 
designs that we just discussed.

75
00:05:46,720 --> 00:05:50,960
They used their own datasets that contained 
relationships extracted from news articles.

76
00:05:50,960 --> 00:05:55,840
These were big datasets with thousands 
of locations and a few hundred people.

77
00:05:56,480 --> 00:06:00,080
And they tried to identify a 
famous local politician who,  

78
00:06:00,080 --> 00:06:03,840
over time, changed the region they 
represented in local elections.

79
00:06:04,400 --> 00:06:08,000
So now Vanessa will summarize our 
results across both evaluations.

80
00:06:11,040 --> 00:06:14,960
So, when analyzing our data 
collectively we found that:

81
00:06:14,960 --> 00:06:20,880
Overall, Coordinated Views were more efficient 
and preferred by participants of our user study.

82
00:06:21,440 --> 00:06:25,440
We believe that scrolling through this 
design was similar to an animation  

83
00:06:25,440 --> 00:06:30,880
which explained why it was easier to see the 
geo-temporal patterns of direction and adjacency.

84
00:06:31,520 --> 00:06:34,240
Our domain experts confirmed this intuition,  

85
00:06:34,240 --> 00:06:38,400
and asked for a play button to 
have automatic scrolling/playback.

86
00:06:39,840 --> 00:06:45,600
A surprising finding was that, although 
Map Glyphs had small resolution maps,  

87
00:06:45,600 --> 00:06:50,800
there were not errors by participants of our 
user study when searching locations in them.

88
00:06:51,440 --> 00:06:56,080
However, the real-world datasets used 
by our domain experts contain maps with  

89
00:06:56,080 --> 00:07:01,840
more than 1000 locations which made 
this design extremely hard to use.

90
00:07:01,840 --> 00:07:05,760
Finally, we found that Time Glyphs 
performed poorly in our user study.

91
00:07:06,320 --> 00:07:11,520
In particular, for tasks that focus on 
the storylines in a particular location,  

92
00:07:11,520 --> 00:07:15,840
Time Glyphs should have had an advantage 
as they split the data per location.

93
00:07:16,400 --> 00:07:19,120
However, even here they were not the clear winner.

94
00:07:19,760 --> 00:07:25,440
We observed, however, an opposite reaction 
from our domain experts: they appreciated  

95
00:07:25,440 --> 00:07:30,720
the Time Glyphs design because it allowed them 
to focus on the story of a specific location,  

96
00:07:30,720 --> 00:07:34,880
as they usually do when working 
with local news article.

97
00:07:36,400 --> 00:07:41,840
In particular, they mentioned that Coordinated 
Views and Time Glyphs are complementary designs.

98
00:07:43,600 --> 00:07:49,440
So, in summary, we presented the design space 
of Geo-Storylines aimed to integrate maps into  

99
00:07:49,440 --> 00:07:53,040
Storyline visualizations, derived 
from a series of design workshops.

100
00:07:53,920 --> 00:07:59,040
We also presented the details of the evaluation of 
the three most promising designs visualizations,  

101
00:07:59,600 --> 00:08:04,880
both in a controlled user study and in feedback 
sessions with domain experts from data journalism.

102
00:08:06,080 --> 00:08:08,400
You can find our implementations code,  

103
00:08:08,400 --> 00:08:13,360
data collected, and analysis scripts as 
well as the videos in the link below.

104
00:08:13,360 --> 00:08:14,720
Thank you for your attention.

105
00:08:14,720 --> 00:08:17,920
We will happy to answer any questions now.


// Narrative Charts
// ================
//
// `d3.layout.narrative()`
//
// The constructor takes no arguements. All relevant object properties should
// be set using the setter functions.
d3.narrative = function () {

  // Import jLouvain
  //import 'vendor/jLouvain/jLouvain.js';
  // ---------------
  // [jLouvain](https://github.com/upphiminn/jLouvain) is a open source (MIT)
  // javascript implementation of the jLouvain method of
  // [community detection](https://www.wikiwand.com/en/Louvain_Modularity).

  // Define all the variables.
  var narrative,
    scenes, characters, introductions, links,
    size, orientation, pathSpace, scale,
    labelSize, labelPosition, groupMargin, scenePadding,
    groups;

  var masks;

  var essai = 0;
  // Set some defaults.
  size = [1, 1];
  scale = 1;
  pathSpace = 10;
  labelSize = [100, 15];
  labelPosition = 'right';
  scenePadding = [0, 0, 0, 0];
  groupMargin = 0;
  orientation = 'horizontal';

  // TODO: MAKE A FUNCTION THAT UPDATES THIS VALUE
  sceneWidth = 350
  sceneHeight = 350
  //var nested = false

  // Public functions (the API)
  // ==========================
  // The narrative object which is returned and exposes the public API.
  narrative = {};


  // ILDA
  /*narrative.nested = function (value){
    nested = value
  }*/


  // Scenes
  // ------
  //
  // `narrative.scenes([array])`
  //
  // Set or get the scenes array. If an array is passed, sets the narrative's
  // scenes to the passed array, else returns the scenes array.
  narrative.scenes = function (_) {
    if (!arguments.length) {
      return scenes;
    }
    scenes = _;
    return narrative;
  };

  // Characters
  // ----------
  //
  // `narrative.characters([array])`
  //
  // Set or get the characters array. If an array is passed, sets the
  // narrative's characters array, otherwise returns the characters array.
  narrative.characters = function (_) {
    if (!arguments.length) {
      return characters;
    }
    characters = _;
    return narrative;
  };

  // Size
  // ----
  //
  // `narrative.size([array])`
  //
  // Set or get the size of the layout. A two element array `[width,height]`. Note
  // that this is considered a guide for the layout algorithm.
  // See `narrative.extent()` for getting the final size of the layout.
  narrative.size = function (_) {
    if (!arguments.length) {
      return size;
    }
    size = _;
    return narrative;
  };

  // Orientation
  // -----------
  //
  // `narrative.orientation([orientation])`
  //
  // *Incomplete:* Only the default (horizontal) option is fully supported.
  //
  // Set the orientation to use for the layout. The choices are `'horizontal'` (default)
  // or `'vertical'`. In a horizontal orientation 'time' runs from left to right
  // and in vertical, top to bottom.
  narrative.orientation = function (_) {
    if (!arguments.length) {
      return orientation;
    }
    orientation = _;
    return narrative;
  };

  // Extent
  // ------
  //
  // `narrative.extent()`
  //
  // Get the extent of the space used by the layout. This is useful for adjusting
  // the size of the containing element after the layout has been calculated.
  //
  // Despite being able to set the size (see `narrative.size()`), it's not always
  // possible to contain the chart in the available space. This function will
  // provide a `[width,height]` array of the layout extent *after* the layout has
  // run.
  narrative.extent = function () {
    return scenes.concat(introductions).reduce(function (max, d) {
      var bounds = d.bounds();
      if (bounds[1][1] > max[1]) {
        max[1] = bounds[1][1];
      }
      if (bounds[1][0] > max[0]) {
        max[0] = bounds[1][0];
      }
      return max;
    }, [0, 0]);
  };

  // Path space
  // ----------
  //
  // `narrative.pathSpace([number])`
  //
  // Set or get the space available to each character's path.
  narrative.pathSpace = function (_) {
    if (!arguments.length) {
      return pathSpace;
    }
    pathSpace = _;
    return narrative;
  };

  // Group margin
  // ------------
  //
  // `narrative.groupMargin([margin])`
  //
  // The characters are divided into groups based on the strength of their relationships
  // (i.e. co-appearances in scenes). These groups are then arranged in a way designed
  // to reduce congestion in the centre of the chart. To give thelayout a more open
  // feel, a group margin can be set.
  narrative.groupMargin = function (_) {
    if (!arguments.length) {
      return groupMargin;
    }
    groupMargin = _;
    return narrative;
  };

  // Scene padding
  // -------------
  //
  // `narrative.scenePadding([array])`
  //
  // By default scenes have a height equal to `character height × character count`
  // and a width of zero. You may want to allow for extra space around scenes so
  // collisions with labels can be avoided. To set a padding pass an array of values
  // matching the CSS padding argument order `[top, right, bottom, left]`.
  narrative.scenePadding = function (_) {
    if (!arguments.length) {
      return scenePadding;
    }
    scenePadding = _;
    return narrative;
  };

  // Label size
  // ----------
  //
  // `narrative.labelSize([array])`
  //
  // Set or get the default space to allocate in the layout for character labels.
  // Must be a two element array `[width,height]`. Label sizes specific to each
  // character which will override these defaults can be set by defining `height`
  // and `width` properties on individual character objects.
  narrative.labelSize = function (_) {
    if (!arguments.length) {
      return labelSize;
    }
    labelSize = _;
    return narrative;
  };

  // Label position
  // --------------
  //
  // `narrative.labelPosition([string])`
  //
  // Set or get the default label position for character labels. Valid options are
  // `above`, `below`, `left`, `right`. This can be overridden by setting defining
  // a `labelPosition` property on individual character objects.
  narrative.labelPosition = function (_) {
    if (!arguments.length) {
      return labelPosition;
    }
    labelPosition = _;
    return narrative;
  };

  // Links
  // -----
  //
  // `narrative.links()`
  //
  // Returns an array of links. Each link is consecutive appearances for a given
  // character. Links are an object with `source` and `target` properties which
  // are both appearance objects.
  narrative.links = function () {
    return links;
  };

  // Link
  // ----
  //
  // `narrative.link()`
  //
  // Returns a function for generating path strings for links.
  // Links are objects with `source` and `target` properties which each contain
  // an `x` and `y` property. In the context of the narrative chart these are
  // either character apperance or introduction nodes.
  narrative.link = function (xTranslate = 0, yTranslate = 0) {
    var curvature = 0.5;

    // ### Link path
    //
    // `link([object])`
    //
    // This function should be used to set the `path` attribute of links when
    // displaying the narrative chart. It accepts an object and returns a path
    // string linking the two.
    function link(d) {

      var x0, x1, y0, y1, cx0, cy0, cx1, cy1, ci;

      // Set path end positions.
      x0 = (d.source.scene) ? d.source.scene.x + d.source.x : d.source.x;
      y0 = (d.source.scene) ? d.source.scene.y + d.source.y : d.source.y;
      x1 = (d.target.scene) ? d.target.scene.x + d.target.x : d.target.x;
      y1 = (d.target.scene) ? d.target.scene.y + d.target.y : d.target.y;


      x0 += xTranslate
      if(d.source.scene){
        x0 += 325 + 70 - 5 // + 50
      }
      if(d.target.scene){
        x1 += 50
      }
      y0 += yTranslate

      x1 += xTranslate
      y1 += yTranslate

      /*if (d.source.scene) {
        x0 += 6
      }
      x1 -= 13*/

      // Set control points.
      if (orientation === 'vertical') {
        ci = d3.interpolateNumber(y0, y1);
        cx0 = x0;
        cy0 = ci(curvature);
        cx1 = x1;
        cy1 = ci(1 - curvature);
      } else {
        ci = d3.interpolateNumber(x0, x1);
        cx0 = ci(curvature);
        cy0 = y0;
        cx1 = ci(1 - curvature);
        cy1 = y1;
      }

      return "M" + x0 + "," + y0 +
        "C" + cx0 + "," + cy0 +
        " " + cx1 + "," + cy1 +
        " " + x1 + "," + y1;
    }

    // ### Curvature
    //
    // `link.curvature([number])`
    //
    // Set or get the curvature which should be used to generate links. Should be
    // in the range zero to one.
    link.curvature = function (_) {
      if (!arguments.length) {
        return curvature;
      }
      curvature = _;
      return link;
    };


    return link;
  };


  // Introductions
  // -------------
  //
  // `narrative.introductions()`
  //
  // Get an array of character introductions for plotting on the graph. Introductions
  // are nodes (usually with labels) displayed before the first scene in which each
  // character appears.
  narrative.introductions = function () {
    return introductions;
  };

  narrative.masks = function (_) {
    if (!arguments.length) {
      return masks;
    }
    masks = _;
    return narrative;
  };

	/*narrative.createLinks = function(){
		createLinks();
	}*/

  // Layout
  // ------
  //
  // `narrative.layout()`
  //
  // Compute the narrative layout. This should be called after all options and
  // data have been set and before attempting to use the layout's output for
  // display purposes.
  narrative.layout = function (data) {
    masks = [];

    computeSceneCharacters();
    computeCharacterGroups();
    setSceneGroups();
    computeGroupAppearances();
    sortGroups();
    computeGroupPositions();
    computeCharacterGroupPositions();
    //recomputeCharacterOrder(); // my added function
    sortGroupAppearances();

    computeSceneTiming();
    computeAppearancePositions();
    computeScenePositions();

    //var len = introductions.length;
    if (!introductions || !introductions.length > 0) {
      createIntroductionNodes();
      computeIntroductionPositions();
    }

    createLinks();

    return narrative;
  };

  narrative.unfoldOriginLayout = function (openscene, foldingWidth, foldingHeight) {

    if (foldingHeight < 0) {
      openscene.y = openscene.y + foldingHeight;
    }

    for (var i = 0; i < scenes.length; i++) {
      var scene = scenes[i];
      if (scene.id != openscene.id && scene.x > openscene.x) {
        scene.x = scene.x + foldingWidth;
      }
      if (scene.id != openscene.id && scene.y >= openscene.y) {
        scene.y = scene.y + foldingHeight;
      }
    }

    introductions.forEach(function (intro) {
      if (intro.x > openscene.x) {
        intro.x = intro.x + foldingWidth;
      }
      if (intro.y > openscene.y + 25) {
        intro.y = intro.y + foldingHeight;
      }
    })
    if (foldingHeight > 0) {
      openscene.y = openscene.y + foldingHeight;
    }

    return narrative;

  }

  narrative.translateAll = function (newNarrative) {
    var newScenes = newNarrative.scenes();
    var newScenesDict = {}
    for (var index in newScenes) {
      var aScene = newScenes[index]
      newScenesDict[aScene.id] = aScene
    }
    for (var i = 0; i < scenes.length; i++) {
      var aScene = scenes[i];
      if (aScene.id in newScenesDict) {
        var newScene = newScenesDict[aScene.id]
        aScene.x = newScene.x
        aScene.y = newScene.y
      } else {
        aScene.x = -2000
        aScene.y = -2000
      }
    }

    var newIntros = newNarrative.introductions();
    var newIntrosDict = {}
    for (var index in newIntros) {
      var anIntro = newIntros[index]
      newIntrosDict[anIntro.character.id] = anIntro
    }

    for (var i = 0; i < introductions.length; i++) {
      var anIntro = introductions[i];
      if (anIntro.character.id in newIntrosDict) {
        var newIntro = newIntrosDict[anIntro.character.id]
        anIntro.x = newIntro.x
        anIntro.y = newIntro.y
      } else {
        anIntro.x = -2000
        anIntro.y = -2000
      }

    }
    // INTROS
    return narrative
  }
  narrative.getGreatestYValue = function () {
    var maxY = 0
    for (var i = 0; i < scenes.length; i++) {
      var aScene = scenes[i]
      maxY = Math.max(maxY, aScene.y)
    }
    for (var i = 0; i < introductions.length; i++) {
      var anIntro = introductions[i]
      maxY = Math.max(maxY, anIntro.y)
    }
    return maxY
  }

  narrative.getScene = function (sceneId) {
    for (var i = 0; i < scenes.length; i++) {
      var aScene = scenes[i];
      if (aScene.id == sceneId) {
        return aScene
      }
    }
    return null
  }
  narrative.getSceneByEdgeId = function (edgeId) {
    for (var i = 0; i < scenes.length; i++) {
      var aScene = scenes[i];
      if (aScene.edgeId == edgeId) {
        return aScene
      }
    }
    return null
  }
  narrative.getSceneByOriginalEdgeId = function (edgeId) {
    const result = []
    for (var i = 0; i < scenes.length; i++) {
      var aScene = scenes[i];
      //console.log(aScene)
      var index = aScene.edge.original_edges_ids.indexOf(edgeId)
      if (index >= 0) {
        result.push(aScene)
      }
    }
    return result
  }
  narrative.getIntroduction = function (characterId) {
    for (var i = 0; i < introductions.length; i++) {
      var anIntro = introductions[i];
      if (anIntro.character.id == characterId)
        return anIntro
    }
    return null
  }

  function resolveCollisionsLinks(scene) {
    var collisionLinks;
    // Get the full list of items this introduction collides with
    var collidables = links;

    collisionLinks = collidesWithLinks(scene);

    moveForCollisionLinks(collisionLinks, scene, introductions);

  }
	/*	// Gets a list of all other nodes that this introduction collides with.
		function collidesWithScenes(scene) {
			var i, ii, collisions;
			var collidables = scenes;

			collisions = [];
			for (i = 0, ii = collidables.length; i < ii; i++) {
				if (scene !== collidables[i] && collides(scene.bounds(), collidables[i].bounds())) {
					collisions.push(collidables[i]);
				}
			}
			return (collisions.length) ? collisions : false;
		}*/

  function collidesWithLinks(scene) {
    var i, ii, collisions;
    var collidables = links;
    collisions = [];

    var introsToMove = [];
    var sceneBottom = scene.y + scene.height;

    for (i = 0, ii = introductions.length; i < ii; i++) {

      var introY = introductions[i].y;

			/*	var sceneBounds = [[scene.x, scene.y], [scene.x + scene.width, scene.y + scene.height]];
			 */
      if (introY < sceneBottom) {
        introsToMove.push(introductions[i]);

      }
    }

    var HeightToDisplace = sceneBottom - introsToMove[0];


    introsToMove.forEach(function (d) {
      d.y = d.y + HeightToDisplace;
    })

		/*for (i = 0, ii = collidables.length; i < ii; i++) {
			
			var sceneBounds = [[scene.x, scene.y], [scene.x + scene.width, scene.y + scene.height]];
			var linkBounds = getLinkBounds(collidables[i]);
			
			function getLinkBounds(l) {

				if (l.source.x) {
					return [[l.source.x, l.source.y], [l.target.x, l.target.y]];
				}
				else {
					return [[l.source.x, l.source.y], [l.target.scene.x, l.target.scene.y]];
				}

			}

			if (scene !== collidables[i] && collides(sceneBounds, linkBounds)) {
				collisions.push(collidables[i]);
			}

		}*/
    return (collisions.length) ? collisions : false;

  }
  // Return the public API.
  return narrative;

  // Private functions
  // =================

  // Initial data wrangling
  // Initial data wrangling
  // ----------------------
  //
  // Populate the scenes with characters from the characters array.
  // This method also cleanses the data to exclude characters which appear only once
  // and scenes with fewer than two characters.
  function computeSceneCharacters() {

    var appearances, finished;

    // Create a map of scenes to characters (i.e. appearances).
    appearances = [];
    scenes.forEach(function (scene) {
      scene.characters.forEach(function (character) {
        // TO FIX!
        if(character == undefined)
        return
        // If the character isn't an object assume it's an index from the characters array.
        character = (typeof character === 'object') ? character : characters[character];

        // Note forced character positions and sizes.
        character._x = character.x || false;
        character._y = character.y || false;
        character._width = character.width || false;
        character._height = character.height || false;

        // Add this appearance to the map.
        appearances.push({
          character: character,
          scene: scene
        });

        // Setup some properties on the character and scene that we'll need later.
        scene.appearances = [];
        scene.bounds = getSceneBounds;
        character.appearances = [];
      });

      // note forces scene positions.
      scene._x = scene.x || false;
      scene._y = scene.y || false;
    });

    // Recursively filter appearances so we ultimately only include characters
    // with more than a single appearance and scenes with more than a single
    // character.
    while (!finished) {
      finished = true;
      appearances = appearances.filter(filterAppearances);
    }

    // Filter appearances.
    //
    // TODO: this could probably be more efficient (maybe with an index https://gist.github.com/AshKyd/adc7fb024787bd543fc5)
    function filterAppearances(appearance) {
      var counts, keep;

      counts = appearances.reduce(function (c, a) {

        if (appearance.character === a.character) {
          c[0]++;
        }

        if (appearance.scene === a.scene) {
          c[1]++;
        }

        return c;

      }, [0, 0]);

      keep = counts[0] >= 1 && counts[1] >= 1;
      finished = finished && keep;

      return keep;
    }

    // Re-construct `characters` and `scenes` arrays with filtered appearances.
    characters = [];
    scenes = [];
    appearances.forEach(function (appearance) {

      // Cross reference scenes and characters based on appearances.
      appearance.scene.appearances.push(appearance);
      appearance.character.appearances.push(appearance);

      if (characters.indexOf(appearance.character) === -1) {
        characters.push(appearance.character);
      }

      if (scenes.indexOf(appearance.scene) === -1) {
        scenes.push(appearance.scene);
      }
    });
  }

  // Character clustering
  // --------------------
  //
  // 	Cluster characters based on their co-occurence in scenes
  function computeCharacterGroups() {

    var nodes, edges, clusters, partitioner, groupsMap, initGroups;

    // An array of character indexes.
    nodes = characters.map(function (d, i) {
      return i;
    });

    initGroups = characters.reduce(function (g, d, i) {
      if (d.initialgroup) {
        g[i] = +d.initialgroup;
      }
      return g;
    }, {});

    // Calculate the edges based on a character's involvement in scenes.
    edges = [];


    scenes.forEach(function (scene) {
      edges = edges.concat(sceneEdges(scene.appearances));
    });

    // Consolidate edges into a unique set of relationships with a weighting
    // based on how often they appear together.
    edges = edges.reduce(function (result, edge) {
      var resultEdge;

      resultEdge = result.filter(function (resultEdge) {
        return (resultEdge.target === edge[0] || resultEdge.target === edge[1]) &&
          (resultEdge.source === edge[0] || resultEdge.source === edge[1]);

      })[0] || {
          source: edge[0],
          target: edge[1],
          weight: 0
        };

      resultEdge.weight++;

      if (resultEdge.weight === 1) {
        result.push(resultEdge);
      }

      return result;
    }, []);
    // Generate the groups.

    partitioner = jLouvain().nodes(nodes).edges(edges);

    if (initGroups) {
      partitioner.partition_init(initGroups);
    }
    clusters = partitioner();

    // Put all characters in groups with bi-directional reference.
    groups = [];
    groupsMap = {};
    characters.forEach(function (character, i) {

      var groupId, group;
      groupId = clusters[i];
      group = groupsMap[groupId];
      if (!group) {
        group = {
          id: groupId,
          characters: []
        };
        groups.push(group);
        groupsMap[groupId] = group;
      }
      group.characters.push(character);
      character.group = group;
    });


    // Creates a single link between each pair of characters in a scene.
    function sceneEdges(list) {
      var i, j, matrix;
      matrix = [];
      for (i = list.length; i--;) {
        for (j = i; j--;) {
          matrix.push([characters.indexOf(list[i].character), characters.indexOf(list[j].character)]);
        }
      }
      return matrix;
    }
  }

  // Group scenes
  // ------------
  //
  // Each scene is assigned to a group based on the median character group for
  // characters appearing in that scene.
  // *Note:* "median" here is a mistake, it should be mode.
  function setSceneGroups() {
    scenes.forEach(function (scene) {
      var groupCounts, groupCountsMap, medianGroup;

      groupCounts = [];
      groupCountsMap = {};
      scene.appearances.forEach(function (appearance) {
        var count, index;

        index = groups.indexOf(appearance.character.group);
        count = groupCountsMap[index];

        if (!count) {
          count = {
            groupIndex: index,
            count: 0
          };
          groupCountsMap[index] = count;
          groupCounts.push(count);
        }
        count.count++;
      });

      groupCounts.sort(function (a, b) {
        return a.count - b.count;
      });

      medianGroup = groups[groupCounts.pop().groupIndex];
      // While we're here record how many scenes this group is the modal group for.
      medianGroup.medianCount = medianGroup.medianCount || 0;
      medianGroup.medianCount++;
      scene.group = medianGroup;
    });
  }

  // Group appearances
  // -----------------
  //
  // Assign unique set of characters to each group based on appearances in
  // scenes belonging to that group.
  function computeGroupAppearances() {
    scenes.forEach(function (scene) {
      var characters;
      characters = scene.appearances.map(function (a) {
        return a.character;
      });
      scene.group.appearances = scene.group.appearances || [];
      scene.group.appearances = scene.group.appearances.concat(characters.filter(function (character) {
        return scene.group.appearances.indexOf(character) === -1;
      }));
    });
  }

  // Sort groups
  // -----------
  //
  // Sort the array of groups so groups which are most often the median are at
  // the extremes of the array. The centre most group should be the group which
  // is least often the median group of a scene.
  function sortGroups() {

    var alt, sortedGroups, group, i;

    // First sort by the group's medianCount property (the number of times the
    // group is the median group in a scene).
    groups.sort(function (a, b) {
      return a.medianCount - b.medianCount;
    });

    // Specify order property and shuffle out groups into an ordered array.
    sortedGroups = [];
    i = 0;
    while (groups.length) {
      group = (alt) ? groups.pop() : groups.shift();
      group.order = i;
      i++;
      sortedGroups.push(group);
      alt = !alt;
    }


    groups = sortedGroups;
  }

  // Group positions
  // ---------------
  //
  // Compute the actual min and max y-positions of each group.
  function computeGroupPositions() {
    var max;
    max = 0;

    groups.forEach(function (group) {
			/*console.log("group")
			console.log(group)*/
      // TODO: NOT SURE WHY I NEED THIS!
      if (group.appearances) {
        group.min = max;
        group.max = max = characterGroupHeight(group.appearances.length) + group.min;
        max += groupMargin;
      }
    });
  }

  // Character group positions
  // -------------------------
  //
  // Compute the position of each character within its group.
  function computeCharacterGroupPositions() {

    characters.forEach(function (character) {

      var sum, count;
      sum = count = 0;
      character.appearances.forEach(function (appearance) {
        count++;
        sum += groups.indexOf(appearance.scene.group);
      });

      character.averageScenePosition = sum / count;

    });

    groups.forEach(function (group) {

      group.characters.sort(function (a, b) {

        var diff;
        // Average scene position.
        diff = a.averageScenePosition - b.averageScenePosition;
        if (diff !== 0) {
          return diff;
        }

        var characterTemp = group.characters.sort(function (a, b) {
          return b.appearances.length - a.appearances.length;
        });
        var hub = characterTemp[0];
        var connectionA = 0;
        var connectionB = 0;
        hub.appearances.forEach(function (d) {
          var objA = lookup(a.key, d.scene.characters)
          var objB = lookup(b.key, d.scene.characters)
          if (objA) {
            connectionA = connectionA + 1;
          }
          if (objB) {
            connectionB = connectionB + 1;
          }
        })
        // Average scene position.
        diff = connectionB - connectionA;
        if (diff !== 0) {
          return diff;
        }

        return characters.indexOf(a) - characters.indexOf(b);
      });
    });

    //computeInternalCharacterPositionsByNumConnections();
  }

  function computeInternalCharacterPositionsByNumConnections() {

    groups.forEach(function (group) {

      var characterTemp = group.characters.sort(function (a, b) {
        return b.appearances.length - a.appearances.length;
      });
      var hub = characterTemp[0];

      group.characters.sort(function (a, b) {
        var connectionA = 0;
        var connectionB = 0;
        hub.appearances.forEach(function (d) {
          var objA = lookup(a.key, d.scene.characters)
          var objB = lookup(b.key, d.scene.characters)
          if (objA) {
            connectionA = connectionA + 1;
          }
          if (objB) {
            connectionB = connectionB + 1;
          }

        })
        // Average scene position.
        var diff = connectionB - connectionA;

        if (diff !== 0) {

          return diff;
        }
        return characters.indexOf(a) - characters.indexOf(b);
      });
    });
  }

  function recomputeCharacterOrder() {
    characters = [];
    groups.forEach(function (gr) {

      gr.characters.forEach(function (ch) {
        characters.push(ch)
      })
    })

  }

  // Sort group appearances
  // ----------------------
  //
  // Group appearances (`group.appearances`) is an array
  // of all characters which appear in scenes assigned to this group.
  function sortGroupAppearances() {


    groups.forEach(function (group) {
      //console.log(group)
      // TODO: NOT SURE WHY I NEED THIS!
      if (group.appearances) {
        group.appearances.sort(function (a, b) {

          var diff;

          // Try simple group order.
          diff = a.group.order - b.group.order;
          if (diff !== 0) {
            return diff;
          }

          // Average scene position.
          diff = a.averageScenePosition - b.averageScenePosition;
          if (diff !== 0) {
            return diff;
          }

          // Array position.
          return group.characters.indexOf(a) - group.characters.indexOf(b);
        });
      }
    });

  }

  // Scene timing
  // ------------
  //
  // Compute the scene timing.
  //
  // TODO: support dates
  // Changed

  // Aggregation by Year
	/*	function computeSceneTiming() {
			var duration = 1;
			var year = -1

			scenes.forEach(function (scene) {
				scene.duration = scene.duration || 1;
				if (!(year == scene.key)) {
					duration += scene.duration;
					year = scene.key
				}
				scene.start = scene.start || duration;

			});

			var ss = (size[0] - labelSize[0]) / duration;

			scale = ((orientation === 'vertical') ? size[1] - labelSize[1] : size[0] - labelSize[0]) / duration;
		}*/

  // Aggregation by Article
  function computeSceneTiming() {
    // TODO
    aggregationType = "Article"
    if (aggregationType == "Article") {
      var duration = 0;
      scenes.forEach(function (scene) {

        scene.start = scene.start || duration;
        scene.duration = scene.duration || 1;
        duration += scene.duration;


      });

      var ss = (size[0] - labelSize[0]) / duration;

      scale = ((orientation === 'vertical') ? size[1] - labelSize[1] : size[0] - labelSize[0]) / duration;
    } else {

      var duration = 1;
      var year = -1

      scenes.forEach(function (scene) {
        scene.duration = scene.duration || 1;
        if (!(year == scene.key)) {
          duration += scene.duration;
          year = scene.key
        }
        scene.start = scene.start || duration;

      });

      var ss = (size[0] - labelSize[0]) / duration;

      scale = ((orientation === 'vertical') ? size[1] - labelSize[1] : size[0] - labelSize[0]) / duration;


    }

  }

  // Character positions
  // -------------------
  //
  // Compute the position of characters within a scene.
  function computeAppearancePositions() {

    scenes.forEach(function (scene) {

      scene.appearances.sort(function (a, b) {

        var diff;

        // For characters in different groups.  Try simple group order.  
        diff = a.character.group.order - b.character.group.order;
        if (diff !== 0) {
          return diff;
        }

        // For characters in the same group use average scene position.
        diff = a.character.averageScenePosition - b.character.averageScenePosition;
        if (diff !== 0) {
          return diff;
        }

				/*		diff = a.character.group.characters.indexOf(a.character) - b.character.group.characters.indexOf(b.character);
						if (diff !== 0) {
							return diff;
						}*/

        // All else failing use main characters array order to keep things consistent.
        return characters.indexOf(a.character) - characters.indexOf(b.character);
      });

      scene.appearances.forEach(function (appearance, i) {
        if (orientation === 'vertical') {
          appearance.y = scenePadding[0];
          appearance.x = characterPosition(i) + scenePadding[3];
        } else {
          // changed by xt
          /*appearance.y = getAppearancePosition(i) + scenePadding[0];*/
          appearance.y = characterPosition(i) + scenePadding[0];

          appearance.x = scenePadding[3];
        }
      });

    });
  }

  // Position scenes
  // ---------------
  //
  // Compute the actual x and y positions for each scene.
  function computeScenePositions() {


    scenes.forEach(function (scene, i) {
      var sum, avg, appearances;


      /*scene.height = getSceneHeight(scene.appearances.length)*/

      // VANE CHANGE
      //scene.height = characterGroupHeight(scene.appearances.length) + scenePadding[0] + scenePadding[2];
      //scene.height = scenePadding[0] + scenePadding[2] + sceneHeight;


      //scene.width = scenePadding[1] + scenePadding[3] + sceneWidth;
      scene.width = sceneWidth;
      scene.height = sceneHeight;

      appearances = scene.appearances.filter(function (appearance) {
        return appearance.character.group !== scene.group;
      });

      if (!appearances.length) {
        appearances = scene.appearances;
      }

      sum = appearances.reduce(function (total, appearance) {
        return total += characterPosition(scene.group.appearances.indexOf(appearance.character)) + scene.group.min;
      }, 0);

      avg = sum / appearances.length;


      if (orientation === 'vertical') {
        scene.x = scene._x || Math.max(0, Math.min(size[0], avg - scene.width / 2));
        scene.y = scene._y || Math.max(0, Math.min(size[1], scale * scene.start + labelSize[1]));
      } else {
        // VANE CHANGE
        //scene.x = scene._x || Math.max(0, Math.min(size[0], scale * scene.start + labelSize[0] + scenePadding[1] + scenePadding[3]));
        scene.x = scene._x || Math.max(0, scale * scene.start + labelSize[0] + scenePadding[1] + scenePadding[3]);
        //scene.y = scene._y || Math.max(0, Math.min(size[1], avg - scene.height / 2));
        scene.y = scene._y || Math.max(0, avg - scene.height / 2);
      }

    });

  }

  // Introduction nodes
  // ------------------
  //
  // Create a collection of character 'introduction' nodes. These are nodes which
  // are displayed before the first appearance of each character.
  function createIntroductionNodes() {

    var appearances;

    appearances = characters.map(function (character) {
      return character.appearances[0];
    });

    introductions = [];
    appearances.forEach(function (appearance) {

      var introduction, x, y;

      // Create the introduction object.
      introduction = {
        character: appearance.character,
        bounds: getLabelBounds,
      };

      // Set the default position.
      if (orientation === 'vertical') {

        x = appearance.scene.x + appearance.x;
        y = appearance.scene.y - 0.5 * scale;

        // Move x-axis position to the dedicated label space if it makes sense.
        // if (x-labelSize[0] < labelSize[0]) {
        // 	x = labelSize[0];
        // }
      } else {

        x = appearance.scene.x - 0.5 * scale;
        y = appearance.scene.y + appearance.y;

        // Move x-axis position to the dedicated label space if it makes sense.
        if (x - labelSize[0] < labelSize[0]) {
          x = labelSize[0];
        }

      }

      if (orientation === 'vertical') {
        introduction.x = appearance.character._x || Math.max(0 + labelSize[0] / 2, Math.min(size[0] - labelSize[0] / 2, x));
        introduction.y = appearance.character._y || Math.max(0, Math.min(size[1] - labelSize[1], y));
      } else {
        introduction.x = appearance.character._x || Math.max(0, Math.min(size[0] - labelSize[0], x));
        introduction.y = appearance.character._y || Math.max(0 + labelSize[1] / 2, Math.min(size[1] - labelSize[1] / 2, y + 20));
      }

      introduction.width = appearance.character._width || labelSize[0];
      introduction.height = appearance.character._height || labelSize[1];

      appearance.character.introduction = introduction;
      introductions.push(introduction);

    });

  }

  // Introduction positions
  // ----------------------
  //
  // Layout the introduction nodes so that wherever possible they don't overlap
  // scenes or each other.
  function computeIntroductionPositions() {

    var collidables, intros;

    // Get a list of things introductions can collide with.
    collidables = introductions.concat(scenes);

    // Use a copy of the introductions array so we can sort it without changing
    // the main array's order.
    intros = introductions.slice();

    // Sort by y-axis position top to bottom.
    intros.sort(function (a, b) {
      return a.y - b.y;
    });

    // Attempt to resolve collisions.
    intros.forEach((orientation === 'vertical') ? resolveCollisionsVertical : resolveCollisionsHorizontal);
    // VANE CHANGES: I NEED TO RESOLVE VERTICAL COLLITIONS
    // 1) I MOVE THE SCENES
    /*intros.forEach( intro => {
      resolveCollisionsHorizontal(intro)
      //resolveCollisionsVertical(intro, false)
    })*/
    // 2) I MOVE THE INTRO THAT COLLIDED BECAUSE OF THE PREVIOUS STEP
    /*intros.forEach( intro => {
      //resolveCollisionsHorizontal(intro)
      resolveCollisionsVertical(intro, true)
    })*/

    // Resolve collisions with horizontal layout.
    function resolveCollisionsHorizontal(introduction) {
      var collisions;
      // Get the full list of items this introduction collides with
      collisions = collidesWith(introduction, collidables);
      moveForCollisionHorizontal(collisions, collidables, introduction);

    }

    // Resolve collisions with vertical layout.
    function resolveCollisionsVertical(introduction, onlyIntros) {
      var collisions;
      // Get the full list of items this introduction collides with
      collisions = collidesWithVertically(introduction, collidables);
      moveForCollisionVertical(collisions, collidables, introduction, onlyIntros);
    }



  }

  // Links
  // -----
  //
  // Create a collection of links between appearances.
  function createLinks() {

    links = [];
    var linkID = -1;

    characters.forEach(function (character) {
      var i;

      linkID = linkID + 1;
      // Links to intro nodes.
      links.push({
        linkID: linkID,
        character: character,
        source: character.introduction,
        target: character.appearances[0]
      });

      // Standard appearance links.
      for (i = 1; i < character.appearances.length; i++) {

        linkID = linkID + 1;
        links.push({
          linkID: linkID,
          character: character,
          source: character.appearances[i - 1],
          target: character.appearances[i]
        });
      }
    });


  }


  function simpleperms(data) {

    if (!(data instanceof Array)) {
      throw new TypeError("input data must be an Array");
    }

    datacopy = data.slice(); // make a copy

    var permutations = [],
      stack = [];

    for (var i = 0; i < datacopy.length - 1; i++) {
      newcopy = swapElements(datacopy, i, i + 1)
      permutations.push(newcopy);
    }
    return permutations;
  }

  function sortAppearancesMinimizeCrossings() {
    console.log('sort')

    var newcharacters = [];
    var minNumIntersects = [];

    for (var n = 0; n < groups.length; n++) {

      group = groups[n];
      console.log(group.characters)
      //var possibleGroupCharacters = simpleperms(group.characters);

      var possibleGroupCharacters = simpleperms(group.characters);
      possibleGroupCharacters = possibleGroupCharacters.slice(0, 10);

      var numLLIntersectsArray = [];
      var numLRIntersectsArray = [];
      overlapAppearanceArray = [];

      for (var i = 0; i < possibleGroupCharacters.length; i++) {

        group.characters = possibleGroupCharacters[i].slice();

        characters = []
        groups.forEach(function (gr) {
          gr.characters.forEach(function (ch) {
            characters.push(ch)
          })
        })

        computeGroupAppearances();
        sortGroups();
        computeGroupPositions();
        computeCharacterGroupPositions();
        //recomputeCharacterOrder(); // my added function
        sortGroupAppearances();

        computeSceneTiming();
        computeAppearancePositions();
        computeScenePositions();
        createIntroductionNodes();
        computeIntroductionPositions();
        createLinks();
        numLRIntersectsArray.push(getLRIntersects(possibleGroupCharacters[i]));
      }

      function findMinimumIndexes(array) {
        var min = Math.min.apply(Math, array);
        minNumIntersects.push(min);
        var indexes = [];

        array.forEach(function (item, idx, arr) {
          if (item === min) {
            indexes.push(idx);
          }
        });

        return indexes;
      }

      //weight method
			/*
						var weightedNumIntersectsArray = [];
						for (var i = 0; i < numLRIntersectsArray.length; i++) {
							var weightedNum = numLRIntersectsArray[i] * 1 + numLLIntersectsArray[i] * 0.2;
							weightedNumIntersectsArray.push(weightedNum);
						}
						var minIndexesLR = findMinimumIndexes(numLRIntersectsArray);
						var minIndexesLL = findMinimumIndexes(numLLIntersectsArray);
						var minIndexesMix = findMinimumIndexes(weightedNumIntersectsArray);
						console.log("minIndexes")
						console.log(minIndexesMix)

						var minIndex = 0;
						if (minIndexesMix.length > 0) {
							minIndex = minIndexesMix[0];
						} else {
							minIndex = minIndexesLR[0];
						}
			*/

      console.log("numLRIntersectsArray, group " + n)
      console.log(numLRIntersectsArray)
      console.log("overlapAppearanceArray, group " + n)
      console.log(overlapAppearanceArray)

      var weightedNumIntersectsArray = [];
      for (var i = 0; i < numLRIntersectsArray.length; i++) {
        var weightedNum = numLRIntersectsArray[i] * 1 + overlapAppearanceArray[i] * 100;
        weightedNumIntersectsArray.push(weightedNum);
      }

      console.log("weightedNumIntersectsArray, group " + n)
      console.log(weightedNumIntersectsArray)

      var minIndexesLR = findMinimumIndexes(weightedNumIntersectsArray);
      console.log("minIndexes LR, group " + n)
      console.log(minIndexesLR)

      var minIndex = 0;

      // Only LR !!!
      minIndex = minIndexesLR[0];
			/*		if (n == 0) {
						minIndex = minIndexesLR[3];
					}*/

			/*	var newcharacter = possibleGroupCharacters[minIndex].slice();
				newcharacters.push(newcharacter)*/

      // LR + LL
      var possibleGroupChsWithLeastLRIns = [];

      minIndexesLR.forEach(function (index) {
        var possibleGCH = possibleGroupCharacters[index];
        possibleGroupChsWithLeastLRIns.push(possibleGCH);
      })
      console.log("group: " + n)
      console.log(possibleGroupChsWithLeastLRIns)

      for (var i = 0; i < possibleGroupChsWithLeastLRIns.length; i++) {

        group.characters = possibleGroupChsWithLeastLRIns[i].slice();

        characters = []
        groups.forEach(function (gr) {
          gr.characters.forEach(function (ch) {
            characters.push(ch)
          })
        })

        computeGroupAppearances();
        sortGroups();
        computeGroupPositions();
        computeCharacterGroupPositions();
        sortGroupAppearances();

        computeSceneTiming();
        computeAppearancePositions();
        computeScenePositions();
        createIntroductionNodes();
        computeIntroductionPositions();
        createLinks();

        var numLL = getLLIntersects(possibleGroupCharacters[i]);
        numLLIntersectsArray.push(numLL);

      }

      console.log("numLLIntersectsArray")
      console.log(numLLIntersectsArray)
      var minIndexesLL = findMinimumIndexes(numLLIntersectsArray);
      console.log(minIndexesLL)

      minIndex = minIndexesLL[0]

      var newcharacter = possibleGroupChsWithLeastLRIns[minIndex].slice();
      newcharacters.push(newcharacter)



    }

    characters = []
    newcharacters.forEach(function (d) {
      characters = characters.concat(d);
    });
    console.log("newcharacters")
    console.log(newcharacters)
    var newcharacterIDs = [];

    characters.forEach(function (d) {
      newcharacterIDs.push(d.id);
    })
    console.log("newcharacterIDs")
    console.log(newcharacterIDs)

    computeGroupAppearances();
    sortGroups();
    computeGroupPositions();
    computeCharacterGroupPositions();
    sortGroupAppearances();

    computeSceneTiming();
    computeAppearancePositions();
    computeScenePositions();
    createIntroductionNodes();
    computeIntroductionPositions();
    createLinks();

    intersectPoints = [];
    LLIntersectsObj = [];

    for (var n = 0; n < groups.length; n++) {
      group = groups[n];
      getLRIntersects2(group.characters)
      getLLIntersects2(group.characters)
    }




    Array.min = function (array) {
      return Math.min.apply(Math, array);
    };


  }


  function getLRIntersects(groupChs) {
    var numLRIntersect = 0;
    var numOverlapAppearance = 0;

    var collidableLinks = [];
    links.forEach(function (d, i) {
      if (groupChs.indexOf(d.character) > -1) {
        collidableLinks.push(d);
      }
    })

    var collidableRelations = [];
    scenes.forEach(function (d, i) {
      //if (groupChs.indexOf(d.character) > -1) {
      collidableRelations.push(d);
      //}
    })

    for (var i = 0; i < collidableLinks.length; i++) {
      var link = collidableLinks[i];
      var linkPoints = getBezierPoints(link);
      for (var j = 0; j < collidableRelations.length; j++) {

        var sceneRect = collidableRelations[j];
        var sceneBound = getSceneBounds(sceneRect);

        var ifIntersect = false;
        var ifOverlapAppearance = false;

        linkPoints.forEach(function (p) {
          ifIntersect = ifIntersect || insideBounds(p, sceneBound);
          if (ifIntersect) {

            sceneRect.appearances.forEach(function (a) {
              var x = a.scene.x + a.x;
              var y = a.scene.y + a.y;
              var pAppearance = [x, y];
              ifOverlapAppearance = overlapAppearance(p, pAppearance)

              if (ifOverlapAppearance) {
                numOverlapAppearance = numOverlapAppearance + 1;
              }
            })
            if (!ifOverlapAppearance) {
              numLRIntersect = numLRIntersect + 1;
            }
          }
        })


      }
    }

    overlapAppearanceArray.push(numOverlapAppearance)

    function getSceneBounds(_scene) {
      return [[_scene.x, _scene.y], [_scene.x + _scene.width, _scene.y + _scene.height]];
    }

    return numLRIntersect;
  }

  function getLRIntersects2(groupChs) {

    var collidableLinks = [];
    links.forEach(function (d, i) {
      if (groupChs.indexOf(d.character) > -1) {
        collidableLinks.push(d);
      }
    })
    var collidableRelations = [];
    scenes.forEach(function (d, i) {
      //if (groupChs.indexOf(d.character) > -1) {
      collidableRelations.push(d);
      //}
    })
    for (var i = 0; i < collidableLinks.length; i++) {
      var link = collidableLinks[i];
      var linkPoints = getBezierPoints(link);

      for (var j = 0; j < collidableRelations.length; j++) {

        var sceneRect = collidableRelations[j];

        if (sceneRect.idsPeople.includes(link.character.id)) {
          continue
        }
        var sceneBound = getSceneBounds(sceneRect);

        var ifIntersect = false;
        var ifOverlapAppearance = false;

        for (var k = 0; k < linkPoints.length; k++) {

          var p = linkPoints[k]

          ifIntersect = insideBounds(p, sceneBound);

          if (ifIntersect) {

            var linkJunction = [];
            linkJunction.push(linkPoints[0])
            var index = linkPoints.length - 1;
            linkJunction.push(linkPoints[index])
            var ClippingItem = {
              linkID: link.linkID,
              intersectCoord: [p[0], p[1]],
              junction: linkJunction
            }

            masks.push(ClippingItem);
            break
						/*
						sceneRect.appearances.forEach(function (a) {
							var x = a.scene.x + a.x;
							var y = a.scene.y + a.y;
							var pAppearance = [x, y];
							ifOverlapAppearance = overlapAppearance(p, pAppearance)

						})
						*/
          }


        }

      }

    }

    function getSceneBounds(_scene) {
      return [[_scene.x, _scene.y], [_scene.x + _scene.width, _scene.y + _scene.height]];
    }
  }

  function getLLIntersects(groupChs) {

    var numLLIntersects = 0;

    var collidables = [];
    links.forEach(function (d, i) {
      if (groupChs.indexOf(d.character) > -1) {
        collidables.push(d);
      }
    })

    for (var i = 0; i < collidables.length; i++) {
      for (var j = i + 1; j < collidables.length; j++) {
        if (collidables[i].character.key != collidables[j].character.key) {
          var line1 = collidables[i];
          var line2 = collidables[j];

					/*	var ifLinesIntersect = detectLLIntersect(getLinkBounds(collidables[i]), getLinkBounds(collidables[j]))
						if (ifLinesIntersect) {
							numLLIntersects = numLLIntersects + 1;
						}*/

          var testSvg = d3.select("#mainScene")
            .append('svg')
            .attr("class", "testSvg")
            .attr("width", chart.width + chart.margin.left + chart.margin.right)
            .attr("height", chart.height + chart.margin.top + chart.margin.bottom);

          var Path1El = testSvg.append('path')
            .attr('class', "tempLink")
            .attr('d', getlinkPath(line1))
            .attr("stroke", function (d) {
              return "grey";
            })
            .attr("fill", "none")
            .attr("stroke-width", "4.5px");

          var Path2El = testSvg.append('path')
            .attr('class', "tempLink")
            .attr('d', getlinkPath(line2))
            .attr("stroke", function (d) {
              return "grey";
            })
            .attr("fill", "none")
            .attr("stroke-width", "4.5px");

          var Path1Node = Path1El.node();
          var Path2Node = Path2El.node();
          var pointsIntersect = detectIntersects(Path1Node, Path2Node);

          d3.select("#mainScene").selectAll(".testSvg").remove();

          if (pointsIntersect.length > 0) {
            numLLIntersects = numLLIntersects + pointsIntersect.length;
          }


        }

      }
    }

    return numLLIntersects;
  }

  function getLLIntersects2(groupChs) {

    var collidables = [];
    links.forEach(function (d, i) {
      if (groupChs.indexOf(d.character) > -1) {
        collidables.push(d);
      }
    })

    for (var i = 0; i < collidables.length; i++) {
      for (var j = i + 1; j < collidables.length; j++) {
        if (collidables[i].character.key != collidables[j].character.key) {
          var line1 = collidables[i];
          var line2 = collidables[j];

          var testSvg = d3.select("#mainScene")
            .append('svg')
            .attr("class", "testSvg")
            .attr("width", chart.width + chart.margin.left + chart.margin.right)
            .attr("height", chart.height + chart.margin.top + chart.margin.bottom);

          var PathEl1 = testSvg.append('path')
            .attr('class', "tempLink")
            .attr('d', getlinkPath(line1))
            .attr("stroke", function (d) {
              return "grey";
            })
            .attr("fill", "none")
            .attr("stroke-width", "4.5px");

          var PathEl2 = testSvg.append('path')
            .attr('class', "tempLink")
            .attr('d', getlinkPath(line2))
            .attr("stroke", function (d) {
              return "grey";
            })
            .attr("fill", "none")
            .attr("stroke-width", "4.5px");

          var PathNode1 = PathEl1.node();
          var PathNode2 = PathEl2.node();
          var pointsIntersect = detectIntersects(PathNode1, PathNode2);

          d3.select("#mainScene").selectAll(".testSvg").remove();

          if (pointsIntersect.length > 0) {

            var El1pos1 = getElpos(PathNode1, 0)
            var El2pos1 = getElpos(PathNode2, 0)
            var startX = Math.max(El1pos1.x, El2pos1.x)
            var El1posEnd = PathNode1.getPointAtLength(PathNode1.getTotalLength());
            var El2posEnd = PathNode2.getPointAtLength(PathNode2.getTotalLength());
            var endX = Math.min(El1posEnd.x, El2posEnd.x)
            var linkJunction = [];
            linkJunction.push(startX)
            linkJunction.push(endX)
            pointsIntersect.forEach(function (point) {
              var ClippingItem = {
                linkID: line1.linkID,
                junction: linkJunction,
                intersectCoord: [point.x, point.y]
              }
              masks.push(ClippingItem);

            })

          }
					/*	var ifLinesIntersect = detectLLIntersect(getLinkBounds(collidables[i]), getLinkBounds(collidables[j]))
						if (ifLinesIntersect) {
							numLLIntersects = numLLIntersects + 1;*/


        }

      }


    }

  }

  // Utility functions
  // =================
  //
  // Get the actual y-axis position of a character with the given (zero based) index.
  function characterPosition(index) {
    return index * pathSpace + pathSpace / 2;
  }

  // Get the actual height of a group based on a character count.
  function characterGroupHeight(count) {
    //return count * pathSpace * 1 / 3 + 90;
    return characterPosition(count) - pathSpace / 2;
  }

  // added by xt
  function getAppearancePosition(index) {
    return (index * pathSpace + pathSpace / 2) * 1 / 3
    //return characterPosition(count) - pathSpace / 2;
  }

  // added by xt
  function getSceneHeight(nAppearance) {
    //return nAppearance * pathSpace * 1 / 3;
    return characterPosition(count) - pathSpace / 2;
  }
  // Scene bounds
  // ------------
  //
  // This is attached to all scene objects as `scene.bound` and returns the bounds
  // for the scene node.
  function getSceneBounds() {
    return [[this.x, this.y], [this.x + this.width, this.y + this.height]];
  }

  // Label bounds
  // ------------
  //
  // This is attached to all character objects as `character.bounds` and returns
  // the bounds of the character's introduction label.
  function getLabelBounds() {
    switch (labelPosition) {
      case ('left'):
        return [[this.x - this.width, this.y - this.height / 2], [this.x, this.y + this.height / 2]];
      case ('above'):
        return [[this.x - this.width / 2, this.y - this.height], [this.x + this.width / 2, this.y]];
      case ('right'):
        return [[this.x, this.y - this.height / 2], [this.x + this.width, this.y + this.height / 2]];
      case ('below'):
        return [[this.x - this.width / 2, this.y], [this.x + this.width / 2, this.y + this.height]];
    }

  }

  function getLinkBounds(l) {

    if (l.source.scene) {

      if (l.target.scene) {
        var x1 = l.source.scene.x + l.source.x;
        var y1 = l.source.scene.y + l.source.y;
        var x2 = l.target.scene.x + l.target.x;
        var y2 = l.target.scene.y + l.target.y;
        //console.log([[x1, y1], [x2, y2]])
        return [[x1, y1], [x2, y2]];

      } else {
        var x1 = l.source.scene.x + l.source.x;
        var y1 = l.source.scene.y + l.source.y;
        var x2 = l.target.x
        var y2 = l.target.y;
        //console.log([[x1, y1], [x2, y2]])
        return [[x1, y1], [x2, y2]];
      }

    } else {
      if (l.target.scene) {
        //console.log([[l.source.x, l.source.y], [l.target.scene.x + l.target.x, l.target.scene.y + l.target.y]])

        return [[l.source.x, l.source.y], [l.target.scene.x + l.target.x, l.target.scene.y + l.target.y]];
      } else {
        //console.log([[l.source.x, l.source.y], [l.target.x, l.target.y]])

        return [[l.source.x, l.source.y], [l.target.x, l.target.y]];
      }
    }

  }

  function getElpos(PathEl, index) {

    var n_segments = 10;
    var pathLength = PathEl.getTotalLength();
    return PathEl.getPointAtLength(pathLength * index / n_segments);
  }

  function detectIntersects(PathEl1, PathEl2) {

    var n_segments = 10;
    var Path1SegArray = [];
    var Path2SegArray = [];

    for (var i = 0; i < n_segments; i++) {
      var El1pos1 = getElpos(PathEl1, i)
      var El1pos2 = getElpos(PathEl1, i + 1)
      var line1Seg = {
        x1: El1pos1.x,
        x2: El1pos2.x,
        y1: El1pos1.y,
        y2: El1pos2.y
      };
      Path1SegArray.push(line1Seg);

      var El2pos1 = getElpos(PathEl2, i)
      var El2pos2 = getElpos(PathEl2, i + 1)

      var line2Seg = {
        x1: El2pos1.x,
        x2: El2pos2.x,
        y1: El2pos1.y,
        y2: El2pos2.y
      };
      Path2SegArray.push(line2Seg);

    }

    var pts = [];

    for (var i = 0; i < n_segments; i++) {
      var seg1 = Path1SegArray[i];
      for (var j = 0; j < n_segments; j++) {
        var seg2 = Path2SegArray[j];
        var pt = line_line_intersect(seg1, seg2);
        if (typeof (pt) != "string") {
          pts.push(pt);
        }
      }

    }
    return pts;
		/*	var numPts = 0;
			for (var i = 0; i < n_segments; i++) {
				var seg1 = Path1SegArray[i];
				for (var j = 0; j < n_segments; j++) {
					var seg2 = Path2SegArray[j];
					var ifIntersect = line_line_intersect(seg1, seg2);
					if (ifIntersect) {
						numPts = numPts + 1
					}
				}

			}

			return numPts;*/
  }

  function line_line_intersect(line1, line2) {

    var x1 = line1.x1;
    var x2 = line1.x2;
    var x3 = line2.x1;
    var x4 = line2.x2;
    var y1 = line1.y1;
    var y2 = line1.y2;
    var y3 = line2.y1;
    var y4 = line2.y2;

    var pt_denom = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    var pt_x_num = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
    var pt_y_num = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4);
    if (pt_denom == 0) {
      return "parallel";
    } else {
      var pt = {
        'x': pt_x_num / pt_denom,
        'y': pt_y_num / pt_denom
      };
      if (btwn(pt.x, x1, x2) && btwn(pt.y, y1, y2) && btwn(pt.x, x3, x4) && btwn(pt.y, y3, y4)) {
        return pt;
      } else {
        return "not in range";
      }
    }
		/*var diffX1 = Math.abs(x1 - x3);
		var diffX2 = Math.abs(x2 - x4);
		var diffY1 = Math.abs(y1 - y3);
		var diffY2 = Math.abs(y2 - y4);

		if (diffX1 < 1 && diffX2 < 1 && diffY1 < 1 && diffY2 < 1) {
			return true;
		}*/
  }

  function btwn(a, b1, b2) {
    if ((a >= b1) && (a <= b2)) {
      return true;
    }
    if ((a >= b2) && (a <= b1)) {
      return true;
    }
    return false;
  }

  function getBezierPoints(d) {

    var curvature = 0.5;

    var x0, x1, y0, y1, cx0, cy0, cx1, cy1, ci;

    // Set path end positions.
    x0 = (d.source.scene) ? d.source.scene.x + d.source.x : d.source.x;
    y0 = (d.source.scene) ? d.source.scene.y + d.source.y : d.source.y;
    x1 = (d.target.scene) ? d.target.scene.x + d.target.x : d.target.x;
    y1 = (d.target.scene) ? d.target.scene.y + d.target.y : d.target.y;

    // Set control points.
    if (orientation === 'vertical') {
      ci = d3.interpolateNumber(y0, y1);
      cx0 = x0;
      cy0 = ci(curvature);
      cx1 = x1;
      cy1 = ci(1 - curvature);
    } else {
      ci = d3.interpolateNumber(x0, x1);
      cx0 = ci(curvature);
      cy0 = y0;
      cx1 = ci(1 - curvature);
      cy1 = y1;
    }

		/*	return "M" + x0 + "," + y0 +
			"C" + cx0 + "," + cy0 +
			" " + cx1 + "," + cy1 +
			" " + x1 + "," + y1;
*/

    function get_bezier_point_position(pos, p0, p1, p2, p3) {
			/*
			 * Determines the coordinates of a point on the bezier curve based on
			 * the pos and the control points
			 * Args:
			 * pos: float from [0-1] - how far along the bezier curve the point is
			 * p0-p3 : [x, y]
			 * */

      const coords = [p0, p1, p2, p3];

      const get_coord = (c0, c1, c2, c3) => (Math.pow(1 - pos, 3) * c0) +
        (3 * Math.pow(1 - pos, 2) * pos * c1) +
        (3 * (1 - pos) * Math.pow(pos, 2) * c2) +
        (Math.pow(pos, 3) * c3);

      const x = get_coord(...coords.map((c) => c[0]));
      const y = get_coord(...coords.map((c) => c[1]));

      return [x, y];
    }

    var control_points = [];

    control_points.push([x0, y0]);
    control_points.push([cx0, cy0]);
    control_points.push([cx1, cy1]);
    control_points.push([x1, y1]);

    const point_count = Math.floor((x1 - x0) / 4);
    //const point_count = Math.floor((x1 - x0) / 2);

    const precision = 1 / (point_count - 1);
    var bezierPoints = [];
    for (let i = 1; i <= point_count; i++) {
      // again - 1 so there are points at the ends of the curve

      var point = get_bezier_point_position(precision * (i - 1), ...control_points);
      if ((point[0] > x0 + 4) && (point[0] < x1)) {
        bezierPoints.push(point);
      }
    }

		/*	const point_count = 1500;

		const precision = 1 / (point_count - 1);

		var bezierPoints = [];

		for (let i = 1; i <= point_count; i++) {
			// again - 1 so there are points at the ends of the curve
			var point = get_bezier_point_position(precision * (i - 1), ...control_points);
			if ((point[0] > x0 + 4) && (point[0] < x1 )) {
				bezierPoints.push(point);
			}
		}
*/
    return bezierPoints;

  }
};

function escapeRegExp(string) {
  return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
}

export function replaceAll(str, find, _replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), _replace)
}

export function getTranslateCoordinates(node) {
  const transform = node.getAttribute('transform')
  const indexStartParentesis = transform.indexOf('(')
  const indexComma = transform.indexOf(',')
  const result = {}
  result.x = Number(transform.substring(indexStartParentesis + 1, indexComma))
  result.y = Number(transform.substring(indexComma + 1, transform.length - 1))
  return result
}

export function cleanName(string){
  var result = string
  const to_replace = ['-', '(', ')', ' ', '.']
  to_replace.forEach(c => {
    result = replaceAll(result, c, '')
  })
  return result
}

export function getLinkGroupId(d, i) {

  var result = 'link-S-'
  var source = d.source
  if (source.scene) {
    result += 'scene-' + source.scene.id + '-'
  }
  result += 'char-' + source.character.id
  result += '-T-'
  var target = d.target
  if (target.scene) {
    result += 'scene-' + target.scene.id + '-'
  }
  result += 'char-' + target.character.id
  return result
}

export function nameCutter(name) {
  return name.substring(0, 20)
}

String.prototype.getAllIndices = function (strFind) {
  const regex = new RegExp(strFind, "ig");
  return this.matchAll(regex);
};

export function getScenesURL(selections, datasetName) {
  var url = '/geostorylines/get_scenes/?name=' + datasetName
  for (let d in selections.dim) {
    url += '&' + d + 'Dim=' + selections.dim[d]
  }
  for (let a in selections.aggr) {
    url += '&' + a + 'Aggr=' + selections.aggr[a]
  }
  url += '&xDimOrder=' + selections.order.x
  return url
}

export function getNestedScenesURL(scene, selections, datasetName) {
  var url = '/geostorylines/get_nested_scenes/?name=' + datasetName
  for (let d in selections.dim) {
    url += '&' + d + 'Dim=' + selections.dim[d]
  }
  for (let a in selections.aggr) {
    url += '&' + a + 'Aggr=' + selections.aggr[a]
  }
  url += '&scene_edges='
  for (let s in scene.edge.original_edges_ids) {
    const edge_id = scene.edge.original_edges_ids[s]
    url += edge_id
    if (s < scene.edge.original_edges_ids.length - 1) {
      url += '+'
    }
  }
  url += '&ZDimOrder=' + selections.order.z
  return url
}
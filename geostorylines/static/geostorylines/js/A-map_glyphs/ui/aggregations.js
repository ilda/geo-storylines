export default function aggregations() {
  var aggregations = {}

  var _active
  var _activeType
  var _gsl
  var _AggrsConfig
  var _allAggregations
  var _uiSelectors

  aggregations.init = function (uiSelectors) {
    //_gsl = gsl
    _uiSelectors = uiSelectors
    _activeType = undefined
    _active = {}
    _active['x'] = 'aggr_article'
    _active['z'] = 'aggr_article'
    createContainers()
    return aggregations
  }

  // INITIALIZATION
  function createContainers() {
  }
  aggregations.setConfig = function (aggrs) {
    _AggrsConfig = aggrs
    _allAggregations = []
    for (let a in aggrs) {
      _allAggregations.push(a)
    }
  }

  aggregations.updateAggregators = function () {
  }

  // GETTERS
  aggregations.getAll = function () {
    return _allAggregations
  }

  aggregations.active = function (axis) {
    return _active[axis]
  }
  aggregations.activeAll = function () {
    return _active
  }
  return aggregations
}

export default function order() {
    var order = {}

    order.init = function () {
        return order
    }

    order.getX = function () {
        return 'time'
    }

    order.getZ = function () {
        return 'time'
    }

    order.activeAll = function () {
        const result = {}
        result['x'] = order.getX()
        result['z'] = order.getZ()
        return result
    }

    return order
}
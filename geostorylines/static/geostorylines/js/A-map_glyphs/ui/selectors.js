import dimensions from './dimensions.js'
import aggregations from './aggregations.js'
import order from './order.js'
import datasets from './dataset.js'
import { helpText } from './help.js'

export default function uiSelectors() {
  var _dimensions
  var _aggregations
  var _order
  var _gsl
  var uiSelectors = {}

  // INIT
  uiSelectors.gsl = function (gsl) {
    _gsl = gsl
    _dimensions = dimensions().gsl(gsl)
    _aggregations = aggregations().init(uiSelectors)
    _order = order().init()
    datasets().gsl(gsl)
    return uiSelectors
  }

  uiSelectors.dimensions = function (values) {
    _dimensions.setConfig(values)
    return uiSelectors
  }

  uiSelectors.aggregations = function (values) {
    _aggregations.setConfig(values)
    return uiSelectors
  }

  uiSelectors.updateAggregators = function () {
    _aggregations.updateAggregators()
    return uiSelectors
  }

  // GETTERS
  uiSelectors.getAllDimensions = function () {
    return _dimensions.getAll()
  }

  uiSelectors.getActiveDimensions = function () {
    return _dimensions.activeAll()
  }

  uiSelectors.getActiveAggregations = function () {
    return _aggregations.activeAll()
  }

  uiSelectors.getAllSelections = function () {
    const r = {}
    r['dim'] = _dimensions.activeAll()
    r['aggr'] = _aggregations.activeAll()
    r['order'] = _order.activeAll()
    return r
  }

  uiSelectors.changeAggregation = function () {
    _gsl.changeAggregation()
  }

  uiSelectors.addHelp = function () {
    d3.select('body')
      .selectAll('#helpContent')
      .data([1])
      .enter()
      .append('section')
      .attr('id', 'helpContent')
      .html(helpText())
    $('#helpContent').popup({
      pagecontainer: '#gsl-main',
      escape: false,
    })

    d3
      .select('.gsl-extraselector')
      .append("img")
      .style("margin-left", 20)
      .attr('width', 70)
      .attr('height', 71)
      .attr("src", static_url + "geostorylines/images/compass_small.png")
    d3
      .select('.gsl-extraselector')
      .append('label')
      .attr('class', 'copy-label')
      .style('margin-left', '10px')
    return uiSelectors
  }

  uiSelectors.reset = function () {
    d3.select('.datasets-panel').remove()
  }
  return uiSelectors
}

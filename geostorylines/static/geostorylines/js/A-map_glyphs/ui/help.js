export function helpText() {
  var result = `
  <br>
  <h2> Map Glyphs </h2>
  <br>
    <ul> 
      <li>
      - This visualization has three parts: the lines, the small maps, and the horizontal timeline.
      </li>
      <br>
      <br>
      <li>
      - Each line represents a person’s <b class="highlightText">story</b>. 
      As a line progresses along the x-axis, it shows events for that person in chronological order.
      </li>
      <br>
      <br>
      <li>
      - Events are represented by a rectangle which contains a <b class="highlightText">map</b>. 
      The locations(s) filled on this map are those related to the event. 
      </li>
      <br>
      <br>
      <li> 
        - An event can involve more than one person. 
        When two lines curve to come together in an event, this symbolizes a 
        <b class="highlightText">relationship</b> between the people represented by those lines.  
      </li>
      <br>
      <br>
      <li>
      - You can <b class="highlightText">highlight</b> any type of entity by clicking on it: 
      people’s lines, places on the map, and dates in the timeline. 
      </li>
      <br>
      <br>
      <li>
      - Click once on the background to <b>clear</b> all your selections.
      </li>
      <br>
      <br>
      <li>
      - The name of the last clicked entity or date will be copied in the clipboard. 
      You can use this feature to copy answers or search for information.
      </li>
      </ul>
      `
  return result
}
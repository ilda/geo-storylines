function lookup(name, arr) {
    for (var i = 0, len = arr.length; i < len; i++) {
        if (arr[i].key === name)
            return arr[i];
    }
    return false;
}

// Gets a list of all other nodes that this introduction collides with.
function collidesWith(introduction, collidables) {
    var i, ii, collisions;
    collisions = [];
    for (i = 0, ii = collidables.length; i < ii; i++) {
        if (introduction !== collidables[i] && collides(introduction.bounds(), collidables[i].bounds())) {
            collisions.push(collidables[i]);
        }
    }
    return (collisions.length) ? collisions : false;
}

function collidesWithVertically(introduction, collidables) {
    var i, ii, collisions;
    collisions = [];
    for (i = 0, ii = collidables.length; i < ii; i++) {
        if (introduction !== collidables[i] && collides(introduction.bounds(), collidables[i].bounds())) {
            collisions.push(collidables[i]);
        }
    }
    return (collisions.length) ? collisions : false;
}

// Check for overlap between two bounding boxes.
function collides(a, b) {
    return !(
        // Verticals.
        a[1][0] <= b[0][0] ||
        b[1][0] <= a[0][0] ||

        // Horizontals.
        a[1][1] <= b[0][1] ||
        b[1][1] <= a[0][1]);
}

function collidesVertically(a, b) {
    return !(
        // Verticals.
        a[1][0] <= b[0][0] ||
        b[1][0] <= a[0][0] ||

        // Horizontals.
        a[1][1] <= b[0][1] ||
        b[1][1] <= a[0][1]);
}

function moveForCollisionHorizontal(collisions, collidables, introduction) {
    var moveOptions, collisionBounds, introBounds, move, _y, movable;
    // No need to continue if there are no collisions.
    if (!collisions) {
        return;
    }
    // Move colliding items out of the way if possible.
    movable = collisions.filter(function (collision) {
        return (collision.character);
    });
    movable.forEach(moveCollision);

    // Now only consider immovables (i.e. scene nodes).
    collisions = collisions.filter(function (collision) {
        return !(collision.character);
    });

    // No need to continue if there are no collisions.
    if (!collisions) {
        return;
    }

    // Get a bounding box for all remaining colliding nodes.
    collisionBounds = bBox(collisions);
    introBounds = introduction.bounds();

    // Record the original y-axis position so we can revert if a move is a failure.
    _y = introduction.y;

    // Calculate the two move options (up or down).
    moveOptions = [collisionBounds[1][1] - introBounds[0][1], collisionBounds[0][1] - introBounds[1][1]];
    console.log(moveOptions)

    // Sort by absolute distance. Try the smallest move first.
    moveOptions.sort(function (a, b) {
        return Math.abs(a) - Math.abs(b);
    });

    // Try the move options in turn.
    while (move = moveOptions.shift()) {

        introduction.y += move + (45 * Math.sign(move));
        collisions = collidesWith(introduction, collidables);

        if (collisions) {
            if (move > 0 && collisions.every(isMovable)) {
                collisions.forEach(moveCollision);
                break;
            } else {
                // Commented the following
                introduction.y = _y;
            }
        } else {
            break;
        }
    }

    // Move the colliding nodes.
    function moveCollision(collision) {
        collision.y += introduction.bounds()[1][1] - collision.bounds()[0][1];
    }
}
function moveForCollisionVertical(collisions, collidables, introduction, onlyIntros) {
    var moveOptions, collisionBounds, introBounds, move, _y, movable;
    // No need to continue if there are no collisions.
    if (!collisions) {
        return;
    }
    // Move colliding items out of the way if possible.
    /*movable = collisions.filter(function(collision) {
        return (collision.character);
    });
    console.log(movable)*/
    collisions.forEach(c => {
        moveCollision(c, collidables, introduction, onlyIntros)
    });
    
    // Move the colliding nodes.
    function moveCollision(collision, collidables, introduction, onlyIntros) {
        //collision.y += introduction.bounds()[1][1] - collision.bounds()[0][1];
        // IF THE COLLITION IS A SCENE
        if (collision.edge) {
            if(onlyIntros) {
                introduction.y += introduction.bounds()[1][1] - collision.bounds()[0][1]
                return 
            }
            //introduction.y += 350 + introduction.bounds()[1][1]
            introduction.y += 350 + introduction.bounds()[1][1]
            // IF I HAVE TO MOVE THE INTRO -> MOVE ALL RELATED SCENES
            
            collidables.forEach(e => {
                if (!('charactersIds' in e)) return
                if (e.charactersIds.indexOf(introduction.character.id) >= 0) {
                    e.y += 350 / 2
                }
            })
        } else {
            //collision.y += introduction.bounds()[1][1] - collision.bounds()[0][1];
        }
    }
}

// Create a bounding box around a collection of nodes.
function bBox(arr) {
    var x0, x1, y0, y1;
    x0 = d3.min(arr, function (d) {
        return d.bounds()[0][0];
    });
    x1 = d3.max(arr, function (d) {
        return d.bounds()[1][0];
    });
    y0 = d3.min(arr, function (d) {
        return d.bounds()[0][1];
    });
    y1 = d3.max(arr, function (d) {
        return d.bounds()[1][1];
    });
    return [
        [x0, y0],
        [x1, y1]
    ];
}

// Is the supplied node movable?
function isMovable(collision) {
    return (collision.character);
}
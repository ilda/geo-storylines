import data from './data/manager.js'
import chart from './vis/chart.js'
import uiSelectors from './ui/selectors.js'
import { getScenesURL } from './gsl-utils.js'
import entityInteractions from './interactions/entity.js'
import backgroundInteractions from './interactions/background.js'
import scenesInteractions from './interactions/scenes.js'

export default function gsl() {
  var _data
  var _uiSelectors
  var _chart
  var _entityInteractions
  var _backgroundInteractions
  var _scenesInteractions
  var _highlighted_location
  var _rotation = 0

  //////////////////////////////////////////////////////////////////////////////
  // INITIALIZE
  //////////////////////////////////////////////////////////////////////////////
  var gsl = {}
  gsl.init = function () {
    _backgroundInteractions = backgroundInteractions().gsl(gsl)
    _entityInteractions = entityInteractions().gsl(gsl)
    _scenesInteractions = scenesInteractions().gsl(gsl)
    _chart = chart()
    _uiSelectors = uiSelectors().gsl(gsl)
    d3.json('/geostorylines/datasets-available').then(function (data) {
      _highlighted_location = data.highlighted_location
      _rotation = data.map_rotation
      // PAPER CONF
      //_rotation = 30
      gsl.changeDataset(data.datasets[0])
    })
    return gsl
  }

  gsl.show = function () {
    gsl.createContainers()
    _uiSelectors = uiSelectors()
      .gsl(gsl)
      .dimensions(_data.dimensions())
      .aggregations(_data.aggregations())
      .addHelp()
    gsl.updateNarrative()
    return gsl
  }

  gsl.data = function (name, dimensions) {
    _data = data().init(name, dimensions)
    return gsl
  }

  gsl.createContainers = function () {
    const main = d3.select('.gsl-container')
    /*const selectors = main
      .append('div')
      .attr('class', 'gsl-selectors')
      .attr('id', 'dimension-select')
    const dims = ['x', 'y', 'z']
    dims.forEach((d) => {
      selectors.append('div').attr('class', 'gsl-' + d + 'selector')
    })
    const extraSelectors = selectors
      .append('div')
      .attr('class', 'gsl-extraselector')*/
    main
      .append('div')
      .attr('class', 'gsl-extraselector')
      .append('button')
      .attr('id', 'help')
      .attr('class', 'btn btn-secondary helpContent_open')
      .html('Help')
    main.append('div').attr('class', 'gsl-legend-x')
    main.append('div').attr('class', 'gsl-legend-y')
    main.append('div').attr('class', 'gsl-scene')
    main.append('div').attr('class', 'gsl-vertical-scroll')
    main.append('div').attr('class', 'gsl-horizontal-scroll')
    _chart.createContainers()
    d3.select('#gsl-main').select('#gsl-legend-y-g').raise()
  }

  gsl.addSearchByKeywordsListeners = function () {
    document
      .getElementById('searchKeywordButton')
      .addEventListener('click', function () {
        const text = d3.select('#searchInput').node().value
        gsl.searchByKeyword(text)
      })
    document
      .getElementById('searchEntityButton')
      .addEventListener('click', function () {
        const text = d3.select('#searchInput').node().value
        gsl.searchByEntity(text)
      })
    d3.selectAll('#clearSearchButton').on('click', function () {
      const text = d3.select('#searchInput').node().value
      gsl.clearSearchByKeyword()
    })
  }

  gsl.addSearchKeywordBox = function (text, stats, type) {
    const regex = new RegExp(' ', 'g')
    const text_no_spaces = text.replace(regex, '-')
    const div = d3
      .select('#searchKeywordsContainer')
      .append('div')
      .attr('id', 'search-kw-' + text_no_spaces + '_' + type)
      .style('padding', '3px')
      .style('margin', '3px')
      .style('background-color', () => {
        if (type == 'keyword') {
          return '#d94801'
        } else {
          return '#3182bd'
        }
      })
      .style('display', 'inline-block')
      .style('color', '#fff')
    div
      .append('div')
      .style('float', 'right')
      .style('display', 'inline-block')
      .style('margin-left', '3px')
      .append('a')
      .attr('id', 'search-kw-x-' + text_no_spaces)
      .on('click', (d) => gsl.removeSearchKeyword(text, type))
      .on('mouseover', (d) => {
        d3.select('#search-kw-x-' + text_no_spaces).style(
          'text-decoration',
          'underline',
        )
      })
      .on('mouseout', (d) => {
        d3.select('#search-kw-x-' + text_no_spaces).style(
          'text-decoration',
          'none',
        )
      })
      .style('color', '#fff')
      .html('x')
    var statsString = ''
    for (let type in stats) {
      statsString += ', ' + stats[type] + ' ' + type
    }
    if (statsString == '') {
      statsString = '  0 results'
    }
    div
      .append('div')
      .style('display', 'inline-block')
      .html(text + ' (' + statsString.substring(2) + ')')
  }

  //////////////////////////////////////////////////////////////////////////////
  // DRAWING AND CLEAN
  //////////////////////////////////////////////////////////////////////////////
  gsl.draw = function () {
    d3.select('#gsl-scene-svg').attr('visibility', 'hidden')
    if (_data.narrative().scenes().length == 0) {
      _chart.drawNoData(_backgroundInteractions)
    } else {
      _chart.draw(
        _data.narrative(),
        _entityInteractions,
        _backgroundInteractions,
        _scenesInteractions,
        _data.map(),
        _rotation
      )
    }
    gsl.updateSelected()
    _chart.resetScrollbars()
    d3.select('#gsl-scene-svg').attr('visibility', 'visible')
    setTimeout(() => {
      _chart.axes().yAxis().updateScroll()
    }, 100);
    return gsl
  }

  gsl.reset = function () {
    d3.select('.gsl-container').selectAll('*').remove()
    _uiSelectors.reset()
    return gsl
  }

  //////////////////////////////////////////////////////////////////////////////
  // UPDATE
  //////////////////////////////////////////////////////////////////////////////
  gsl.updateNarrative = function () {
    const selections = _uiSelectors.getAllSelections()
    const datasetName = _data.datasetName()
    var url = getScenesURL(selections, datasetName)
    d3.json(url)
      .then(function (data) {
        gsl.constructScenes(data)
        _data.map(data.map)
        _data.setNodesDict(data.nodes_dict)
        const newScenes = data.scenes
        const nScenes = newScenes.length
        const chartDimensions = _chart.getDimensions(nScenes)
        _chart.size(chartDimensions[0], chartDimensions[1])
        _data.updateNarrative(newScenes, _chart.width, _chart.height)
        _chart.updateSizeNarrative(_data.narrative())
        gsl.draw()
      })
  }
  gsl.constructScenes = function (data) {
    console.log('_highlighted_location')
    console.log(_highlighted_location)
    const nodes = data.nodes_dict
    const firstKey = Object.keys(nodes)[0]
    const extraNode = JSON.parse(JSON.stringify(nodes[firstKey]))
    extraNode.id = 'non-mentioned-location'
    extraNode.name = 'non-mentioned-location'
    extraNode.type = 'locations'
    extraNode.related_edges = []
    for (let dim in extraNode.related_nodes) {
      extraNode.related_nodes[dim] = []
    }
    const locations = {}

    for (let nId in nodes) {
      const node = nodes[nId]
      if (!node.aggregated_nodes_ids) continue
      nodes[nId].aggregated_nodes = []
      node.aggregated_nodes_ids.forEach(aggrId => {
        nodes[nId].aggregated_nodes.push(nodes[aggrId])
      })
      if (nodes[nId].type == 'locations') {
        locations[node.name] = node
      }
    }
    const rawScenes = data.scenes
    rawScenes.forEach(e => {
      e.characters = []
      e.charactersIds.forEach(c_id => {
        if (nodes[c_id] != undefined)
          e.characters.push(nodes[c_id])
      })
      e.keyNode = nodes[e.keyNode['id']]
      e.locations = []
      e.edge.nodes_ids.forEach(nId => {
        const node = nodes[nId]
        if (node.type == 'locations') {
          e.locations.push(node)
        }
      })
    })
    data.map.features.forEach(f => {
      if (f.properties.name in locations) {
        f.properties.node = locations[f.properties.name]
      } else {
        const extraNode = JSON.parse(JSON.stringify(nodes[firstKey]))
        extraNode.id = f.properties.name
        extraNode.name = f.properties.name
        extraNode.type = 'locations'
        extraNode.related_edges = []
        for (let dim in extraNode.related_nodes) {
          extraNode.related_nodes[dim] = []
        }
        f.properties.node = extraNode
      }
      if (f.properties.name == _highlighted_location) {
        f.properties.highlighted = true
        console.log(f.properties.name)
      } else {
        f.properties.highlighted = false
      }
    })
  }

  //////////////////////////////////////////////////////////////////////////////
  // REACT TO UI SELECTORS
  //////////////////////////////////////////////////////////////////////////////
  gsl.changeDimension = function () {
    _uiSelectors.updateAggregators()
    gsl.updateNarrative()
  }

  gsl.changeAggregation = function () {
    gsl.updateNarrative()
  }

  //////////////////////////////////////////////////////////////////////////////
  // REACT TO SELECTION
  //////////////////////////////////////////////////////////////////////////////
  gsl.select = function (entity) {
    _data.toggleSelection(entity)
    gsl.updateSelected()
  }

  gsl.updateSelected = function () {
    _chart.updateSelected(_data, _uiSelectors.getAllSelections())
  }

  gsl.resetSelected = function () {
    _data.resetSelected()
    gsl.updateSelected()
  }

  //////////////////////////////////////////////////////////////////////////////
  // DATASET SELECTOR
  //////////////////////////////////////////////////////////////////////////////
  gsl.changeDataset = function (name) {
    d3.json('/geostorylines/get_data/?q=' + name).then((data) => {
      gsl.data(name, data).reset().show()
      d3.select('#dataset-selector-label').node().innerHTML = name
    })
  }
  return gsl
}
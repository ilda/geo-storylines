import sceneStyle from '../vis/style/scenes.js'
import aggregatorFactory from '../aggregators/factory.js'
import selectionManager from './selection.js'

export default function data() {
  var data = {} 

  // DATA NEW
  var _nodesDict
  var _datasetName
  var _dimensions

  // NARRATIVES
  var _narrative

  // SELECT
  var _selection

  // AGGREGATION
  var _aggregators = {}

  // GEO INFO
  var _map
  var _coords

  /////////////////////////////////////////////////////////////////////////////
  // INIT
  data.init = function (name, dimensions) {
    _datasetName = name
    _dimensions = dimensions
    data.createAggregators()
    _selection = selectionManager().init()
    return data
  }

  /////////////////////////////////////////////////////////////////////////////
  // GETTERS ORIGINAL DATA
  data.getOriginalNode = function (id) {
    if (id in _nodesDict) {
      return _nodesDict[id]
    }
    return undefined
  }

  data.datasetName = function () {
    return _datasetName
  }

  data.setNodesDict = function(aDict){
    _nodesDict = aDict
  }

  /////////////////////////////////////////////////////////////////////////////
  // GETTERS FOR SELECTORS
  data.dimensions = function () {
    return _dimensions
  }

  data.aggregations = function () {
    const r = {}
    for (let d in _dimensions) {
      r[d] = _aggregators[d].getLevels()
    }
    return r
  }

  /////////////////////////////////////////////////////////////////////////////
  // AGGREGATION
  data.createAggregators = function () {
    for (let d in _dimensions) {
      _aggregators[d] = aggregatorFactory(d)
    }
  }

  data.getAggregator = function (typeEntity) {
    return _aggregators[typeEntity]
  }

  data.getAllAggregators = function () {
    return _aggregators
  }

  data.isXDimAggregated = function(xDim, xAggr){
    return _aggregators[xDim].ignoredLevels().indexOf(xAggr) < 0
  }

  /////////////////////////////////////////////////////////////////////////////
  // NARRATIVE
  data.narrative = function (newNarrative) {
    if (newNarrative == undefined) {
      return _narrative
    } else {
      _narrative = newNarrative
      return data
    }
  }
  data.createNarrative = function (newScenes, width, height) {
    const narrative = d3
      .narrative()
      .scenes(newScenes)
      .size([width, height])
      .pathSpace(sceneStyle('pathSpace'))
      .groupMargin(sceneStyle('groupMargin'))
      .labelSize(sceneStyle('labelSize'))
      .scenePadding([
        0,
        sceneStyle('sceneWidth'), 
        0,
        sceneStyle('sceneWidth'), 
      ])
      .labelPosition('left')
      .layout()
    // Postprocessing for the y position
    narrative.scenes().forEach(e => {
      const n =  sceneStyle('sceneHeight')/(e.appearances.length + 1)
      const h = 25 * e.appearances.length 
      const shift = sceneStyle('sceneWidth')/2 - h/2
      e.appearances.forEach((a, i) => {
        a.y = shift + (i + 1) * 25
        a.x = 10
      })
    })
    return narrative
  }

  data.updateNarrative = function (newScenes, width, height) {
    _narrative = data.createNarrative(newScenes, width, height)
  }

  /////////////////////////////////////////////////////////////////////////////
  // SELECTION
  data.isSelectionActive = function () {
    return _selection.isSelectionActive()
  }

  data.toggleSelection = function (entity) {
    _selection.toggleSelection(entity)
    return data
  }

  data.toggleSeveralEntities = function (text, select) {
    _selection.toggleSeveralEntities(text, select)
    return data
  }

  data.resetSelected = function () {
    _selection.resetSelected()
  }

  data.isSceneSelected = function (scene, selections) {
    return _selection.isSceneSelected(scene, selections)
  }

  data.isSceneRelatedSelected = function (scene) {
    return _selection.isSceneRelatedSelected(scene)
  }

  data.isEntitySelected = function (node) {
    return _selection.isEntitySelected(node)
  }

  data.isEntityRelatedSelected = function (node, selections) {
    return _selection.isEntityRelatedSelected(node, selections)
  }

  data.isSelected = function (entityId) {
    return _selection.isSelected(entityId)
  }

  data.isRelatedSelected = function (entityId) {
    return _selection.isRelatedSelected(entityId)
  }

  /////////////////////////////////////////////////////////////////////////////
  // MAP
  data.map = function(geojson){
    if (geojson == undefined) {
      return _map
    } else {
      _map = geojson
      return data
    }
  }
  
  return data
}

/*export default function scenesStyle(characteristic) {
  var style = {
    //labelSize: [10, 30],
    labelSize: [0, 0],
    pathSpace: 90,
    groupMargin: 10,
    leftPadding: 20,
    rightPadding: 20,
    // If you change this value, don't forget to
    // update the one ine narrative.js, 
    // line 268
    sceneWidth: 200,
    sceneHeight: 200
  }
  return style[characteristic]
}*/



export default function scenesStyle(characteristic) {
  var style = {
    //labelSize: [10, 30],
    labelSize: [0, 0],
    pathSpace: 130,
    groupMargin: 20,
    leftPadding: 70,
    rightPadding: 70,
    sceneWidth: 325,
    sceneHeight: 325
  }
  return style[characteristic]
}

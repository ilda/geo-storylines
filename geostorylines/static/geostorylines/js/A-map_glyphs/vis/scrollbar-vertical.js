import { getTranslateCoordinates } from '../gsl-utils.js'
import logInteraction from '../interactions/log.js'

export default function verticalScrollbar() {
  var scrollbar = {}
  var _domain
  var _range
  var _scaleChart
  var _scaleContainer
  var _delta
  var _size
  var _width
  var _chart
  var _lines = []
  var _openSceneRects = []
  var _filterRects = []
  scrollbar.domain = function (value) {
    _domain = value
    return scrollbar
  }
  scrollbar.chart = function (chart) {
    _chart = chart
    return scrollbar
  }
  scrollbar.createContainers = function () {
    var w = $('.gsl-vertical-scroll').width()
    var h = $('.gsl-vertical-scroll').height()
    d3.select('.gsl-vertical-scroll')
      .append('svg')
      .attr('id', 'svgVerticalScrollbar')
      .attr('width', _width)
      .attr('height', _range)
      .style('background-color', '#f0f0f0')
      .append('g')
      .attr('id', 'markersContainer')
    d3
      .select('#svgVerticalScrollbar')
      .append('rect')
      .attr('class', 'slider')
      .attr('width', _width)
      .attr('height', _range)
      .style('fill', '#737373')
      .style('opacity', 0.4)
      .attr('y', 0)

    var dragHandler = d3.drag()
      .on('start', function () {
        var current = d3.select(this)
        _delta = current.attr('y') - d3.event.y
        const interaction = {}
        interaction.type = 'scroll-start'
        interaction.argument = 'vertical'
        logInteraction(interaction)
      })
      .on('drag', function () {
        var y = d3.event.y + _delta
        scrollbar.move(y)
      })
      .on('end', function () {
        const interaction = {}
        interaction.type = 'scroll-end'
        interaction.argument = 'vertical'
        logInteraction(interaction)
      })
    dragHandler(d3.select('#svgVerticalScrollbar').selectAll('rect'))
  }
  scrollbar.move = function (y) {
    y = Math.max(-(_size/4), y)
    y = Math.min(_range - (_size/4), y)
    d3.select('#svgVerticalScrollbar').select('.slider').attr('y', y)
    var node = d3.selectAll('#gsl-legend-y-g').node()
    var t = getTranslateCoordinates(node)
    d3
      .select('#gsl-legend-y-svg')
      .selectAll('#gsl-legend-y-g')
      .attr('transform', 'translate(' + t.x + ',' + -_scaleContainer(y) + ')')
    var node = d3.selectAll('#gsl-main').node()
    var t = getTranslateCoordinates(node)
    d3
      .selectAll('#gsl-main')
      .attr('transform', 'translate(' + t.x + ',' + -_scaleContainer(y) + ')')
    _chart.axes().yAxis().updateScroll()
    return scrollbar
  }
  scrollbar.moveDelta = function (delta) {
    const y = d3.selectAll('#svgVerticalScrollbar')
      .selectAll('.slider')
      .attr('y')
    scrollbar.move(Number(y) + delta)
    return scrollbar
  }
  scrollbar.updateSize = function () {
    _range = $('.gsl-vertical-scroll').height()
    _width = $('.gsl-vertical-scroll').width()
    _scaleContainer = d3.scaleLinear().domain([0, _range]).range([-25, _domain])
    _scaleChart = d3.scaleLinear().domain([-25, _domain]).range([0, _range])
    _size = _scaleChart(_range)
    if (_size > _range) {
      _size = _range
    }
    d3
      .selectAll('#svgVerticalScrollbar')
      .attr('width', _width)
      .attr('height', _range)
    d3
      .select('#svgVerticalScrollbar')
      .select('.slider')
      .attr('height', _size)
      .attr('width', _width)
      .style('fill', '#737373')
      .style('opacity', 0.4)
    /*if (_size == _range) {
      d3
        .select('#svgVerticalScrollbar')
        .select('.slider')
        .style('visibility', 'hidden')
    } else {
      d3
        .select('#svgVerticalScrollbar')
        .select('.slider')
        .style('visibility', 'visible')
    }*/
    return scrollbar
  }
  scrollbar.addLine = function (y, color) {
    const newLine = {}
    newLine.y = y
    newLine.color = color
    _lines.push(newLine)
    d3
      .select('#svgVerticalScrollbar')
      .select('#markersContainer')
      .append('line')
      .attr('class', 'vScrollLine')
      .attr('x1', 0)
      .attr('x2', _width / 2 - 1)
      .attr('y1', _scaleChart(y))
      .attr('y2', _scaleChart(y))
      .style('stroke', color)
      .style('stroke-width', 4)
    return scrollbar
  }
  scrollbar.addOpenSceneRect = function (y, height, color) {
    const newRect = {}
    newRect.y = y
    newRect.height = height
    newRect.color = color
    _openSceneRects.push(newRect)
    d3
      .select('#svgVerticalScrollbar')
      .select('#markersContainer')
      .append('rect')
      .attr('class', 'vScrollRect')
      .attr('x', _width / 2 + 1)
      .attr('y', _scaleChart(y))
      .attr('width', _width / 2)
      .attr('height', _scaleChart(height))
      .style('fill', color)
    return scrollbar
  }

  scrollbar.addFilterRect = function (y, color) {
    const newRect = {}
    newRect.y = y
    newRect.color = color
    _filterRects.push(newRect)
    d3
      .select('#svgVerticalScrollbar')
      .select('#markersContainer')
      .append('rect')
      .attr('class', 'vScrollRect')
      .attr('x', _width/4)
      .attr('y', _scaleChart(y))
      .attr('width', _width / 2)
      .attr('height', 4)
      .style('fill', '#f0f0f0')
      .style('stroke', color)
      .style('stroke-width', 1)
    return scrollbar
  }

  
  scrollbar.clean = function () {
    _lines = []
    _openSceneRects = []
    _filterRects = []
    scrollbar.cleanSVG()
    return scrollbar
  }
  scrollbar.cleanSVG = function () {
    d3.selectAll('.vScrollLine').remove()
    d3.selectAll('.vScrollRect').remove()
    return scrollbar
  }
  scrollbar.reDrawSelected = function () {
    scrollbar.cleanSVG()
    _lines.forEach(l => {
      scrollbar.addLine(l.y, l.color)
    })
    _openSceneRects.forEach(r => {
      scrollbar.addOpenSceneRect(r.y, r.height, r.color)
    })
    _filterRects.forEach(r => {
      scrollbar.addFilterRect(r.y, r.color)
    })
    return scrollbar
  }
  scrollbar.reset = function () {
    d3.select('#svgVerticalScrollbar').select('.slider').attr('y', 0)
    _delta = 0
  }

  return scrollbar
}
import * as style from './style/stories.js'
import sceneStyle from './style/scenes.js'
import { replaceAll, cleanName } from '../gsl-utils.js'

export default function scenes() {
  var scenes = {}

  scenes.createContainers = function (level) {
    d3.select('#gsl-main')
      .append('g')
      .attr('id', 'gsl-scenes-' + level)
  }
  scenes.createContainersOpenScenes = function (level) {
    d3.select('#gsl-main')
      .append('g')
      .attr('id', 'gsl-scenes-open-' + level)
  }

  scenes.draw = function (narrative, scenesInteractions, entityInteractions, map, rotation) {
    drawLayer0(narrative)
    drawLayer1(narrative, scenesInteractions, entityInteractions, map, rotation)
    return scenes
  }

  scenes.clean = function () {
    d3.select('#gsl-scenes-0').selectAll('*').remove()
    d3.select('#gsl-scenes-1').selectAll('*').remove()
    d3.select('#gsl-scenes-open-0').selectAll('*').remove()
    d3.select('#gsl-scenes-open-1').selectAll('*').remove()
    return scenes
  }
  scenes.update = function (narrative, animationDuration) {
    const promises = []
    var transition = d3
      .selectAll('#gsl-scenes-0')
      .selectAll('.scene')
      .data(narrative.scenes().filter((d) => !d.isFake))
      .transition()
      .duration(animationDuration)
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x) + sceneStyle('leftPadding')
        if (d.offsetY) {
          y = Math.round(d.y) + 0.5 + d.offsetY
        } else {
          y = Math.round(d.y) + 0.5
        }
        return 'translate(' + [x, y] + ')'
      })
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = d3
      .selectAll('#gsl-scenes-1')
      .selectAll('.scene')
      .data(narrative.scenes().filter((d) => !d.isFake))
      .transition()
      .duration(animationDuration)
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x) + sceneStyle('leftPadding')
        if (d.offsetY) {
          y = Math.round(d.y) + 0.5 + d.offsetY
        } else {
          y = Math.round(d.y) + 0.5
        }
        return 'translate(' + [x, y] + ')'
      })
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = d3
      .selectAll('#gsl-scenes-1')
      .selectAll('.open-scene')
      .transition()
      .duration(animationDuration)
      .attr('transform', (d) => 'translate(' + (d.x + 5) + ',' + d.y + ')')
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = d3
      .selectAll('#gsl-scenes-open-1')
      .selectAll('.open-scene')
      .transition()
      .duration(animationDuration)
      .attr('transform', d => 'translate(' + (d.x + 5) + ',' + d.y + ')')
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = d3
      .selectAll('#gsl-scenes-open-0')
      .selectAll('.open-scene')
      .transition()
      .duration(animationDuration)
      .attr('transform', d => 'translate(' + (d.x + 5) + ',' + d.y + ')')
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = d3
      .selectAll('#gsl-lines-open-inside-0')
      .selectAll('.open-scene')
      .transition()
      .duration(animationDuration)
      .attr('transform', d => 'translate(' + (d.x + 5) + ',' + (d.y) + ')')
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    return promises
  }
  scenes.updateSelected = function (data, selections) {
    // TODO: THE UPDATE OF THE OPEN SCENES SHOULD BE SOMEWHERE ELSE
    // TODO: KNOWING WHEN IF THERE ARE LEVELS TO IGNORE SHOULD NOT BE HERE
    const xDim = selections.dim.x
    const xAggr = selections.aggr.x
    const isXDimAggr = data.isXDimAggregated(xDim, xAggr)

    const isSelectionActive = data.isSelectionActive()
    // CLOSED SCENES
    d3.selectAll('.scene')
      .selectAll('.sceneRect')
      .style('stroke', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        return style.getSceneRectColor(isSelected)
      })
      .style('opacity', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        const isRelatedSelected = data.isSceneRelatedSelected(d)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-width', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        return style.getSceneRectStrokeWidth(isSelected)
      })
      d3.selectAll('.scene')
      .selectAll('.mini-map-parent')
      .style('opacity', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        const isRelatedSelected = data.isSceneRelatedSelected(d)
        return style.getOpacityMap(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      d3.selectAll('.scene')
      .selectAll('.mini-map')
      .style('opacity', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        const isRelatedSelected = data.isSceneRelatedSelected(d)
        return style.getOpacityMap(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })

    d3.selectAll('.scene')
      .selectAll('text')
      .style('opacity', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        const isRelatedSelected = data.isSceneRelatedSelected(d)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      d3.selectAll('.scene')
      .style('opacity', (d) => {
        const isSelected = data.isSceneSelected(d, selections)
        const isRelatedSelected = data.isSceneRelatedSelected(d)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })

      d3.selectAll('.scene')
      .selectAll('.appearance')
      .attr('r', d => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isSceneSelected = data.isSceneSelected(d.scene, selections)
        if(isSceneSelected){
          return style.getSceneCircleRadius(isSelected)
        } else {
          return style.getSceneCircleRadius(false)
        }
        
      })
      .style('fill',  d => {
        const isSceneSelected = data.isSceneSelected(d.scene, selections)
        if(isSceneSelected){
          return style.getSceneCircleStrokeColor()
        } else {
          const isSelected = data.isEntitySelected(d.character, selections)
          return style.getSceneCircleFillColor(isSelected, d.isTerm)
        }        
      })
      .style('stroke', (d) => {
        const isSelected = data.isEntitySelected(d.character, selections)
        return style.getSceneCircleFillColor(isSelected, d.isTerm)
      })
      .style('opacity', (d) => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.character,
          selections,
        )
        return style.getSceneCircleOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-width', (d) => {
        const isSelected = data.isEntitySelected(d.character, selections)
        return style.getSceneRectStrokeWidth(isSelected)
      })
    // OPEN SCENES INNER MARKERS
    d3.selectAll('.scene')
      .selectAll('.mini-map')
      .selectAll('path')
      .style('stroke', (d) => {
        if (d.properties.highlighted) return 'red'
        const isSelected = data.isEntitySelected(d.properties.node, selections)
        return style.getSceneCircleFillColor(isSelected)
      })
      .style('fill-opacity', (d) => {
        //if (d.properties.highlighted) return 1
        const isSelected = data.isEntitySelected(d.properties.node, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.properties.node,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-opacity', d => {
        if (d.properties.highlighted) return 'red'
        const isSelected = data.isEntitySelected(d.properties.node, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.properties.node,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-width', (d) => {
        if (d.properties.highlighted) return '3'
        const isSelected = data.isEntitySelected(d.properties.node, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.properties.node,
          selections,
        )
        if (isSelectionActive && isSelected) return 3
        return 1
      })

    d3
      .selectAll('.scene')
      //.selectAll('.mini-map')
      .selectAll('.scene-no-locations')
      .style('stroke', '#f0f0f0')
    // OPEN SCENES INNER TEXT
    d3.selectAll('.openStoryContainer')
      .selectAll('.scene-nested')
      .selectAll('text')
      .style('fill', (d) => {
        if (isXDimAggr) {
          const isSelected = data.isEntitySelected(d.keyNodeX, selections)
          return style.getTextColor(isSelected)
        } else {
          const isSelected = data.isEntitySelected(d.keyNodeZ, selections)
          return style.getTextColor(isSelected)
        }
      })
      .style('font-weight', (d) => {
        if (isXDimAggr) {
          const isSelected = data.isEntitySelected(d.keyNodeX, selections)
          return style.getTextFontWeight(isSelected)
        } else {
          const isSelected = data.isEntitySelected(d.keyNodeZ, selections)
          return style.getTextFontWeight(isSelected)
        }
      })
    // OPEN SCENES INNER AXIS
    d3.selectAll('g.open-scene')
      .selectAll('.xDimLegend')
      .selectAll('text')
      .style('fill', (d) => {
        const isSelected = data.isEntitySelected(d.keyNodeZ, selections)
        return style.getTextColor(isSelected)
      })
      .style('font-weight', (d) => {
        const isSelected = data.isEntitySelected(d.keyNodeZ, selections)
        return style.getTextFontWeight(isSelected)
      })


    return scenes
  }

  function drawLayer0(narrative) {
    var sceneG = d3
      .select('#gsl-scenes-0')
      .selectAll('.scene')
      .data(narrative.scenes().filter((d) => !d.isFake))
      .enter()
      .append('g')
      .attr('class', 'scene')
      .attr('id', (d) => 'scene_' + d.id)
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x) + sceneStyle('leftPadding')
        if (d.offsetY) {
          y = Math.round(d.y) + 0.5 + d.offsetY
        } else {
          y = Math.round(d.y) + 0.5
        }
        return 'translate(' + [x, y] + ')'
      })


    sceneG
      .append('rect')
      .attr('x', -20)
      .attr('y', -20)
      .attr('width', sceneStyle('sceneWidth') + 40)
      .attr('height', (d) => sceneStyle('sceneHeight') + 40)
      .attr('rx', 3)
      .attr('ry', 3)
      .style('fill', '#f7f7f7')
      .style('stroke', 'none')
      .style('opacity', 0.7)

    // LEFT
    /*sceneG
      .append('rect')
      .attr('x', -10)
      .attr('y', d => (sceneStyle('sceneHeight') / 2) - ((d.appearances.length * 25) / 2))
      .attr('width', 12)
      .attr('height', d => ((d.appearances.length) * 25) + 15)
      .attr('rx', 3)
      .attr('ry', 3)
      .style('fill', '#f7f7f7')
      .style('stroke', 'none')
      .style('opacity', 0.7)
    // RIGHT
    sceneG
      .append('rect')
      .attr('x', sceneStyle('sceneWidth'))
      .attr('y', d => (sceneStyle('sceneHeight') / 2) - ((d.appearances.length * 25) / 2))
      .attr('width', 12)
      .attr('height', d => ((d.appearances.length) * 25) + 15)
      .attr('rx', 3)
      .attr('ry', 3)
      .style('fill', '#f7f7f7')
      .style('stroke', 'none')
      .style('opacity', 0.7)*/
  }

  function drawLayer1(narrative, scenesInteractions, entityInteractions, map, rotation) {
    var sceneG = d3
      .select('#gsl-scenes-1')
      .selectAll('.scene')
      .data(narrative.scenes().filter((d) => !d.isFake))
      .enter()
      .append('g')
      .attr('class', 'scene')
      .attr('id', (d) => 'scene_' + d.id)
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x) + sceneStyle('leftPadding')
        if (d.offsetY) {
          y = Math.round(d.y) + 0.5 + d.offsetY
        } else {
          y = Math.round(d.y) + 0.5
        }
        return 'translate(' + [x, y] + ')'
      })
    drawRelationshipsContainers(sceneG, scenesInteractions, entityInteractions, map, rotation)
    drawRelationshipsInnerMarkers(sceneG)
    return scenes
  }

  function drawRelationshipsContainers(sceneG, scenesInteractions, entityInteractions, map, rotation) {
    var sceneRect = sceneG
      .append('rect')
      .attr('class', (d) => 'sceneRect childScene_' + d.id)
      .attr('id', (d) => 'sceneRect_' + d.id)
      .attr('sceneId', (d) => d.id)
      .attr('width', sceneStyle('sceneWidth'))
      .attr('height', sceneStyle('sceneHeight'))
      .attr('y', 0)
      .attr('x', 0)
      .attr('rx', 3)
      .attr('ry', 3)
      .style('fill', 'white')
      .style('fill-opacity', 1)
      .style('stroke-dasharray', (d) => {
        if (d.nNested > 0) {
          return null
        } else {
          return '3, 3'
        }
      })

    sceneG
      .append('text')
      .attr('class', 'nNested')
      .text((d) => {
        if (d.nNested > 1) {
          return d.nNested
        } else {
          return ''
        }
      })
      .attr('transform', 'translate(' + (sceneStyle('sceneWidth') / 2) + ',-6)')

    const size = sceneStyle('sceneWidth') - 5;

    var projection = d3.geoMercator()
      .rotate([0, 0, rotation])
      .fitExtent([[5, 5], [size, size]], map);

    const miniMap = sceneG
      .append('g')
      .attr('class', 'mini-map')
      .attr('id', (d) => { return 'mini-map-' + d.id })
    miniMap
      .selectAll('path')
      .data(() => map.features.filter(f => f.properties.hierarchical_level == 'child'))
      .enter()
      .append('path')
      .attr('d', d3.geoPath().projection(projection))
      .attr('id', d => cleanName(d.properties.name))
      .style('stroke', '#bdbdbd')
      .attr('stroke-width', '1px')
      .style('stroke-dasharray', 2)
      .style('fill', '#d9d9d9')
      .style('cursor', 'pointer')
      .style('pointer-events', 'visible')
      .on('mouseover', d => entityInteractions.mouseOver(d.properties.name))
      .on('mouseout', d => entityInteractions.mouseOut())
      .on('click', function (d) {
        entityInteractions.click(d.properties.node, 'map', d.properties.name)
        d3.select(this).raise()
      })

    const miniMapParent = sceneG
      .append('g')
      .attr('class', 'mini-map-parent')
      .attr('id', (d) => { return 'mini-map-parent-' + d.id })
    miniMapParent
      .selectAll('path')
      .data(() => map.features.filter(f => f.properties.hierarchical_level == 'parent'))
      .enter()
      .append('path')
      .attr('d', d3.geoPath().projection(projection))
      .style('stroke', d => d.properties.color)
      .style('stroke', '#000')
      .style('stroke-width', '2px')
      .style('fill', 'none')
      .style('fill-opacity', 0)
      .style('stroke-opacity', 1)


    sceneG
      .each(d => {
        d.locations.forEach(l => {
          d3
            .select('#mini-map-' + d.id)
            .selectAll('path#' + cleanName(l.name))
            //.style('fill', '#737373')
            //.style('fill', 'url(#circles-1) #000')
            .style('fill', '#d94801')
            .style('stroke', '#252525')
        })
        if (d.locations.length == 0) {
          d3
            .select('#mini-map-' + d.id)
            .selectAll('path')
            //.attr('class', 'scene-no-locations')
          d3
            .select('#mini-map-parent-' + d.id)
            .selectAll('path')
            //.attr('class', 'scene-no-locations')
        }
      })

    return sceneRect
  }

  function drawRelationshipsInnerMarkers(sceneG) {
    // LEFT
    sceneG
      .append('rect')
      .attr('class', 'sceneRect')
      .attr('x', -8)
      .attr('y', d => (sceneStyle('sceneHeight') / 2) - ((d.appearances.length * 25) / 2))
      .attr('width', 7)
      .attr('height', d => ((d.appearances.length) * 25) + 25)
      .style('fill', 'white')
      .style('stroke', 'grey')
      .attr('rx', 3)
      .attr('ry', 3)

    sceneG
      .selectAll('.appearanceLeft')
      .data((d) => d.appearances)
      .enter()
      .append('circle')
      .attr('class', 'appearance appearanceLeft')
      .attr('id', (d) => 'sceneCircle_' + d.character.id)
      .attr('parentScene', (d) => { return d.scene.id })
      .attr('typeDom', 'scene')
      .attr('cx', '-5px')
      .attr('cy', (d) => d.y)
      .attr('r', 4)

    // RIGHT
    sceneG
      .append('rect')
      .attr('class', 'sceneRect')
      .attr('x', sceneStyle('sceneWidth'))
      .attr('y', d => (sceneStyle('sceneHeight') / 2) - ((d.appearances.length * 25) / 2))
      .attr('width', 7)
      .attr('height', d => ((d.appearances.length) * 25) + 25)
      .style('fill', 'white')
      .style('stroke', 'grey')
      .attr('rx', 3)
      .attr('ry', 3)
    sceneG
      .selectAll('.appearanceRight')
      .data((d) => d.appearances)
      .enter()
      .append('circle')
      .attr('class', 'appearance appearanceRight')
      .attr('id', (d) => 'sceneCircle_' + d.character.id)
      .attr('parentScene', (d) => { return d.scene.id })
      .attr('typeDom', 'scene')
      .attr('cx', sceneStyle('sceneWidth') + 4)
      .attr('cy', (d) => d.y)
      .attr('r', 4)


  }
  return scenes
}

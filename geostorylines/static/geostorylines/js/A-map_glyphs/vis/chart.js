import axes from './axes.js'
import stories from './stories.js'
import scrollbars from './scrollbars.js'
import scenesStyle from './style/scenes.js'

export default function chart() {
  var chart = {
    width: 1000,
    height: 1000,
    padding: 100,
    left: 195
  }
  var _axes
  var _stories
  var _scrollbars
  chart.init = function () {
    _axes = axes()
    _stories = stories()
    _scrollbars = scrollbars().chart(chart)
    return chart
  }
  chart.size = function (width, height) {
    chart.width = width //+ chart.padding
    chart.height = height //+ chart.padding
    _scrollbars
      .verticalDomain(chart.height + chart.padding)
      .horizontalDomain(chart.width + chart.padding)
    return chart
  }
  chart.createContainers = function () {
    const svg = d3.select('.gsl-scene')
      .append('svg')
      .attr('id', 'gsl-scene-svg')
      .attr('width', chart.width + chart.padding)
      .attr('height', chart.height + chart.padding)
    // svg
    //   .append('defs')
    //   .append('pattern')
    //   .attr('id', 'circles-1')
    //   .attr('patternUnits', 'userSpaceOnUse')
    //   .attr('width', "10")
    //   .attr('height', "10")
    //   .append('image')
    //   .attr('xlink:href', "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc4JyBoZWlnaHQ9JzgnPgogIDxyZWN0IHdpZHRoPSc4JyBoZWlnaHQ9JzgnIGZpbGw9JyNmZmYnLz4KICA8cGF0aCBkPSdNMCAwTDggOFpNOCAwTDAgOFonIHN0cm9rZS13aWR0aD0nMC41JyBzdHJva2U9JyNhYWEnLz4KPC9zdmc+Cg==")
    //   .attr('x', "0")
    //   .attr('y', "0")
    //   .attr('width', "10")
    //   .attr('height', "10")
    //   < defs >
    //   <pattern id="circles-1" patternUnits="userSpaceOnUse" width="10" height="10">
    //     <image xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPScjZmZmJyAvPgogIDxjaXJjbGUgY3g9IjEiIGN5PSIxIiByPSIxIiBmaWxsPSIjMDAwIi8+Cjwvc3ZnPg=="
    //       x="0" y="0" width="10" height="10">
    //     </image>
    //   </pattern> 
    // </defs >
    svg
      .append('g')
      .attr('id', 'gsl-main')
    d3
      .select('body')
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0)
    _axes.createContainers()
    _stories.createContainers()
    _scrollbars.createContainers()
    return chart
  }
  chart.updateSizeNarrative = function (narrative) {
    const scenes = narrative.scenes()
    const nScenes = scenes.length
    const chartDimensions = chart.getDimensions(nScenes)
    chart.width = chartDimensions[0] //+ chart.padding
    chart.height = narrative.getGreatestYValue() //+ chart.padding
    chart.updateSize(chart.left)
    return chart
  }
  chart.updateSizeShift = function (narrative, width, height) {
    chart.width += width
    chart.height += height
    chart.updateSize(chart.left)
    return chart
  }
  chart.updateSize = function (left) {
    chart.height = Math.max(chart.height, 1145)
    d3.select('#gsl-scene-svg')
      .attr('width', chart.width + chart.padding)
      .attr('height', chart.height + chart.padding)
    _axes.updateSize(chart.width, chart.height + chart.padding, left)
    _scrollbars.updateSize(chart.width + chart.padding, chart.height + chart.padding + 100, left)
    return chart
  }
  chart.getDimensions = function (nScenes) {
    const w = 100 + nScenes * (scenesStyle('sceneWidth') + 2 * scenesStyle('leftPadding'))
    const h = 500 + nScenes * 1000
    return [w, h]
  }
  chart.stories = function () {
    return _stories
  }
  chart.axes = function () {
    return _axes
  }
  chart.draw = function (narrative, entityInteractions, backgroundInteractions, scenesInteractions, map, rotation) {
    chart.clean()
    d3
      .select('#gsl-main')
      .attr('transform', 'translate(' + (-195) + ',25)')
    d3
      .select('#gsl-scene-svg')
      .style('background-color', '#f7f7f7')
      .on('click', d => backgroundInteractions.click())
    _axes.draw(narrative, chart.height + chart.padding, entityInteractions, backgroundInteractions)
    _stories.draw(narrative, entityInteractions, scenesInteractions, map, rotation)
    return chart
  }
  chart.update = function (narrative, animationDuration) {
    const promises_stories = _stories.update(narrative, animationDuration)
    const promises_axes = _axes.update(narrative, animationDuration)
    return promises_stories.concat(promises_axes)
  }
  chart.clean = function () {
    _stories.clean()
    _axes.clean()
    d3.select('#gsl-main').selectAll('.intro').remove()
    d3.select('#gsl-main').selectAll('.no-data').remove()
    return chart
  }
  chart.updateSelected = function (data, selections) {
    _stories.updateSelected(data, selections)
    _axes.updateSelected(data, selections)
    _scrollbars.updateSelected(data, selections)
    return chart
  }
  chart.drawNoData = function (backgroundInteractions) {
    chart.clean()
    const width = $('.gsl-scene').width()
    const height = $('.gsl-scene').height()
    d3.select('#gsl-main')
      .attr('transform', 'translate(' + (-chart.left) + ',25)')
    d3.select('#gsl-scene-svg')
      .attr('width', width + chart.left)
      .attr('height', height)
    d3.select('#gsl-legend-y-svg')
      .attr('width', chart.left)
      .attr('height', height)
    d3.select('#scene_yLegend')
      .attr('width', chart.left)
    d3.select('#scene_yLegend_noreal')
      .attr('width', chart.left)
    _axes.drawNoData(width, height, chart.left, backgroundInteractions)
    d3
      .select('#gsl-main')
      .append('text')
      .text('No data')
      .attr('class', 'no-data')
      .attr('x', width / 2)
      .attr('y', height / 2)
      .style('font-size', '20px')

  }
  chart.resetScrollbars = function () {
    _scrollbars.reset()
  }
  return chart.init()
}
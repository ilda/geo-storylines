import * as style from './style/stories.js'
import { getTranslateCoordinates } from '../gsl-utils.js'

export default function yAxis() {
  var axis = {}
  var _width
  axis.init = function () {
    _width = 200
    return axis
  }
  axis.createContainers = function () {
    d3.select('.gsl-legend-y')
      .append('svg')
      .attr('id', 'gsl-legend-y-svg')
      .append('g')
      .attr('id', 'gsl-legend-y-g')
      .attr('transform', 'translate(0, 25)')
    d3
      .selectAll('#gsl-main')
      .append('g')
      .attr('id', 'gsl-legend-y-g')
    return axis
  }
  axis.updateSize = function (chartHeight, width) {
    _width = width
    const bbox = $('.gsl-legend-y').width()
    d3.select('#gsl-legend-y-svg')
      .attr('width', bbox)
      .attr('height', chartHeight)
    return axis
  }
  axis.clean = function () {
    d3.selectAll('#gsl-legend-y-g').selectAll('*').remove()
  }
  axis.draw = function (narrative, height, entityInteractions, backgroundInteractions) {
    d3
      .select('#gsl-legend-y-svg')
      .style('background-color', '#f7f7f7')
      .on('click', d => backgroundInteractions.click())

    d3.select('#gsl-legend-y-g').attr('transform', 'translate(0, 25)')

    // DRAW ENTITIES' LABELS
    var introG = d3.selectAll('#gsl-main')
      .select('#gsl-legend-y-g')
      .selectAll('.intro')
      .data(narrative.introductions().filter(d => !d.character.isFake))
      .enter()
      .append('g')
      .attr('class', 'intro')
      .attr('id', d => 'intro_' + d.character.id)
      .on('click', d => entityInteractions.click(d.character, 'yDimName', d.character.name))
      .attr('cursor', 'pointer')
      .style('pointer-events', 'visible')
      .attr('transform', function (d) {
        var x = Math.round(d.x)
        var y = Math.round(d.y)
        return 'translate(' + [x, y] + ')'
      })
    introG.append('rect')
      .attr('class', d => 'entityDom_' + d.character.id)
      .attr('typeDom', 'char')
      .attr('y', -4)
      .attr('x', -4)
      .attr('width', 4)
      .attr('height', 8)
    introG
      .append('text')
      .attr('text-anchor', 'end')
      .attr('y', '4px')
      .attr('x', '-8px')
      .text(d => d.character.name)
      .on('mouseover', d => entityInteractions.mouseOver(d.character.name))
      .on('mouseout', d => entityInteractions.mouseOut())

    // LEFT RECT
    d3.select('#gsl-legend-y-svg')
      .select('g')
      .selectAll('.intro').remove()
    var introGR = d3.select('#gsl-legend-y-svg')
      .select('#gsl-legend-y-g')
      .selectAll('.intro')
      .data(narrative.introductions().filter(d => !d.character.isFake))
      .enter()
      .append('g')
      .attr('class', 'intro')
      .attr('id', d => 'intro_' + d.character.id)
      .on('click', d => entityInteractions.click(d.character, 'yDimName', d.character.name))
      .attr('cursor', 'pointer')
      .style('pointer-events', 'visible')
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x)
        y = Math.round(d.y)
        return 'translate(' + [x, y] + ')'
      })
    introGR.append('rect')
      .attr('y', -4)
      .attr('x', -4)
      .attr('width', 4)
      .attr('height', 8)

    introGR
      .append('text')
      .attr('text-anchor', 'end')
      .attr('y', '4px')
      .attr('x', '-8px')
      .text(d => d.character.name)
      .on('mouseover', d => entityInteractions.mouseOver(d.character.name))
      .on('mouseout', d => entityInteractions.mouseOut())
    return axis
  }
  axis.drawNoData = function (width, height, backgroundInteractions) {
  }
  axis.update = function (narrative, animationDuration) {
    const transition = d3.selectAll('#gsl-legend-y-g')
      .selectAll('.intro')
      .data(narrative.introductions().filter(d => !d.character.isFake))
      .transition()
      .duration(animationDuration)
      .attr('transform', d => 'translate(' + [d.x, d.y] + ')')
    const promises = [axis.updateScroll(narrative, animationDuration)]
    if(!transition.empty()){
      // TODO: why this gives an error?
      //promises.push(transition.end())
    }
    
    return promises
  }
  axis.updateScroll = function (narrative, animationDuration = 0) {
    var node = d3.selectAll('#gsl-main').node()
    var t = getTranslateCoordinates(node)
    var scrollLeft = -t.x - 195
    var yLegendWidth = 195
    const transition = d3
      .select('#gsl-legend-y-svg')
      .selectAll('.intro')
      .transition()
      .duration(animationDuration)
      .attr('transform', function (d) {
        // TODO: OPTIMIZE THIS
        var xPos = 0
        var yPos = -1000
        var x = Math.round(d.x)
        if (x - scrollLeft >= yLegendWidth) {
          xPos = d.x - scrollLeft
        } else {
          xPos = yLegendWidth - 2
        }
        var y = Math.round(d.y)
        var charPaths = d3.selectAll('#gsl-lines-open-outside-0').selectAll('.link-g-' + d.character.id).selectAll('.link-ghost').nodes()
        charPaths = charPaths.concat(d3.selectAll('#gsl-lines-0').selectAll('.link-g-' + d.character.id + '[active=activated]').selectAll('.link-ghost').nodes())
        charPaths = charPaths.concat(d3.selectAll('#gsl-lines-open-inside-0').selectAll('g.link-' + d.character.id).selectAll('.link-ghost').nodes())
        var maxYPath = 0
        charPaths.forEach(function (p) {
          var bbox = p.getBBox()
          maxYPath = Math.max(maxYPath, bbox.x + bbox.width)
          var startPoint = scrollLeft + yLegendWidth
          var beginning = 0
          var end = p.getTotalLength()
          var target = 0
          var pos
          if (startPoint <= (bbox.x + bbox.width) && startPoint >= bbox.x) {
            while (true) {
              target = Math.floor((beginning + end) / 2)
              pos = p.getPointAtLength(target)
              if ((target === end || target === beginning) && pos.x !== startPoint) {
                break
              }
              if (pos.x > startPoint) {
                end = target
              } else if (pos.x < startPoint) {
                beginning = target
              } else {
                break
              }
            }
            yPos = pos.y
          }
          return
        })
        if (maxYPath < scrollLeft + yLegendWidth) {
          xPos = -2000
        }
        if (yPos === -1000) {
          yPos = y
        }
        d.yDiff = d.y - yPos
        d.xDiff = d.x - xPos
        d.maxYPath = maxYPath
        return 'translate(' + xPos + ',' + yPos + ')'
      })
    if(!transition.empty()){
      return transition.end()
    } else {
      return undefined
    }
  }
  axis.updateSelected = function (data, selections) {
    const isSelectionActive = data.isSelectionActive()
    d3
      .selectAll('.intro')
      .selectAll('text')
      .style('fill', d => {
        const isSelected = data.isEntitySelected(d.character, selections)
        return style.getTextColor(isSelected, d.character.isTerm)
      })
      .style('font-weight', d => {
        const isSelected = data.isEntitySelected(d.character, selections)
        return style.getTextFontWeight(isSelected)
      })
      .style('fill-opacity', d => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(d.character, selections)
        return style.getOpacity(isSelectionActive, isSelected, isRelatedSelected)
      })
    d3
      .selectAll('.intro')
      .selectAll('rect')
      .style('fill-opacity', d => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(d.character, selections)
        return style.getOpacity(isSelectionActive, isSelected, isRelatedSelected)
      })
  }
  axis.legendWidth = function (narrative) {
    const intros = narrative.introductions()
    var mixIntroX = 2000
    for (var index in intros) {
      var xpos = intros[index].x
      mixIntroX = Math.min(mixIntroX, xpos)
    }
    return mixIntroX
  }


  return axis.init()
}
export function helpText() {
  var result = `  
  <br>
  <h2> Coordinated Views </h2>
  <br>
  <ul> 
      <li>
        - This visualization is divided into two parts: the map on the left side, and a set 
        of lines accompanied by a timeline on the right side. 
        The two parts are linked through <b class="highlightText">connecting lines</b>.
      </li>
      <br>
      <br>
      <li>
        - On the right side, each line represents a person’s <b class="highlightText">story</b>. 
        As a line progresses along the x-axis, it shows events for that person in chronological order. 
      </li>
      <br>
      <br>
      <li>
      - Events are represented by bar symbols on a person’s line. 
      An event can involve more than one person. 
      When two lines curve to come together in an event, this symbolizes a <b class="highlightText">relationship</b> 
      between the people represented by those lines. The lines join in a relationship bar. 
      </li>
      <br>
      <br>
      <li>
      - A <b class="highlightText">connecting line</b> always links each person to a location, which is filled in 
      on the <b class="highlightText">map</b> on the left. 
      The location(s) linked to a person are those related to their <b class="highlightText">upcoming</b> event 
      (those closest to the left edge of the timeline). 
      </li>
      <br>
      <br>
      <li>
      - The circle symbols on the connecting lines represent these upcoming events and show who is involved in them. 
      </li>
      <br>
      <br>
      <li>
      - You can <b class="highlightText">highlight</b> any type of entity by clicking on it: people’s lines, 
      places on the map, and dates in the timeline. 
      </li>
      <br>
      <br>
      <li>
      - The most important interaction is <b class="highlightText">scrolling through the timeline</b>! 
      Only the locations linked to the upcoming events are shown on the map. 
      </li>
      <br>
      <br>
      <li>
      - The name of the last clicked entity or date will be copied in the clipboard. 
      You can use this feature to copy answers or search for information.
      </li>
      </ul>
    `
  return result
}
export default function dimensions() {
  var dimensions = {}

  var _active
  var _gsl
  var _allDimensions

  dimensions.gsl = function (gsl) {
    _gsl = gsl
    _active = {}
    _active['x'] = "time"
    _active['y'] = "people"
    _active['z'] = "locations"
    return dimensions
  }

  dimensions.setConfig = function (dims) {
    _allDimensions = []
    for (let d in dims) {
      _allDimensions.push(d)
    }
    return dimensions
  }

  // GETTERS
  dimensions.getAll = function () {
    return _allDimensions
  }

  dimensions.active = function (axis) {
    return _active[axis]
  }
  dimensions.activeAll = function () {
    return _active
  }
  
  return dimensions
}
import * as style from './style/stories.js'
import sceneStyle from './style/scenes.js'
import { getLinkGroupId } from '../gsl-utils.js'

export default function lines() {
  var lines = {}

  // CREATE CONTAINERS
  lines.createContainers = function (level) {
    d3.selectAll('#gsl-main')
      .append('g')
      .attr('id', 'gsl-lines-' + level)
    d3.selectAll('#gsl-main')
      .append('g')
      .attr('id', 'gsl-lines-open-outside-' + level)
  }
  lines.createContainersOpenScenes = function (level) {
    d3.select('#gsl-main')
      .append('g')
      .attr('id', 'gsl-lines-open-inside-' + level)
  }

  // DRAW
  lines.draw = function (narrative, entityInteractions) {
    drawLayer0(narrative, entityInteractions)
    drawLayer1(narrative, entityInteractions)
  }

  function drawLayer0(narrative, entityInteractions) {
    var linkG = d3
      .select('#gsl-lines-0')
      .selectAll('.link-g')
      .data(narrative.links().filter((d) => !d.character.isFake))
      .enter()
      .append('g')
      .attr('class', (d) => 'link-g link-g-' + d.character.id)
      .attr('id', (d, i) => getLinkGroupId(d, i))
      .attr('active', 'activated')

    linkG
      .append('path')
      .attr('class', 'link')
      .attr('d', narrative.link())
      .style('fill', 'none')
      .style('stroke-width', '3px')
      .style('cursor', 'pointer')
      .style('pointer-events', 'visibleStroke')
      .on('click', (d) =>
        entityInteractions.click(d.character, 'yDimLine'),
      )
      .on('mouseover', (d) => entityInteractions.mouseOver(d.character.name))
      .on('mouseout', (d) => entityInteractions.mouseOut())

    linkG
      .append('path')
      .attr('class', 'link-ghost')
      .attr('d', narrative.link())
      .style('stroke', 'blue')
      .style('fill', 'none')
      .style('visibility', 'hidden')
  }

  function drawLayer1(narrative, entityInteractions) {
    // GROUP
    var linkG = d3
      .select('#gsl-lines-1')
      .selectAll('.link-g')
      .data(narrative.links().filter((d) => !d.character.isFake))
      .enter()
      .append('g')
      .attr('class', (d) => 'link-g link-g-' + d.character.id)
      .attr('id', (d, i) => getLinkGroupId(d, i))
      .attr('active', 'activated')

    // MASK
    var mask = linkG
      .append('clipPath')
      .attr('id', (d, i) => 'mask-' + getLinkGroupId(d, i))
    // SOURCE
    mask
      .append('rect')
      .attr('id', 'source')
      .attr('x', (d) => {
        const x0 = d.source.scene ? d.source.scene.x + d.source.x : d.source.x
        return x0 + 5
      })
      .attr('y', (d) => {
        const y0 = d.source.scene ? d.source.scene.y + d.source.y : d.source.y
        return y0 - 20
      })
      .attr('width', 6)
      .attr('height', 40)
    // TARGET
    mask
      .append('rect')
      .attr('id', 'target')
      .attr('x', (d) => {
        const x1 = d.target.scene ? d.target.scene.x + d.target.x : d.target.x
        return x1 - sceneStyle('sceneWidth') 
      })
      .attr('y', (d) => {
        const y1 = d.target.scene ? d.target.scene.y + d.target.y : d.target.y
        return y1 - 20
      })
      .attr('width', 6)
      .attr('height', 40)
    // PATH
    linkG
      .append('path')
      .attr('class', 'link')
      .attr('d', narrative.link())
      .attr('clip-path', (d, i) => {
        return 'url(#mask-' + getLinkGroupId(d, i) + ')'
      })
      .style('stroke', 'red')
      .style('opacity', 1)
      .style('fill', 'none')
      .style('stroke-width', '3px')
  }

  // UPDATE
  lines.update = function (narrative, animationDuration, select) {
    const promises = []
    var transition
    // LAYER 0
    var linksGroups = d3
      .selectAll('#gsl-lines-0')
      .selectAll('.link-g')
      .data(narrative.links().filter((d) => !d.character.isFake))
    linksGroups.selectAll('.link-ghost').attr('d', narrative.link())
    transition = linksGroups
      .selectAll('.link')
      .transition()
      .duration(animationDuration)
      .attr('d', narrative.link())
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    // LAYER 1
    linksGroups = d3
      .selectAll('#gsl-lines-1')
      .selectAll('.link-g')
      .data(narrative.links().filter((d) => !d.character.isFake))
    const mask = linksGroups.selectAll('clipPath')
    transition = mask
      .select('#source')
      .transition()
      .duration(animationDuration)
      .attr('x', (d) => {
        const x0 = d.source.scene ? d.source.scene.x + d.source.x : d.source.x
        return x0
      })
      .attr('y', (d) => {
        const y0 = d.source.scene ? d.source.scene.y + d.source.y : d.source.y
        return y0 - 20
      })
    if (!transition.empty()) {
      promises.push(transition.end())
    }
    transition = mask
      .select('#target')
      .transition()
      .duration(animationDuration)
      .attr('x', (d) => {
        const x1 = d.target.scene ? d.target.scene.x + d.target.x : d.target.x
        return x1 - sceneStyle('sceneWidth') - 6
      })
      .attr('y', (d) => {
        const y1 = d.target.scene ? d.target.scene.y + d.target.y : d.target.y
        return y1 - 20
      })
    if (!transition.empty()) {
      promises.push(transition.end())
    }

    transition = linksGroups
      .selectAll('.link')
      .transition()
      .duration(animationDuration)
      .attr('d', narrative.link())
    promises.push(transition.end())
    return promises
  }

  lines.updateSelected = function (data, selections) {
    const isSelectionActive = data.isSelectionActive()
    d3.selectAll('.link')
      .style('opacity', (d) => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.character,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke', (d) => {
        const isSelected = data.isEntitySelected(d.character, selections)
        return style.getLineColor(isSelected, d.character.isTerm)
      })
    var toRaise = d3
      .select('#gsl-lines-0')
      .selectAll('.link-g')
      .filter((d) => {
        return data.isEntitySelected(d.character, selections)
      })
    toRaise.raise()
    toRaise = d3
      .select('#gsl-lines-open-outside-0')
      .selectAll('.linkG')
      .filter((d) => {
        return data.isEntitySelected(d.character, selections)
      })
    toRaise.raise()
  }
  // CLEAN
  lines.clean = function () {
    d3.select('#gsl-lines-0').selectAll('*').remove()
    d3.select('#gsl-lines-1').selectAll('*').remove()
    d3.select('#gsl-lines-open-inside-0').selectAll('*').remove()
    d3.select('#gsl-lines-open-inside-1').selectAll('*').remove()
    d3.select('#gsl-lines-open-outside-0').selectAll('*').remove()
    d3.select('#gsl-lines-open-outside-1').selectAll('*').remove()
  }
  return lines
}

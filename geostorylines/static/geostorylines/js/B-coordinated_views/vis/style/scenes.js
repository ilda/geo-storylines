export default function sceneStyle(characteristic) {
  var style = {
    labelSize: [195, 30],
    pathSpace: 25,
    groupMargin: 10,
    leftPadding: 10,
    rightPadding: 10,
    sceneWidth: 7,
    scenePadding: 2
  }
  return style[characteristic]
}

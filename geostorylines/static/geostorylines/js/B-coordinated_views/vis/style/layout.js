export default function layout(characteristic) {
    var gslBbox = d3.select('.gsl-container').node().getBoundingClientRect()
    var style = {
      yAxisSize: ((gslBbox.width)/7)*3,
    }
    return style[characteristic]
  }
  
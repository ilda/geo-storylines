import { getTranslateCoordinates } from '../gsl-utils.js'
import layout from './style/layout.js'
import logInteraction from '../interactions/log.js'

export default function horizontalScrollbar() {
  var scrollbar = {}
  var _domain
  var _range
  var _left
  var _size
  var _scaleContainer
  var _scaleChart
  var _delta
  var _height
  var _chart
  var _lines = []
  var _openSceneRects = []
  var _filterRects = []
  scrollbar.domain = function (value) {
    _domain = value

    return scrollbar
  }
  scrollbar.chart = function (chart) {
    _chart = chart
    return scrollbar
  }
  scrollbar.createContainers = function () {
    d3
      .select('.gsl-horizontal-scroll')
      .append('svg')
      .attr('id', 'svgHorizontalScrollbar')
      .attr('width', _range)
      .attr('height', _height)
      .style('background-color', '#f0f0f0')
      .append('g')
      .attr('id', 'markersContainer')
    d3
      .select('#svgHorizontalScrollbar')
      .append('rect')
      .attr('class', 'slider')
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', _range)
      .attr('height', _height)
      .style('fill', '#737373')
      .style('opacity', 0.4)      

    var dragHandler = d3.drag()
      .on('start', function () {
        var current = d3.select(this)
        _delta = current.attr('x') - d3.event.x
        const interaction = {}
        interaction.type = 'scroll-start'
        interaction.argument = 'horizontal'
        logInteraction(interaction)
      })
      .on('drag', function () {
        var x = d3.event.x + _delta
        scrollbar.move(x)
      })
      .on('end', function () {
        const interaction = {}
        interaction.type = 'scroll-end'
        interaction.argument = 'horizontal'
        logInteraction(interaction)
      })
    dragHandler(d3.select('#svgHorizontalScrollbar').select('.slider'))
    return scrollbar
  }
  scrollbar.move = function (x) {
    x = Math.max(0, x)
    //x = Math.max(-(_size/4), x)
    x = Math.min(_range - 10, x)
    d3.select('#svgHorizontalScrollbar').selectAll('.slider').attr('x', x)
    var node = d3.select('#gsl-legend-x-svg').select('#gsl-legend-x-g').node()
    var t = getTranslateCoordinates(node)
    d3
      .select('#gsl-legend-x-svg')
      .selectAll('#gsl-legend-x-g')
      .attr('transform', 'translate(' + -_scaleContainer(x) + ',' + t.y + ')')
    var node = d3.selectAll('#gsl-main').node()
    var t = getTranslateCoordinates(node)
    d3
      .selectAll('#gsl-main')
      .attr('transform', 'translate(' + -_scaleContainer(x) + ',' + t.y + ')')
    _chart.axes().yAxis().updateScroll()
  }
  scrollbar.moveDelta = function (delta) {
    const x = d3.selectAll('#svgHorizontalScrollbar')
      .selectAll('.slider')
    scrollbar.move(Number(x) + delta)
    return scrollbar
  }
  scrollbar.updateSize = function () {
    _range = $('.gsl-horizontal-scroll').width()
    _height = $('.gsl-horizontal-scroll').height()
    _left = layout('yAxisSize')
    //_left = 0
    _scaleContainer = d3.scaleLinear().domain([0, _range]).range([0, _domain])
    _scaleChart = d3.scaleLinear().domain([0, _domain]).range([0, _range])
    _size = _scaleChart(_range)
    if (_size > _range) {
      _size = _range 
    }
    d3
      .select('#svgHorizontalScrollbar')
      .attr('width', _range)
      .attr('height', _height)
    d3
      .selectAll('#horizontalScrollbar')
      .attr('width', _range)
      .attr('height', _height)
      .style('padding-left', _left)
    d3.selectAll('#svgHorizontalScrollbarLeft')
      .attr('width', layout('yAxisSize'))
      .attr('height', _height)
    d3
      .selectAll('#svgHorizontalScrollbar')
      .selectAll('.slider')
      .attr('y', 2)
      .attr('height', _height - 4)
      .attr('width', _size)      
      .style('fill', '#737373')
      .style('opacity', 0.4)
    return scrollbar
  }
  scrollbar.addLine = function (x, color) {
    const newLine = {}
    newLine.x = x
    newLine.color = color
    _lines.push(newLine)
    d3
      .select('#svgHorizontalScrollbar')
      .select('#markersContainer')
      .append('line')
      .attr('class', 'hScrollLine')
      .attr('x1', _scaleChart(x))
      .attr('x2', _scaleChart(x))
      .attr('y1', 0)
      .attr('y2', _height / 2 - 1)
      .style('stroke', color)
      .style('stroke-width', 4)
    return scrollbar
  }
  scrollbar.addOpenSceneRect = function (x, width, color) {
    const newRect = {}
    newRect.x = x
    newRect.width = width
    newRect.color = color
    _openSceneRects.push(newRect)
    d3
      .select('#svgHorizontalScrollbar')
      .select('#markersContainer')
      .append('rect')
      .attr('class', 'hScrollRect')
      .attr('x', _scaleChart(x))
      .attr('y', _height / 2 + 1)
      .attr('width', _scaleChart(width))
      .attr('height', _height / 2)
      .style('fill', color)
    return scrollbar
  }
  scrollbar.addFilterRect = function (x, color) {
    const newRect = {}
    newRect.x = x
    newRect.color = color
    _filterRects.push(newRect)
    var svg = d3.select('#svgHorizontalScrollbar')
    var scaledX = _scaleChart(x)
    if(x == layout('yAxisSize')) {
      scaledX = 0
    }
    svg
      .select('#markersContainer')
      .append('rect')
      .attr('class', 'hScrollRect')
      .attr('x', scaledX)
      .attr('y', _height/4)
      .attr('width', 4)
      .attr('height', _height /2)
      .style('fill', '#f0f0f0')
      .style('stroke', color)
      .style('stroke-width', 1)
    return scrollbar
  }
  scrollbar.clean = function () {
    _lines = []
    _openSceneRects = []
    _filterRects = []
    scrollbar.cleanSVG()
  }
  scrollbar.cleanSVG = function () {
    d3.selectAll('.hScrollLine').remove()
    d3.selectAll('.hScrollRect').remove()
    return scrollbar
  }
  scrollbar.reDrawSelected = function () {
    scrollbar.cleanSVG()
    _lines.forEach(l => {
      scrollbar.addLine(l.x, l.color)
    })
    _openSceneRects.forEach(r => {
      scrollbar.addOpenSceneRect(r.x, r.width, r.color)
    })
    _filterRects.forEach(r => {
      scrollbar.addFilterRect(r.x, r.color)
    })
    return scrollbar
  }
  scrollbar.reset = function () {
    d3.select('#svgHorizontalScrollbar').select('.slider').attr('x', 0)
    _delta = 0
    scrollbar.move(0)
  }
  return scrollbar
}
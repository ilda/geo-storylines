import verticalScrollbar from './scrollbar-vertical.js'
import horizontalScrollbar from './scrollbar-horizontal.js'
import * as style from './style/stories.js'

export default function scrollbars() {
  var scrollbars = {}
  var _vertical
  var _horizontal
  var _chart
  scrollbars.init = function () {
    _vertical = verticalScrollbar()
    _horizontal = horizontalScrollbar()
    return scrollbars
  }
  scrollbars.chart = function (chart) {
    _chart = chart
    _vertical.chart(chart)
    _horizontal.chart(chart)
    return scrollbars
  }
  // Not sure if there are too many functions or not
  scrollbars.verticalDomain = function (value) {
    _vertical.domain(value)
    return scrollbars
  }
  scrollbars.horizontalDomain = function (value) {
    _horizontal.domain(value)
    return scrollbars
  }
  scrollbars.verticalRange = function (value) {
    _vertical.range(value)
    return scrollbars
  }
  scrollbars.horizontalRange = function (value) {
    _horizontal.range(value)
    return scrollbars
  }
  scrollbars.createContainers = function () {
    _vertical.createContainers()
    _horizontal.createContainers()
    return scrollbars
  }
  scrollbars.updateSize = function (domainWidth, domainHeight, left) {
    _horizontal
      .domain(domainWidth)
      .updateSize()
    _vertical
      .domain(domainHeight)
      .updateSize()
    return scrollbars
  }
  scrollbars.updateRange = function () {
    setTimeout(() => {
      _vertical.updateSize().reDrawSelected()
      _horizontal.updateSize().reDrawSelected()
    }, 1)

  }
  scrollbars.updateSelected = function (data, selections) {
    _vertical.clean()
    _horizontal.clean()
    d3
      .select('#gsl-scenes-0')
      .selectAll('.scene')
      .each(scene => {
        if (scene.isOpen) {
          return
        }
        var isSelected = data.isSceneSelected(scene, selections)
        var isRelatedSelected = data.isSceneRelatedSelected(scene)
        if (isSelected || isRelatedSelected) {
          const lineColor = style.getSceneScrollColor(isSelected)
          _vertical.addLine(scene.y, lineColor)
          _horizontal.addLine(scene.x, lineColor)
        }
      })
    d3
      .selectAll('g.open-scene')
      .each(scene => {
        var isSelected = data.isSelected(scene.keyNode.id)
        scene.characters.forEach(function (element) {
          if (data.isSelected(element.id)) {
            isSelected = true
          }
        })
        const lineColor = style.getSceneScrollColor(isSelected)
        const rect = d3.select('#sceneRect_' + scene.id).node().getBBox()
        _vertical.addOpenSceneRect(scene.y, rect.height, lineColor)
        _horizontal.addOpenSceneRect(scene.x, rect.width, lineColor)
      })
    d3
      .select('#gsl-legend-y-svg')
      .select('#gsl-legend-y-g')
      .selectAll('.intro')
      .each(d => {
        if (d.character.isFiltered || d.character.isTerm) {
          var isTerm = false
          if (d.character != undefined && d.character.isTerm)
            isTerm = true
          const color = style.getFilterRectColor(isTerm)
          _vertical.addFilterRect(d.y, color)
          _horizontal.addFilterRect(d.x, color)
        }
      })
    d3
      .select('#gsl-legend-x-g')
      .selectAll('.xDimLegend')
      .each(d => {
        if (d.keyNode.isFiltered) {
          const color = style.getFilterRectColor(false)
          _horizontal.addFilterRect(d.xpos, color)
        }

      })
  }
  scrollbars.reset = function () {
    _horizontal.reset()
    _vertical.reset()
  }
  return scrollbars.init()
}
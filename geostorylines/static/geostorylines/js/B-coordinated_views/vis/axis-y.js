import * as style from './style/stories.js'
import { getTranslateCoordinates, narrativeLink, cleanName, nameCutter } from '../gsl-utils.js'
import layout from './style/layout.js'

export default function yAxis() {
  var axis = {}
  var _projection
  var _width
  var _data
  var _uiSelectors
  var _centroids = {}
  var _mapSize = parseInt(window.screen.width*0.33) 
  var _closerSceneDistance = parseInt(window.screen.width*0.41) // This could be better, probably
  // TODO: This is a bit ugly
  var _entityInteractions
  var _data
  _uiSelectors
  axis.init = function () {
    return axis
  }
  axis.dataAndSelectors = function (data, uiselectors) {
    _data = data
    _uiSelectors = uiselectors
  }
  axis.createContainers = function () {
    const svg = d3.select('.gsl-legend-y')
      .append('svg')
      .attr('id', 'gsl-legend-y-svg')
    svg
      .append('g')
      .attr('id', 'gsl-legend-y-g')
      .attr('transform', 'translate(0, 25)')
    svg
      .append('g')
      .attr('id', 'gsl-legend-y-map')
    d3
      .selectAll('#gsl-main')
      .append('g')
      .attr('id', 'gsl-legend-y-g')


    return axis
  }
  axis.updateSize = function (chartHeight, width) {
    _width = width
    const bbox = $('.gsl-legend-y').width()
    d3.select('#gsl-legend-y-svg')
      .attr('width', bbox)
      .attr('height', chartHeight)
    return axis
  }
  axis.clean = function () {
    d3.selectAll('#gsl-legend-y-g').selectAll('*').remove()
  }
  axis.draw = function (narrative, entityInteractions, backgroundInteractions, map, rotation) {
    _entityInteractions = entityInteractions
    d3
      .select('#gsl-legend-y-svg')
      .style('background-color', '#f7f7f7')
      .on('click', d => backgroundInteractions.click())

    d3.select('#gsl-legend-y-g').attr('transform', 'translate(0, 25)')

    // DRAW ENTITIES' LABELS
    var introG = d3.selectAll('#gsl-main')
      .select('#gsl-legend-y-g')
      .selectAll('.intro')
      .data(narrative.introductions().filter(d => !d.character.isFake))
      .enter()
      .append('g')
      .attr('class', 'intro')
      .attr('id', d => 'intro_' + d.character.id)
      .on('click', d => entityInteractions.click(d.character, 'yDimName', d.character.name))
      .attr('cursor', 'pointer')
      .style('pointer-events', 'visible')
      .attr('transform', function (d) {
        var x = Math.round(d.x)
        var y = Math.round(d.y)
        return 'translate(' + [x, y] + ')'
      })
    introG.append('rect')
      .attr('class', d => 'entityDom_' + d.character.id)
      .attr('typeDom', 'char')
      .attr('y', -4)
      .attr('x', -4)
      .attr('width', 4)
      .attr('height', 8)
    introG
      .append('text')
      .attr('text-anchor', 'end')
      .attr('y', '4px')
      .attr('x', '-8px')
      .text(d => d.character.name)
      .on('mouseover', d => entityInteractions.mouseOver(d.character.name))
      .on('mouseout', d => entityInteractions.mouseOut())

    // LEFT RECT
    d3.select('#gsl-legend-y-svg')
      .select('g')
      .selectAll('.intro').remove()
    var introGR = d3.select('#gsl-legend-y-svg')
      .select('#gsl-legend-y-g')
      .selectAll('.intro')
      .data(narrative.introductions().filter(d => !d.character.isFake))
      .enter()
      .append('g')
      .attr('class', 'intro')
      .attr('id', d => 'intro_' + d.character.id)
      .on('click', d => entityInteractions.click(d.character, 'yDimName', d.character.name))
      .attr('cursor', 'pointer')
      .style('pointer-events', 'visible')
      .attr('transform', function (d) {
        var x, y
        x = Math.round(d.x)
        y = Math.round(d.y)
        return 'translate(' + [x, y] + ')'
      })
    introGR.append('rect')
      .attr('y', -4)
      .attr('x', -4)
      .attr('width', 4)
      .attr('height', 8)

    introGR
      .append('text')
      .attr('text-anchor', 'end')
      .attr('y', '4px')
      .attr('x', '-8px')
      .text(d => nameCutter(d.character.name, 15))
      .on('mouseover', d => entityInteractions.mouseOver(d.character.name))
      .on('mouseout', d => entityInteractions.mouseOut())
    drawMap(map, entityInteractions, rotation)
    return axis
  }
  axis.drawNoData = function (width, height, backgroundInteractions) {
  }
  axis.update = function (narrative, animationDuration) {
    const transition = d3.selectAll('#gsl-legend-y-g')
      .selectAll('.intro')
      .data(narrative.introductions().filter(d => !d.character.isFake))
      .transition()
      .duration(animationDuration)
      .attr('transform', d => 'translate(' + [d.x, d.y] + ')')
    const promises = [axis.updateScroll(narrative, animationDuration)]
    if (!transition.empty()) {
      // TODO: why this gives an error?
      //promises.push(transition.end())
    }

    return promises
  }
  axis.updateScroll = function (narrative, animationDuration = 0, addExtraMarkers = true) {
    var node = d3.selectAll('#gsl-main').node()
    var t = getTranslateCoordinates(node)
    var scrollLeft = -t.x - layout('yAxisSize') + 5
    var scrollTop = t.y
    //console.log('###$$$')
    //console.log(scrollLeft)

    var closerScene = undefined
    const scenes = d3.select('#gsl-main').select('#gsl-scenes-1').selectAll('.scene')
    var min = 40000000000
    scenes.each(s => {
      const dif = s.x - scrollLeft
      if (dif > _closerSceneDistance && dif < min) {
        min = dif
        closerScene = s
      }

    })

    var yLegendWidth = layout('yAxisSize')
    const linksScenes = {}
    const transition = d3
      .select('#gsl-legend-y-svg')
      .selectAll('.intro')
      .transition()
      .duration(animationDuration)
      .attr('transform', function (d) {
        // TODO: OPTIMIZE THIS
        var xPos = 0
        var yPos = -1000
        var x = Math.round(d.x)
        if (x - scrollLeft >= yLegendWidth) {
          xPos = d.x - scrollLeft
        } else {
          xPos = yLegendWidth - 7
        }
        var y = Math.round(d.y)
        var charPaths = d3.selectAll('#gsl-lines-open-outside-0').selectAll('.link-g-' + d.character.id).selectAll('.link-ghost').nodes()
        charPaths = charPaths.concat(d3.selectAll('#gsl-lines-0').selectAll('.link-g-' + d.character.id + '[active=activated]').selectAll('.link-ghost').nodes())
        charPaths = charPaths.concat(d3.selectAll('#gsl-lines-open-inside-0').selectAll('g.link-' + d.character.id).selectAll('.link-ghost').nodes())
        var maxYPath = 0
        var path = undefined
        charPaths.forEach(function (p) {
          var bbox = p.getBBox()
          maxYPath = Math.max(maxYPath, bbox.x + bbox.width)
          var startPoint = scrollLeft + yLegendWidth
          var beginning = 0
          var end = p.getTotalLength()
          var target = 0
          var pos
          if (startPoint <= (bbox.x + bbox.width) && startPoint >= bbox.x) {
            while (true) {
              target = Math.floor((beginning + end) / 2)
              pos = p.getPointAtLength(target)
              if ((target === end || target === beginning) && pos.x !== startPoint) {
                break
              }
              if (pos.x > startPoint) {
                end = target
              } else if (pos.x < startPoint) {
                beginning = target
              } else {
                break
              }
            }
            yPos = pos.y
            path = p
          }
          return
        })
        // When the introduction dissapears because there no more scenes for it
        if (maxYPath < scrollLeft + yLegendWidth) {
          xPos = -2000
          path = undefined
        }
        // If I don't need to modify the y position
        if (yPos === -1000) {
          path = undefined
          yPos = y
        }
        if (path !== undefined) {
          const scene = path.__data__.target.scene

          if (scene != undefined && scene.id == closerScene.id && scene.locations.length > 0) {
            const isSelectionActive = _data.isSelectionActive()
            const selections = _uiSelectors.getAllSelections()
            var flag = true
            if (isSelectionActive && !_data.isSceneSelected(closerScene, selections)) flag = false
            if (flag) {
              if (!(scene.id in linksScenes)) {
                linksScenes[scene.id] = {}
                linksScenes[scene.id]['scene'] = scene
                linksScenes[scene.id]['yPositions'] = []
                linksScenes[scene.id]['xPositions'] = []
                linksScenes[scene.id]['characters'] = []
              }
              linksScenes[scene.id]['yPositions'].push(yPos + scrollTop)
              var l = d3.select('.gsl-legend-y').select("#intro_" + path.__data__.character.id).select('text').node().getBoundingClientRect().width + 15
              linksScenes[scene.id]['xPositions'].push(l)
              linksScenes[scene.id]['characters'].push(path.__data__.character)
            }
          }
        }
        d.yDiff = d.y - yPos
        d.xDiff = d.x - xPos
        d.maxYPath = maxYPath
        return 'translate(' + xPos + ',' + yPos + ')'
      })
    var linkScenesValues = Object.keys(linksScenes).map(function (key) {
      return linksScenes[key];
    });
    if (addExtraMarkers) {
      addLocationsMarkers(linkScenesValues)
    }
    if (!transition.empty()) {
      return transition.end()
    } else {
      return undefined
    }
  }
  axis.updateSelected = function (data, selections) {
    const isSelectionActive = data.isSelectionActive()
    d3
      .selectAll('.intro')
      .selectAll('text')
      .style('fill', d => {
        const isSelected = data.isEntitySelected(d.character, selections)
        return style.getTextColor(isSelected, d.character.isTerm)
      })
      .style('font-weight', d => {
        const isSelected = data.isEntitySelected(d.character, selections)
        return style.getTextFontWeight(isSelected)
      })
      .style('fill-opacity', d => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(d.character, selections)
        return style.getOpacity(isSelectionActive, isSelected, isRelatedSelected)
      })
    d3
      .selectAll('.intro')
      .selectAll('rect')
      .style('fill-opacity', d => {
        const isSelected = data.isEntitySelected(d.character, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(d.character, selections)
        return style.getOpacity(isSelectionActive, isSelected, isRelatedSelected)
      })
    d3
      .selectAll('.map-path')
      .style('stroke', (d) => {
        if (d.properties.highlighted) return 'red'
        const isSelected = data.isEntitySelected(d.properties.node, selections)
        return style.getSceneCircleFillColor(isSelected)
      })
      .style('fill-opacity', (d) => {
        const isSelected = data.isEntitySelected(d.properties.node, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.properties.node,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-opacity', d => {
        if (d.properties.highlighted) return 'red'
        const isSelected = data.isEntitySelected(d.properties.node, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.properties.node,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-width', (d) => {
        if (d.properties.highlighted) return '3'
        const isSelected = data.isEntitySelected(d.properties.node, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.properties.node,
          selections,
        )
        if (isSelectionActive && (isSelected || isRelatedSelected)) return 3
        // PAPER CONF
        //if (isSelectionActive && (isSelected || isRelatedSelected)) return 2
        return 1
      })

    d3
      .selectAll('.link-scene-map')
      .style('opacity', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        const isRelatedSelected = _data.isSceneRelatedSelected(d.scene)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        return style.getSceneRectColor(isSelected)
      })


  }
  axis.legendWidth = function (narrative) {
    const intros = narrative.introductions()
    var mixIntroX = 2000
    for (var index in intros) {
      var xpos = intros[index].x
      mixIntroX = Math.min(mixIntroX, xpos)
    }
    return mixIntroX
  }

  function drawMap(map, entityInteractions, rotation) {
    _projection = d3.geoMercator()
      .rotate([0, 0, rotation])
      // PAPER CONF
      //.fitExtent([[200, 0], [_mapSize+200, _mapSize-200]], map);
      .fitExtent([[10, 100], [_mapSize + 10, _mapSize + 100]], map);
    const path = d3.geoPath().projection(_projection)
    map.features.forEach(f => {
      if (f.properties.hierarchical_level == 'parent') return
      const c = path.centroid(f)
      _centroids[f.properties.name] = c
      if (f.properties.name == "United States")
        _centroids[f.properties.name] = _projection([-101.68, 41.08])
      if (f.properties.name == "Argentina")
        _centroids[f.properties.name] = _projection([-65.62, -36.25])
      if (f.properties.name == "Canada")
        _centroids[f.properties.name] = _projection([-102.66, 59.37])
    })

    d3
      .selectAll('.gsl-legend-y')
      .selectAll('#gsl-legend-y-map')
      .append('g')
      .attr('class', 'map-features')
      .selectAll('.map-path')
      .data(() => map.features.filter(f => f.properties.hierarchical_level == 'child'))
      .enter()
      .append('path')
      .attr('class', 'map-path')
      .attr('id', d => 'map-path-' + cleanName(d.properties.name, ' ', '-'))
      .attr('d', path)
      .style('stroke', '#d9d9d9')
      .style('stroke-width', '1px')
      // COMMENT NEXT LINE FOR PAPER CONF
      .style('stroke-dasharray', 2)
      .style('fill', '#d9d9d9')
      .on('mouseover', d => entityInteractions.mouseOver(d.properties.name))
      .on('mouseout', d => entityInteractions.mouseOut())
      .on('click', function (d) {
        entityInteractions.click(d.properties.node, 'yDimName', d.properties.name)
        d3.select(this).raise()
      })
    d3
      .selectAll('.gsl-legend-y')
      .selectAll('#gsl-legend-y-map')
      .append('g')
      .attr('class', 'map-features-parent')
      .selectAll('.map-path-parent')
      .data(() => map.features.filter(f => f.properties.hierarchical_level == 'parent'))
      .enter()
      .append('path')
      .attr('class', 'map-path-parent')
      .attr('d', path)
      .style('stroke', d => d.properties.color)
      .style('stroke', '#000')
      .style('stroke-width', '2px')
      .style('fill', 'none')
      .style('fill-opacity', 0)
      .style('stroke-opacity', 1)


  }
  function addLocationsMarkers(linksScenes) {
    const selections = _uiSelectors.getAllSelections()
    const isSelectionActive = _data.isSelectionActive()
    d3
      .selectAll('.gsl-legend-y')
      .selectAll('#gsl-legend-y-map')
      .selectAll('.scene-circle')
      .remove()
    d3
      .selectAll('.gsl-legend-y')
      .selectAll('#gsl-legend-y-map')
      .selectAll('.link-scene-map')
      .remove()
    d3
      .selectAll('.map-path')
      .style('fill', '#d9d9d9')
    const locs = d3
      .selectAll('.gsl-legend-y')
      .selectAll('#gsl-legend-y-map')
      .selectAll('.appearance')
      .data(linksScenes)
      .enter()

    // TO THE INTROS
    locs
      .selectAll('.link-scenes')
      .data(d => {
        const newData = []
        var min = 100000
        d.yPositions.forEach(p => {
          min = Math.min(min, p)
        })
        const mean = min
        const middlePoint = [_mapSize + 50 - 2, mean]
        d.yPositions.forEach((p, i) => {
          const tmp = {}
          tmp.character = d.characters[i]
          tmp.point = [middlePoint, [layout('yAxisSize') - d.xPositions[i], p]]
          tmp.scene = d.scene
          newData.push(tmp)
        })
        return newData
      })
      .enter()
      .append('path')
      .attr('class', 'link-scene-map')
      .attr('d', d => narrativeLink(d.point))
      .style('fill', 'none')
      .style('opacity', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        const isRelatedSelected = _data.isSceneRelatedSelected(d.scene)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke', (d) => {
        const isSelected = _data.isEntitySelected(d.character, selections)
        return style.getSceneRectColor(isSelected)
      })
      .style('stroke-width', '3px')
      .style('cursor', 'pointer')
      .style('pointer-events', 'visibleStroke')
      .on('click', (d) =>
        _entityInteractions.click(d.character, 'yDimLine'),
      )
      .on('mouseover', (d) => _entityInteractions.mouseOver(d.character.name))
      .on('mouseout', (d) => _entityInteractions.mouseOut())

    // TO THE LOCATIONS
    locs
      .selectAll('.link-map')
      .data(d => {
        const newData = []
        var min = 100000
        d.yPositions.forEach(p => {
          min = Math.min(min, p)
        })
        const mean = min
        const middlePoint = [_mapSize + 50 + 2, mean]
        d.scene.locations.forEach(l => {
          const tmp = {}
          tmp.character = l
          const coords = _centroids[l.name]
          tmp.point = [[coords[0] + 3, coords[1]], middlePoint]
          tmp.scene = d.scene
          newData.push(tmp)
        })
        return newData
      })
      .enter()
      .append('path')
      .attr('class', 'link-scene-map')
      .attr('d', d => narrativeLink(d.point))
      .style('fill', 'none')
      .style('opacity', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        const isRelatedSelected = _data.isSceneRelatedSelected(d.scene)
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        return style.getSceneRectColor(isSelected)
      })
      .style('stroke-width', '3px')
      .style('cursor', 'pointer')
      .style('pointer-events', 'visibleStroke')
      .on('click', (d) =>
        _entityInteractions.click(d.character, 'yDimLine'),
      )
      .on('mouseover', (d) => _entityInteractions.mouseOver(d.character.name))
      .on('mouseout', (d) => _entityInteractions.mouseOut())
    locs
      .append('circle')
      .attr('class', 'scene-circle')
      .attr('r', 5)
      .attr('cx', d => _mapSize + 50)
      .attr('cy', d => {
        var min = 100000
        d.yPositions.forEach(p => {
          min = Math.min(min, p)
        })
        return min
      })
      .style('fill', '#bdbdbd')
      .style('opacity', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        const isRelatedSelected = _data.isSceneRelatedSelected(
          d.scene,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('fill', '#bdbdbd')
      .style('stroke', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        return style.getSceneRectColor(isSelected)
      })
      .style('stroke-width', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        return style.getSceneRectStrokeWidth(isSelected)
      })
    locs
      .selectAll('.app-locations')
      .data(d => {
        const result = []
        d.scene.locations.forEach(l => {
          const tmp = {}
          tmp.location = l
          tmp.scene = d.scene
          result.push(tmp)
        })
        return result
      })
      .enter()
      .append('circle')
      .attr('class', 'scene-circle')
      .attr('r', 2)
      .attr('cx', d => _centroids[d.location.name][0])
      .attr('cy', d => _centroids[d.location.name][1])
      .style('opacity', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        const isRelatedSelected = _data.isSceneRelatedSelected(
          d.scene,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('fill', '#000')
      .style('stroke', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        return style.getSceneRectColor(isSelected)
      })
      .style('stroke-width', (d) => {
        const isSelected = _data.isSceneSelected(d.scene, selections)
        return style.getSceneRectStrokeWidth(isSelected)
      })
      .on('mouseover', d => _entityInteractions.mouseOver(d.name))
      .on('mouseout', d => _entityInteractions.mouseOut())

    linksScenes.forEach(linkE => {
      linkE.scene.locations.forEach(l => {
        d3
          .select('#map-path-' + cleanName(l.name, ' ', '-'))
          .style('fill', '#d94801')
      })
    })
  }



  return axis.init()
}
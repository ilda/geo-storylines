
export default function hierarchicalAggregator() {
  // THIS IS AN ABSTRACT "CLASS". 
  // Sorry if I'm breaking too many design patterns

  var aggregator = {}

  var _levels
  var _typeEntity

  var _aggregatedValues = {}

  // levels must be a list with the aggregation levels
  // They are ordered from the highest level to the lowest
  // The first element represents the higher level
  aggregator.init = function (typeEntity, levels) {
    _typeEntity = typeEntity
    _levels = levels
    return aggregator
  }

  aggregator.getLevels = function () {
    return _levels
  }

  aggregator.ignoredLevels = function () {
    return ['aggr_article']
  }

  aggregator.aggregateElement = function (element, aggregationLevel) {
    return element
  }
  
  return aggregator
}
export default function backgroundInteractionsDemo() {
  var interaction = {
    clickedOnce: false,
    timerClick: undefined
  }
  var _gsl
  interaction.gsl = function (gsl) {
    _gsl = gsl
    return interaction
  }
  interaction.click = function () {
    d3.event.stopPropagation();
    if (interaction.clickedOnce) {
      interaction.runDoubleClick()
    } else {
      interaction.timerClick = setTimeout(function () {
        interaction.runSimpleClick()
      }, 230)
      interaction.clickedOnce = true
    }
  }
  interaction.runSimpleClick = function () {
    click()
    interaction.clickedOnce = false
  }
  interaction.runDoubleClick = function () {
    interaction.clickedOnce = false
    clearTimeout(interaction.timerClick)
    doubleClick()
  }
  function click() {
  }
  function doubleClick() {
    _gsl.resetSelected()
  }
  return interaction
}

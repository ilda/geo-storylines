import data from './data/manager.js'
import chart from './vis/chart.js'
import uiSelectors from './ui/selectors.js'
import { getScenesURL, narrativeLink, cleanName } from './gsl-utils.js'
import entityInteractions from './interactions/entity.js'
import backgroundInteractions from './interactions/background.js'
import scenesInteractions from './interactions/scenes.js'
import * as style from './vis/style/stories.js'
import xAxis from './vis/axis-x.js'
import scrollbars from './vis/scrollbars.js'


export default function gsl() {
  var _data
  var _uiSelectors
  var _charts
  var _entityInteractions
  var _backgroundInteractions
  var _scenesInteractions
  var _scrollbars
  var _xAxis
  var _commonNarrative
  var _projection
  var _centroids = {}
  // PAPER CONF
  //var _rotation = 30
  var _rotation = 0
  var _mapSize = parseInt(window.screen.width * 0.33) // 850


  //////////////////////////////////////////////////////////////////////////////
  // INITIALIZE
  //////////////////////////////////////////////////////////////////////////////
  var gsl = {}
  gsl.init = function () {
    _backgroundInteractions = backgroundInteractions().gsl(gsl)
    _entityInteractions = entityInteractions().gsl(gsl)
    _scenesInteractions = scenesInteractions().gsl(gsl)
    _uiSelectors = uiSelectors().gsl(gsl)
    d3.json('/geostorylines/datasets-available').then(function (data) {
      gsl.changeDataset(data.datasets[0])
    })
    d3
      .select('body')
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0)
    return gsl
  }

  gsl.show = function () {
    _charts = {}
    gsl.createContainers()
    _scrollbars = scrollbars().createContainers()
    _uiSelectors = uiSelectors()
      .gsl(gsl)
      .dimensions(_data.dimensions())
      .aggregations(_data.aggregations())
      .addHelp()
    gsl.updateNarrative()
    return gsl
  }

  gsl.data = function (name, dimensions) {
    _data = data().init(name, dimensions)
    return gsl
  }

  gsl.createContainers = function () {
    const main = d3.select('.gsl-container')

    main
      .append('div')
      .attr('class', 'gsl-extraselector')
      .append('button')
      .attr('id', 'help')
      .attr('class', 'btn btn-secondary helpContent_open')
      .html('Help')

    main
      .append('div')
      .attr('class', 'gsl-map')
      .append('svg')
      .attr('width', 1500)
      .attr('height', 1225)

    main
      .append('div')
      .attr('class', 'gsl-legend-x-main')

    main
      .append('div')
      .attr('class', 'gsl-storylines')
    main
      .append('div')
      .attr('class', 'gsl-horizontal-scroll-main')

    main
      .append('div')
      .attr('class', 'gsl-vertical-scroll-main')
  }

  gsl.createAllConatinersperLocation = function (locations, scenes, nodes) {
    scenes.forEach(e => {
      e.characters = []
      e.charactersIds.forEach(c_id => {
        if (nodes[c_id] != undefined)
          e.characters.push(nodes[c_id])
      })
    })
    const dummyChart = chart()
    const chartDimensions = dummyChart.getDimensions(scenes.length)
    _commonNarrative = _data.createNarrative(scenes, chartDimensions[0], chartDimensions[1])
    var last = ''
    const locsDict = {}
    for (let nId in locations) {
      locsDict[nId] = _centroids[nodes[nId].name]
    }
    // Create items array
    var items = Object.keys(locsDict).map(function (key) {
      return [key, locsDict[key], locsDict[key][1]];
    });

    // Sort the array based on the second element
    items = items.sort(function (first, second) {
      return first[2] - second[2];
    });

    for (let index in items) {
      const nId = items[index][0]
      if (locations[nId].length > 0) {
        const subsetScenes = []
        const scenesIds = locations[nId]
        scenes.forEach(e => {
          if (scenesIds.indexOf(e.id) >= 0) {
            subsetScenes.push(e)
          }
        })
        subsetScenes.forEach(e => {
          e.characters = []
          e.charactersIds.forEach(c_id => {
            if (nodes[c_id] != undefined)
              e.characters.push(nodes[c_id])
          })
        })
        gsl.createContainerPerLocation(nId, subsetScenes, nodes[nId].name, _commonNarrative, chartDimensions, nodes[nId])
      }
      last = nId
    }
    // COMPUTE NO LOCATIONS
    const subsetScenes = []
    scenes.forEach(e => {
      if (e.edge.node_types_ids.locations.length == 0) {
        subsetScenes.push(e)
      }
    })
    subsetScenes.forEach(e => {
      e.characters = []
      e.charactersIds.forEach(c_id => {
        if (nodes[c_id] != undefined)
          e.characters.push(nodes[c_id])
      })
    })
    gsl.createContainerPerLocation('no-locations', subsetScenes, 'no-locations', _commonNarrative, chartDimensions)

    _charts[last].updateSelected(_data, _uiSelectors.getAllSelections())
    _xAxis = xAxis()
      .createContainers()
      .updateSize(chartDimensions[0])
      .draw(_commonNarrative, chartDimensions[1], _entityInteractions, _backgroundInteractions)
    _scrollbars.updateSize(chartDimensions[0], chartDimensions[1])
    _scrollbars.chart(_charts)
    _scrollbars.reset()
  }

  gsl.createContainerPerLocation = function (locationId, newScenes, name, commonNarrative, chartDimensions, character) {
    if (newScenes.length == 0) return
    const divId = 'storyline-' + cleanName(name, ' ', '_')
    const main = d3.select('.gsl-container').select('.gsl-storylines')
    const locationStoryDiv = main.append('div')

    const locationStory = locationStoryDiv.attr('id', divId).attr('class', 'gsl-storylines-container')
    const legendX = locationStory
      .append('div')
      .attr('class', 'gsl-legend-x gsl-legend-x-' + locationId)

    legendX.html(name)
    locationStory.append('div').attr('class', 'gsl-legend-y')
    locationStory.append('div').attr('class', 'gsl-scene')
    if (locationId != 'no-locations') {
      locationStory
        .append('div')
        .attr('class', 'gsl-legend-x-extra')
      legendX
        .on('click', d => _entityInteractions.click(character, 'yDimName', locationId))
        .on('mouseover', d => _entityInteractions.mouseOver(name))
        .on('mouseout', d => _entityInteractions.mouseOut())
    } else {
      locationStory
        .append('div')
        .attr('class', 'gsl-legend-x-extra-no-locations')
    }
    if (locationId in _charts) return
    _charts[locationId] = chart(character, newScenes)
    _charts[locationId].createContainers(divId)
    const nScenes = newScenes.length
    const chartDimensionsNew = _charts[locationId].getDimensions(nScenes)
    _charts[locationId].size(chartDimensions[0], chartDimensions[1])
    const newNarrative = _data.createNarrative(newScenes, chartDimensions[0], chartDimensionsNew[1])
    newNarrative.scenes().forEach(s => {
      const commonScene = commonNarrative.getScene(s.id)
      s.x = commonScene.x
    })
    _charts[locationId].updateSizeNarrative(commonNarrative)
    _charts[locationId].draw(
      newNarrative,
      _entityInteractions,
      _backgroundInteractions,
      _scenesInteractions,
    )
  }

  gsl.addSearchByKeywordsListeners = function () {
    document
      .getElementById('searchKeywordButton')
      .addEventListener('click', function () {
        const text = d3.select('#searchInput').node().value
        gsl.searchByKeyword(text)
      })
    document
      .getElementById('searchEntityButton')
      .addEventListener('click', function () {
        const text = d3.select('#searchInput').node().value
        gsl.searchByEntity(text)
      })
    d3.selectAll('#clearSearchButton').on('click', function () {
      const text = d3.select('#searchInput').node().value
      gsl.clearSearchByKeyword()
    })
  }

  gsl.addSearchKeywordBox = function (text, stats, type) {
    const regex = new RegExp(' ', 'g')
    const text_no_spaces = text.replace(regex, '-')
    const div = d3
      .select('#searchKeywordsContainer')
      .append('div')
      .attr('id', 'search-kw-' + text_no_spaces + '_' + type)
      .style('padding', '3px')
      .style('margin', '3px')
      .style('background-color', () => {
        if (type == 'keyword') {
          return '#d94801'
        } else {
          return '#3182bd'
        }
      })
      .style('display', 'inline-block')
      .style('color', '#fff')
    div
      .append('div')
      .style('float', 'right')
      .style('display', 'inline-block')
      .style('margin-left', '3px')
      .append('a')
      .attr('id', 'search-kw-x-' + text_no_spaces)
      .on('click', (d) => gsl.removeSearchKeyword(text, type))
      .on('mouseover', (d) => {
        d3.select('#search-kw-x-' + text_no_spaces).style(
          'text-decoration',
          'underline',
        )
      })
      .on('mouseout', (d) => {
        d3.select('#search-kw-x-' + text_no_spaces).style(
          'text-decoration',
          'none',
        )
      })
      .style('color', '#fff')
      .html('x')
    var statsString = ''
    for (let type in stats) {
      statsString += ', ' + stats[type] + ' ' + type
    }
    if (statsString == '') {
      statsString = '  0 results'
    }
    div
      .append('div')
      .style('display', 'inline-block')
      .html(text + ' (' + statsString.substring(2) + ')')
  }

  //////////////////////////////////////////////////////////////////////////////
  // DRAWING AND CLEAN
  //////////////////////////////////////////////////////////////////////////////
  gsl.draw = function () {
    d3.select('#gsl-scene-svg').attr('visibility', 'hidden')
    if (_data.narrative().scenes().length == 0) {
      _chart.drawNoData(_backgroundInteractions)
    } else {
      _chart.draw(
        _data.narrative(),
        _entityInteractions,
        _backgroundInteractions,
        _scenesInteractions,
      )
    }
    gsl.updateSelected()
    _chart.resetScrollbars()
    d3.select('#gsl-scene-svg').attr('visibility', 'visible')
    return gsl
  }

  gsl.reset = function () {
    d3.select('.gsl-container').remove()
    d3.select('.demo-vis').append('div').attr('class', 'gsl-container')
    _uiSelectors.reset()
    return gsl
  }

  //////////////////////////////////////////////////////////////////////////////
  // UPDATE
  //////////////////////////////////////////////////////////////////////////////
  gsl.updateNarrative = function () {
    const selections = _uiSelectors.getAllSelections()
    const datasetName = _data.datasetName()
    var url = getScenesURL(selections, datasetName)
    d3.json(url)
      .then(function (data) {
        const locations = gsl.constructScenes(data)
        gsl.drawMap(data.map)
        _data.setNodesDict(data.nodes_dict)
        gsl.createAllConatinersperLocation(locations, data.scenes, data.nodes_dict)
        gsl.drawMapLines(data.map)
        $(".gsl-storylines").scroll(function () {
          gsl.drawMapLines(data.map)
        });
      })
  }
  gsl.constructScenes = function (data) {
    const locations = {}
    const locationsNames = {}
    const nodes = data.nodes_dict
    for (let nId in nodes) {
      if (nodes[nId].type == 'locations') {
        locations[nId] = []
        locationsNames[nodes[nId].name] = nodes[nId]
      }
    }
    data.scenes.forEach(e => {
      e.allRelatedEntities.forEach(nId => {
        if (nId in locations && locations[nId].indexOf(e.id) < 0) {
          locations[nId].push(e.id)
        }
      })
    })
    data.map.features.forEach(f => {
      if (f.properties.name in locationsNames) {
        f.properties.node = locationsNames[f.properties.name]
      } else {
        const firstKey = Object.keys(nodes)[0]
        const extraNode = JSON.parse(JSON.stringify(nodes[firstKey]))
        extraNode.id = f.properties.name
        extraNode.name = f.properties.name
        extraNode.type = 'locations'
        extraNode.isFakeLocation = true
        extraNode.related_edges = []
        for (let dim in extraNode.related_nodes)
          extraNode.related_nodes[dim] = []
        f.properties.node = extraNode
      }
    })
    return locations
  }

  gsl.drawMap = function (map) {
    _projection = d3.geoMercator()
      .rotate([0, 0, _rotation])
      // PAPER CONF
      //.fitExtent([[250, 40], [_mapSize+250, _mapSize-160]], map);
      .fitExtent([[10, 100], [_mapSize - 10, _mapSize]], map);
    const path = d3.geoPath().projection(_projection)
    map.features.forEach(f => {
      if (f.properties.hierarchical_level == 'parent') return
      const c = path.centroid(f)
      _centroids[f.properties.name] = c
      if (f.properties.name == "United States")
        _centroids[f.properties.name] = _projection([-101.68, 41.08])
      if (f.properties.name == "Argentina")
        _centroids[f.properties.name] = _projection([-65.62, -36.25])
    })

    d3
      .select('.gsl-map')
      .select('svg')
      .on('click', d => _backgroundInteractions.click())

    d3
      .select('.gsl-map')
      .select('svg')
      .append('g')
      .attr('id', 'map-locations')
      .selectAll('path')
      .data(() => map.features.filter(f => f.properties.hierarchical_level == 'child'))
      .enter()
      .append('path')
      .attr('class', 'path-map-location')
      .attr('d', path)
      .style('stroke', d => {
        if (d.properties.highlighted) return 'red'
        return '#252525'
      })
      .style('stroke-width', d => {
        if (d.properties.highlighted) return '3'
        else return 1
      })
      .style('stroke-dasharray', 2)
      .style('fill', d => {
        return '#d9d9d9'
      })
      .on('mouseover', d => _entityInteractions.mouseOver(d.properties.name))
      .on('mouseout', d => _entityInteractions.mouseOut())
      .on('click', function (d) {
        _entityInteractions.click(d.properties.node, 'map')
        d3.select(this).raise()
      })
    d3
      .select('.gsl-map')
      .select('svg')
      .append('g')
      .attr('class', 'map-features-parent')
      .selectAll('.path')
      .data(() => map.features.filter(f => f.properties.hierarchical_level == 'parent'))
      .enter()
      .append('path')
      .attr('d', path)
      .style('stroke', d => d.properties.color)
      .style('stroke', '#000')
      .style('stroke-width', '2px')
      .style('fill', 'none')
      .style('fill-opacity', 0)
      .style('stroke-opacity', 1)

    d3
      .select('.gsl-map')
      .select('svg')
      .selectAll('circle')
      .data(() => {
        const newData = []
        map.features.forEach(f => {
          if (f.properties.node.isFakeLocation) return
          const tmp = {}
          tmp.coords = _centroids[f.properties.node.name]
          tmp.name = f.properties.name
          tmp.character = f.properties.node
          newData.push(tmp)
        })
        return newData
      })
      .enter()
      .append('circle')
      .attr('r', 3)
      .attr('class', 'appearance')
      .attr('cx', d => d.coords[0])
      .attr('cy', d => d.coords[1])
      .on('mouseover', d => _entityInteractions.mouseOver(d.name))
      .on('mouseout', d => _entityInteractions.mouseOut())
      .on('click', d => _entityInteractions.click(d.character, 'map'))

  }
  gsl.drawMapLines = function (map) {
    const isSelectionActive = _data.isSelectionActive()
    const selections = _uiSelectors.getAllSelections()
    d3.selectAll('.link-map').remove()
    var domainV = 0
    d3
      .select('.gsl-map')
      .select('svg')
      .selectAll('.link-map')
      .data(() => {
        const newData = []
        map.features.forEach(f => {
          if (f.properties.node.isFakeLocation) return
          const div = d3.select('#storyline-' + cleanName(f.properties.name, ' ', '_')).node()
          const bbox = div.getBoundingClientRect()
          const coords = _centroids[f.properties.node.name]
          const point = []
          point[0] = [coords[0], coords[1]] // source
          point[1] = [bbox.x + 10, bbox.y - 100] // target
          const tmp = {}
          tmp.point = point
          tmp.name = f.properties.name
          tmp.node = f.properties.node
          newData.push(tmp)
          domainV += bbox.height
        })
        return newData
      })
      .enter()
      .append('path')
      .attr('class', 'link-map')
      .attr('d', d => narrativeLink(d.point))
      .style('fill', 'none')
      .style('stroke', (d) => {
        if (d.node.highlighted) return 'red'
        const isSelected = _data.isEntitySelected(d.node, selections)
        const lineC = style.getLineColor(isSelected, false)
        if (isSelected) {
          d3
            .select('#storyline-' + cleanName(d.node.name))
            .select('.gsl-legend-y')
            .style('border-left', '5px solid ' + lineC)
        } else {
          d3
            .select('#storyline-' + cleanName(d.node.name))
            .select('.gsl-legend-y')
            .style('border-left', 'none')
        }
        return lineC
      })
      .style('opacity', (d) => {
        if (d.node.highlighted) return 1
        const isSelected = _data.isEntitySelected(d.node, selections)
        const isRelatedSelected = _data.isEntityRelatedSelected(
          d.node,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-width', '3px')

      .style('cursor', 'pointer')
      .style('pointer-events', 'visibleStroke')
      .on('mouseover', d => _entityInteractions.mouseOver(d.name))
      .on('mouseout', d => _entityInteractions.mouseOut())
      .on('click', d => _entityInteractions.click(d.node, 'map'))


    _scrollbars.updateSize(_scrollbars.horizontalDomain(), domainV)

  }

  //////////////////////////////////////////////////////////////////////////////
  // REACT TO UI SELECTORS
  //////////////////////////////////////////////////////////////////////////////
  gsl.changeDimension = function () {
    _uiSelectors.updateAggregators()
    gsl.updateNarrative()
  }

  gsl.changeAggregation = function () {
    gsl.updateNarrative()
  }

  //////////////////////////////////////////////////////////////////////////////
  // REACT TO SELECTION
  //////////////////////////////////////////////////////////////////////////////
  gsl.select = function (entity) {
    _data.toggleSelection(entity)
    if (entity.type == 'locations' && _data.isEntitySelected(entity)) {
      const div = d3
        .select('#storyline-' + cleanName(entity.name))
      if (!div.empty()) {
        _scrollbars.scrollIntoView(div.node())
      }
    }
    gsl.updateSelected()
  }

  gsl.updateSelected = function () {
    const isSelectionActive = _data.isSelectionActive()
    const selections = _uiSelectors.getAllSelections()
    for (let locId in _charts) {
      _charts[locId].updateSelected(_data, selections)
    }
    _xAxis.updateSelected(_data, selections)
    _scrollbars.updateSelected(_data, selections, _commonNarrative.scenes())

    d3
      .select('.gsl-map')
      .select('svg')
      .selectAll('.path-map-location')
      .style('stroke', (d) => {
        if (d.properties.highlighted) return 'red'
        const isSelected = _data.isEntitySelected(d.properties.node, selections)
        return style.getSceneCircleFillColor(isSelected)
      })
      .style('fill-opacity', (d) => {
        const isSelected = _data.isEntitySelected(d.properties.node, selections)
        const isRelatedSelected = _data.isEntityRelatedSelected(
          d.properties.node,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-opacity', d => {
        if (d.properties.highlighted) return 'red'
        const isSelected = _data.isEntitySelected(d.properties.node, selections)
        const isRelatedSelected = _data.isEntityRelatedSelected(
          d.properties.node,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
      .style('stroke-width', (d) => {
        if (d.properties.highlighted) return '3'
        const isSelected = _data.isEntitySelected(d.properties.node, selections)
        const isRelatedSelected = _data.isEntityRelatedSelected(
          d.properties.node,
          selections,
        )
        if (isSelectionActive && isSelected) return '3'
        return '1'
      })

    d3
      .select('.gsl-map')
      .select('svg')
      .selectAll('.link-map')
      .style('stroke', (d) => {
        if (d.node.highlighted) return 'red'
        const isSelected = _data.isEntitySelected(d.node, selections)
        const lineC = style.getLineColor(isSelected, false)
        if (isSelected) {
          d3
            .select('#storyline-' + cleanName(d.node.name))
            .select('.gsl-legend-y')
            .style('border-left', '5px solid ' + lineC)
        } else {
          d3
            .select('#storyline-' + cleanName(d.node.name))
            .select('.gsl-legend-y')
            .style('border-left', 'none')
        }
        return lineC
      })
      .style('opacity', (d) => {
        if (d.node.highlighted) return 1
        const isSelected = _data.isEntitySelected(d.node, selections)
        const isRelatedSelected = _data.isEntityRelatedSelected(
          d.node,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
    d3
      .select('.gsl-map')
      .select('svg')
      .selectAll('.appearance')
      .style('fill', (d) => {
        if (d.characterhighlighted) return 'red'
        const isSelected = _data.isEntitySelected(d.character, selections)
        return style.getSceneCircleFillColor(isSelected, false)
      })
      .style('opacity', (d) => {
        if (d.character.highlighted) return 1
        const isSelected = _data.isEntitySelected(d.character, selections)
        const isRelatedSelected = _data.isEntityRelatedSelected(
          d.character,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
  }

  gsl.resetSelected = function () {
    _data.resetSelected()
    gsl.updateSelected()
  }

  //////////////////////////////////////////////////////////////////////////////
  // DATASET SELECTOR
  //////////////////////////////////////////////////////////////////////////////
  gsl.changeDataset = function (name) {
    d3.json('/geostorylines/get_data/?q=' + name).then((data) => {
      gsl.data(name, data).reset().show();
      d3.select('#dataset-selector-label').node().innerHTML = name
    })
  }
  return gsl
}
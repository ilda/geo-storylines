export function helpText() {
  var result = `  <br>
  <h2> Time Glyphs </h2>
  <br>
  <ul> 
      <li>
        - The main part is the map on the left side of the screen. 
        If a location on the map is filled in, then this means that at least one event is related to it. 
      </li>
      <br>
      <br>
      <li>
        - Each filled location is connected to its own timeline. 
        The lines in this timeline show <b class="highlightText">when</b> the events in that 
        location happen, and <b class="highlightText">who</b> is related to them.
      </li>
      <br>
      <br>
      <li>
      - Each line represents a person’s <b class="highlightText">story</b>.
      As a line progresses along the x-axis, it shows events for that person in chronological order. 
      </li>
      <br>
      <br>
      <li>
      - One person’s line will appear multiple times: once for each location where they are related to an event.  
      </li>
      <br>
      <br>
      <li>
      - Events are represented by bar symbols on a person’s line. An event can involve more than one person. 
      When two lines curve to come together in an event, this symbolizes a <b class="highlightText">relationship</b> 
      between the people represented by those lines. The lines join in a relationship bar. 
      </li>
      <br>
      <br>
      <li>
      - You can <b class="highlightText">highlight</b> any type of entity by clicking on it: people’s lines, 
      places on the map, and dates in the timeline. 
      </li>
      <br>
      <br>
      <li>
      - Make sure to <b class="highlightText">scroll up and down</b> to see the information for all the locations! 
      </li>
      <br>
      <br>
      <li>
      - The name of the last clicked entity or date will be copied in the clipboard. 
      You can use this feature to copy answers or search for information.
      </li>
      </ul>`
  return result
}
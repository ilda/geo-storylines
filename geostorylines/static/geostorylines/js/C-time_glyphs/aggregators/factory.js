import defaultAggregator from './default.js'

export default function aggregatorFactory(type) {
  return defaultAggregator(type)
}
export default function scenesStyle(characteristic) {
  var style = {
    labelSize: [195, 15],
    pathSpace: 10,
    groupMargin: 5,
    leftPadding: 5,
    rightPadding: 5,
    sceneWidth: 4,
    scenePadding: 2
  }
  return style[characteristic]
}

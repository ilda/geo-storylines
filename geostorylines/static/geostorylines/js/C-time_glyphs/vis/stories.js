import lines from './lines.js'
import scenes from './scenes.js'

export default function stories() {
  var stories = {}
  var _lines
  var _scenes
  var _nestedScenes
  stories.init = function () {
    _lines = lines()
    _scenes = scenes()
    return stories
  }
  stories.draw = function (narrative, entityInteractions, scenesInteractions, divId) {
    _lines.draw(narrative, entityInteractions, divId)
    _scenes.draw(narrative, scenesInteractions, divId)
    return stories
  }
  stories.updateSelected = function (data, selections) {
    _lines.updateSelected(data, selections)
    _scenes.updateSelected(data, selections)
  }
  stories.update = function (narrative, animationDuration, select) {
    const promises_scenes = _scenes.update(narrative, animationDuration)
    const promises_lines = _lines.update(narrative, animationDuration)
    return promises_scenes.concat(promises_lines)
  }
  stories.createContainers = function (divId) {
    _lines.createContainers(0, divId)
    _scenes.createContainers(0, divId)
    _lines.createContainers(1, divId)
    _scenes.createContainers(1, divId)

    _lines.createContainersOpenScenes(0, divId)
    _scenes.createContainersOpenScenes(0, divId)
    _lines.createContainersOpenScenes(1, divId)
    _scenes.createContainersOpenScenes(1, divId)
  }

  stories.clean = function () {
    _lines.clean()
    _scenes.clean()
  }

  return stories.init()
}
import verticalScrollbar from './scrollbar-vertical.js'
import horizontalScrollbar from './scrollbar-horizontal.js'
import * as style from './style/stories.js'

export default function scrollbars() {
  var scrollbars = {}
  var _vertical
  var _horizontal
  var _chart
  scrollbars.init = function () {
    _vertical = verticalScrollbar()
    _horizontal = horizontalScrollbar()

    /*function pan() {
      const deltaY = d3.event.deltaY
      const deltaX = d3.event.deltaX
      if (deltaY != 0) {
        var move = 40
        if (Math.abs(deltaY) < 20) {
          move = 2
        }
        var sign = deltaY && deltaY / Math.abs(deltaY)
        if (d3.event.shiftKey) {
          _horizontal.moveDelta(move * sign)
        } else {
          _vertical.moveDelta(move * sign)
        }
      } else if (Math.abs(deltaX) != 0) {
        var sign = deltaX && deltaX / Math.abs(deltaX)
        var move = 40
        if (deltaX < 20) {
          move = 2
        }
        _horizontal.moveDelta(move * sign)
      }
      d3.event.stopPropagation()
    }
    var zoomer = d3.zoom()
    var svg = d3
      .selectAll(".gsl-scene")
      .call(zoomer)
      .on("wheel.zoom", pan)*/

    return scrollbars
  }
  scrollbars.chart = function (chart) {
    _chart = chart
    _vertical.chart(chart)
    _horizontal.chart(chart)
    return scrollbars
  }
  // Not sure if there are too many functions or not
  scrollbars.verticalDomain = function (value) {
    if (value == undefined)
      return _vertical.domain()
    _vertical.domain(value)
    return scrollbars
  }
  scrollbars.horizontalDomain = function (value) {
    if (value == undefined)
      return _horizontal.domain()
    _horizontal.domain(value)
    return scrollbars
  }
  scrollbars.verticalRange = function (value) {
    _vertical.range(value)
    return scrollbars
  }
  scrollbars.horizontalRange = function (value) {
    _horizontal.range(value)
    return scrollbars
  }
  scrollbars.createContainers = function () {
    _vertical.createContainers()
    _horizontal.createContainers()
    return scrollbars
  }
  scrollbars.updateSize = function (domainWidth, domainHeight, left) {
    _horizontal
      .domain(domainWidth)
    _horizontal.updateSize()
    _vertical
      .domain(domainHeight)
    _vertical.updateSize()
    return scrollbars
  }
  scrollbars.updateRange = function () {
    setTimeout(() => {
      _vertical.updateSize().reDrawSelected()
      _horizontal.updateSize().reDrawSelected()
    }, 1)

  }
  scrollbars.updateSelected = function (data, selections, scenes) {
    _vertical.clean()
    _horizontal.clean()
    scenes.forEach(scene => {
      if (scene.isOpen) {
        return
      }
      var isSelected = data.isSceneSelected(scene, selections)
      var isRelatedSelected = data.isSceneRelatedSelected(scene)
      if (isSelected || isRelatedSelected) {
        const lineColor = style.getSceneScrollColor(isSelected)
        _vertical.addLine(scene.y, lineColor)
        _horizontal.addLine(scene.x, lineColor)
      }
    })
    d3
      .select('.gsl-storylines')
      .selectAll('.gsl-storylines-container')
      .each(function (n, i) {
        const div = d3.select(this).node()
        const bbox = div.getBoundingClientRect()
        d3
          .select(this)
          .selectAll('.scene')
          .each(scene => {
            var isSelected = false
            var isRelatedSelected = false
            scene.allRelatedEntities.forEach(function (element) {
              if (data.isSelected(element)) {
                isSelected = true
              }
            })
            scene.characters.forEach(function (element) {
              // TO FIX!
              if (element == undefined)
                return
              if (data.isRelatedSelected(element.id)) {
                isRelatedSelected = true
              }
            })
            if (isSelected || isRelatedSelected) {
              const lineColor = style.getSceneScrollColor(isSelected)
              _vertical.addLine(scene.y + bbox.y - 100, lineColor)
            }
          })
      })
  }
  scrollbars.reset = function () {
    _horizontal.reset()
    _vertical.reset()
  }
  scrollbars.scrollIntoView = function (element) {
    _vertical.scrollIntoView(element)
    return scrollbars
  }
  return scrollbars.init()
}
import axes from './axes.js'
import stories from './stories.js'
import scrollbars from './scrollbars.js'
import layout from './style/layout.js'
import * as style from './style/stories.js'

export default function chart(locationNode, scenes) {
  var chart = {
    width: 1000,
    height: 1000,
    padding: 100,
    left: layout('yAxisSize')
  }
  var _axes
  var _stories
  var _scrollbars
  var _divId
  var _locationNode
  var _scenes
  chart.init = function (locationNode, scenes) {
    _axes = axes()
    _stories = stories()
    _locationNode = locationNode
    _scenes = scenes
    return chart
  }
  chart.size = function (width, height) {
    chart.width = width 
    chart.height = height
    return chart
  }
  chart.createContainers = function (divId) {
    _divId = divId
    d3.select('#' + divId)
      .select('.gsl-scene')
      .append('svg')
      .attr('id', 'gsl-scene-svg')
      .attr('width', chart.width + chart.padding)
      .attr('height', chart.height + chart.padding)
      .append('g')
      .attr('id', 'gsl-main')
    _stories.createContainers(divId)
    _axes.createContainers(divId)
    return chart
  }
  chart.updateSizeNarrative = function (narrative) {
    const scenes = narrative.scenes()
    const nScenes = scenes.length
    const chartDimensions = chart.getDimensions(nScenes)
    chart.width = chartDimensions[0]
    chart.height = narrative.getGreatestYValue()
    chart.updateSize(chart.left)
    return chart
  }
  chart.updateSizeShift = function (narrative, width, height) {
    chart.width += width
    chart.height += height
    chart.updateSize(chart.left)
    return chart
  }
  chart.updateSize = function (left) {
    d3
      .select('#' + _divId)
      .select('#gsl-scene-svg')
      .attr('width', chart.width + chart.padding)
      .attr('height', chart.height + chart.padding)
    _axes.updateSize(chart.width, chart.height + chart.padding, left, _divId)
    d3
      .select('#' + _divId)
      .style('height', (chart.height + chart.padding + 30) + 'px')
    return chart
  }
  chart.getDimensions = function (nScenes) {
    const w = 500 + nScenes * 30
    const h = 500 + nScenes * 50
    return [w, h]
  }
  chart.stories = function () {
    return _stories
  }
  chart.axes = function () {
    return _axes
  }
  chart.draw = function (narrative, entityInteractions, backgroundInteractions, scenesInteractions) {
    d3
      .select('#' + _divId)
      .select('#gsl-main')
      .attr('transform', 'translate(' + (-layout('yAxisSize')) + ',25)')
    d3
      .select('#' + _divId)
      .select('#gsl-scene-svg')
      .style('background-color', '#f7f7f7')
      .on('click', d => backgroundInteractions.click())
    _axes.draw(narrative, chart.height + chart.padding, entityInteractions, backgroundInteractions, _divId)
    _stories.draw(narrative, entityInteractions, scenesInteractions, _divId)
    return chart
  }
  chart.update = function (narrative, animationDuration) {
    const promises_stories = _stories.update(narrative, animationDuration)
    const promises_axes = _axes.update(narrative, animationDuration)
    return promises_stories.concat(promises_axes)
  }
  chart.clean = function () {
    _stories.clean()
    _axes.clean()
    d3.select('#' + _divId).select('#gsl-main').selectAll('.intro').remove()
    d3.select('#' + _divId).select('#gsl-main').selectAll('.no-data').remove()
    return chart
  }
  chart.updateSelected = function (data, selections) {
    _stories.updateSelected(data, selections)
    _axes.updateSelected(data, selections)
    const isSelectionActive = data.isSelectionActive()
    if (_locationNode != undefined) {
      const isSelected = data.isEntitySelected(_locationNode, selections)
      const isRelatedSelected = data.isEntityRelatedSelected(
        _locationNode,
        selections,
      )
      const opacity = style.getOpacity(
        isSelectionActive,
        isSelected,
        isRelatedSelected,
        false,
      )
      d3.select('.gsl-legend-x-' + _locationNode.id).style('opacity', opacity)
    } else {
      // No locations timeline
      var flag = false
      _scenes.forEach(s => {
        const isSceneSelected = data.isSceneSelected(s, selections)
        flag = flag || isSceneSelected
      })
      const opacity = style.getOpacity(
        isSelectionActive,
        flag,
        false,
        false,
      )
      d3.select('.gsl-legend-x-no-locations').style('opacity', opacity)
    }
    return chart
  }
  chart.drawNoData = function (backgroundInteractions) {
    chart.clean()
    const width = $('.gsl-scene').width()
    const height = $('.gsl-scene').height()
    d3.select('#gsl-main')
      .attr('transform', 'translate(' + (-chart.left) + ',25)')
    d3.select('#gsl-scene-svg')
      .attr('width', width + chart.left)
      .attr('height', height)
    d3.select('#gsl-legend-y-svg')
      .attr('width', chart.left)
      .attr('height', height)
    d3.select('#scene_yLegend')
      .attr('width', chart.left)
    d3.select('#scene_yLegend_noreal')
      .attr('width', chart.left)
    _axes.drawNoData(width, height, chart.left, backgroundInteractions)
    d3
      .select('#gsl-main')
      .append('text')
      .text('No data')
      .attr('class', 'no-data')
      .attr('x', width / 2)
      .attr('y', height / 2)
      .style('font-size', '20px')

  }
  chart.resetScrollbars = function () {
    _scrollbars.reset()
  }
  return chart.init(locationNode, scenes)
}
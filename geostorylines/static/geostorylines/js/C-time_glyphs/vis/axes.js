import xAxis from './axis-x.js'
import yAxis from './axis-y.js'

export default function axes() {
  var axes = {}
  var _xAxis
  var _yAxis
  axes.init = function () {
    _xAxis = xAxis()
    _yAxis = yAxis()
    return axes
  }
  axes.yAxis = function () {
    return _yAxis
  }
  axes.xAxis = function () {
    return _xAxis
  }
  axes.updateSize = function (chartWidth, chartHeight, left, divId) {
    //_xAxis.updateSize(chartWidth, left, divId)
    _yAxis.updateSize(chartHeight, left, divId)
    return axes
  }
  axes.clean = function () {
    //_xAxis.clean()
    _yAxis.clean()
  }
  axes.draw = function (narrative, chartHeight, entityInteractions, backgroundInteractions, divId) {
    //_xAxis.draw(narrative, chartHeight, entityInteractions, backgroundInteractions, divId)
    _yAxis.draw(narrative, chartHeight, entityInteractions, backgroundInteractions, divId)
    return axes
  }
  axes.update = function (narrative, animationDuration) {
    //const promises_x = _xAxis.update(narrative, animationDuration)
    const promises_y = _yAxis.update(narrative, animationDuration)
    return promises_x.concat(promises_y)
  }
  axes.updateSelected = function (data, selections) {
    //_xAxis.updateSelected(data, selections)
    _yAxis.updateSelected(data, selections)
    return axes
  }
  axes.createContainers = function (divId) {
    //_xAxis.createContainers(divId)
    _yAxis.createContainers(divId)
    return axes
  }
  axes.drawNoData = function (width, height, left, backgroundInteractions) {
    _yAxis.drawNoData(left, height, backgroundInteractions)
  }

  return axes.init()
}
import * as style from './style/stories.js'
import layout from './style/layout.js'

export default function xAxis() {
  var axis = {}
  var _values = []
  //var _extraValues = []
  var _height
  axis.init = function () {
    _height = 100
    return axis
  }
  axis.createContainers = function (divId) {
    d3
      .select('.gsl-legend-x-main')
      .append('svg')
      .attr('id', 'gsl-legend-x-svg')
      .append('g')
      .attr('id', 'gsl-legend-x-g')
    d3
      .select('.gsl-storylines')
      .selectAll('.gsl-storylines-container')
      .selectAll('#gsl-main')
      .append('g')
      .attr('id', 'gsl-legend-x-g')
      .lower()
    d3
      .selectAll('.gsl-legend-x-extra')
      .append('svg')
      .attr('id', 'gsl-legend-x-extra-svg')
      .append('g')
      .attr('id', 'gsl-legend-x-extra-g')
    return axis
  }
  axis.updateSize = function (chartWidth, left, divId) {
    d3
      .selectAll('#gsl-legend-x-svg')
      .attr('width', chartWidth + 17)
      .attr('height', _height)
    d3
      .selectAll('#gsl-legend-x-extra-svg')
      .attr('width', chartWidth + 17)
      .attr('height', 35)
    return axis
  }
  axis.draw = function (
    narrative,
    chartHeight,
    entityInteractions,
    backgroundInteractions,
    divId
  ) {
    d3
      .select('.gsl-legend-x-main')
      .select('#gsl-legend-x-svg')
      .select('#gsl-legend-x-g')
      .attr('transform', 'translate(' + -layout('yAxisSize') + ',60)')
    computeValues(narrative)
    drawLines(backgroundInteractions, chartHeight, divId)
    drawLabels(entityInteractions, divId)
    return axis
  }
  axis.clean = function () {
    d3.selectAll('#gsl-legend-x-g').selectAll('*').remove()
    return axis
  }
  axis.update = function (narrative, animationDuration) {
    computeValues(narrative)
    const promises = []
    const p_li = updateLines(animationDuration)
    //if(p_li !== undefined) {
    promises.push(p_li)
    //}
    const p_la = updateLabels(animationDuration)
    //if(p_la !== undefined){
    promises.push(p_la)
    //}
    return promises
  }
  axis.updateSelected = function (data, selections) {
    const isSelectionActive = data.isSelectionActive()
    d3.select('.gsl-legend-x-main')
      .selectAll('#gsl-legend-x-svg')
      .selectAll('.xDimLegend')
      .selectAll('text')
      .style('fill', (d) => {
        const isSelected = data.isEntitySelected(d.keyNode, selections)
        return style.getTextColor(isSelected)
      })
      .style('font-weight', (d) => {
        const isSelected = data.isEntitySelected(d.keyNode, selections)
        return style.getTextFontWeight(isSelected)
      })
      .style('fill-opacity', (d) => {
        const isSelected = data.isEntitySelected(d.keyNode, selections)
        const isRelatedSelected = data.isEntityRelatedSelected(
          d.keyNode,
          selections,
        )
        return style.getOpacity(
          isSelectionActive,
          isSelected,
          isRelatedSelected,
        )
      })
  }
  function computeValues(narrative) {
    const scenes = narrative.scenes()
    //console.log(scenes)
    _values = []
    //_extraValues = []
    //var flagExtraValues = false
    var listXDict = {}
    for (var index in scenes) {
      var aScene = scenes[index]
      /*if (aScene.keyNode.type == 'metatime') {
        flagExtraValues = true
      }*/
      if (aScene.key in listXDict) {
        listXDict[aScene.key]['ids'].push(aScene.id)
        if (aScene.x < 0) {
          continue
        }
        if (listXDict[aScene.key]['xpos'] < 0 && aScene.x > 0) {
          listXDict[aScene.key]['xpos'] = aScene.x
        } else if (listXDict[aScene.key]['xpos'] > aScene.x) {
          listXDict[aScene.key]['xpos'] = aScene.x
        }
      } else {
        listXDict[aScene.key] = { xpos: aScene.x }
        listXDict[aScene.key]['ids'] = [aScene.id]
        listXDict[aScene.key]['keyNode'] = aScene.keyNode
      }
    }
    for (var key in listXDict) {
      var elto = listXDict[key]
      if (elto.keyNode.isFake) {
        elto['label'] = key.substring(0, 7)
      } else {
        elto['label'] = key
      }
      _values.push(elto)
    }
  }
  function drawLines(backgroundInteractions, height, divId) {
    // LINES INSIDE THE VIS
    var legendGroups = d3
      .selectAll('.gsl-scene')
      .selectAll('#gsl-legend-x-g')
      .selectAll('.xDimLegend')
      .data(_values)
      .enter()
      .append('g')
      .attr('class', 'xDimLegend')
      .attr('transform', function (d, i) {
        return 'translate(' + d.xpos + ',' + 0 + ')'
      })
    legendGroups
      .append('line')
      .style('stroke-width', 5)
      .style('stroke', '#fcfcfc')
      .attr('x1', function (d) {
        return 0
      })
      .attr('x2', function (d) {
        return 0
      })
      .attr('y1', function (d) {
        return -300
      })
      .attr('y2', function (d) {
        return 20000 //height
      })
      .on('click', (d) => backgroundInteractions.click())

    // GRAY LINES
    var legendGroupsGray = d3
      .selectAll('#gsl-legend-x-extra-g')
      .selectAll('.xDimLegend')
      .data(_values)
      .enter()
      .append('g')
      .attr('class', 'xDimLegend')
      .attr('transform', function (d, i) {
        return 'translate(' + d.xpos + ',' + 0 + ')'
      })
    legendGroupsGray
      .append('line')
      .style('stroke-width', 5)
      .style('stroke', '#f0f0f0')
      .style("stroke-dasharray", ("4, 4"))
      .attr('x1', function (d) {
        return 0
      })
      .attr('x2', function (d) {
        return 0
      })
      .attr('y1', function (d) {
        return -300
      })
      .attr('y2', function (d) {
        return 20000 //height
      })
      .on('click', (d) => backgroundInteractions.click())
  }

  function drawLabels(entityInteractions, divId) {
    var legendGroupsLegend = d3
      .selectAll('#gsl-legend-x-svg')
      .selectAll('#gsl-legend-x-g')
      .selectAll('.xDimLegend')
      .data(_values)
      .enter()
      .append('g')
      .attr('class', 'xDimLegend')
      .attr('id', (d) => 'xaxis_' + d.keyNode.id)
      .attr('transform', function (d, i) {
        return 'translate(' + d.xpos + ',' + 0 + ')'
      })

    legendGroupsLegend
      .append('text')
      .text(function (d, i) {
        return d.label
      })
      .attr('transform', function () {
        return 'rotate(-45)'
      })
      .on('mouseover', (d) => entityInteractions.mouseOver(d.label))
      .on('mouseout', (d) => entityInteractions.mouseOut())
      .on('click', (d) =>
        entityInteractions.click(d.keyNode, 'xDim', d.label),
      )
  }

  function updateLines(animationDuration) {
    const transition = d3
      .select('#gsl-main')
      .selectAll('.xDimLegend')
      .data(_values)
      .transition()
      .duration(animationDuration)
      .attr('transform', function (d, i) {
        return 'translate(' + d.xpos + ',' + 0 + ')'
      })
    if (!transition.empty()) {
      return transition.end()
    } else {
      return undefined
    }
  }

  function updateLabels(animationDuration) {
    const transition = d3
      .select('#gsl-legend-x-svg')
      .select('g')
      .selectAll('.xDimLegend')
      .data(_values)
      .transition()
      .duration(animationDuration)
      .attr('transform', function (d, i) {
        return 'translate(' + d.xpos + ',' + 0 + ')'
      })
    if (!transition.empty()) {
      return transition.end()
    } else {
      return undefined
    }
  }
  return axis.init()
}

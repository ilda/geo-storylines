export function getTranslateCoordinates(node) {
  const transform = node.getAttribute('transform')
  const indexStartParentesis = transform.indexOf('(')
  const indexComma = transform.indexOf(',')
  const result = {}
  result.x = Number(transform.substring(indexStartParentesis + 1, indexComma))
  result.y = Number(transform.substring(indexComma + 1, transform.length - 1))
  return result
}

export function getLinkGroupId(d, i) {

  var result = 'link-S-'
  var source = d.source
  if (source.scene) {
    result += 'scene-' + source.scene.id + '-'
  }
  result += 'char-' + source.character.id
  result += '-T-'
  var target = d.target
  if (target.scene) {
    result += 'scene-' + target.scene.id + '-'
  }
  result += 'char-' + target.character.id
  return result
}

export function nameCutter(name) {
  return name.substring(0, 20)
}

function escapeRegExp(string) {
  return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
}

export function replaceAll(str, find, _replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), _replace)
}

export function cleanName(string){
  var result = string
  const to_replace = ['-', '(', ')', ' ', '.']
  to_replace.forEach(c => {
    result = replaceAll(result, c, '')
  })
  return result
}


String.prototype.getAllIndices = function (strFind) {
  const regex = new RegExp(strFind, "ig");
  return this.matchAll(regex);
};

export function getScenesURL(selections, datasetName) {
  var url = '/geostorylines/get_scenes/?name=' + datasetName
  for (let d in selections.dim) {
    url += '&' + d + 'Dim=' + selections.dim[d]
  }
  for (let a in selections.aggr) {
    url += '&' + a + 'Aggr=' + selections.aggr[a]
  }
  url += '&xDimOrder=' + selections.order.x
  url += '&mapName=' + 'regions_simple_2.json'
  url += '&mapCountry=' + 'FR'
  return url
}

export function getNestedScenesURL(scene, selections, datasetName) {
  var url = '/geostorylines/get_nested_scenes/?name=' + datasetName
  for (let d in selections.dim) {
    url += '&' + d + 'Dim=' + selections.dim[d]
  }
  for (let a in selections.aggr) {
    url += '&' + a + 'Aggr=' + selections.aggr[a]
  }
  url += '&scene_edges='
  for (let s in scene.edge.original_edges_ids) {
    const edge_id = scene.edge.original_edges_ids[s]
    url += edge_id
    if (s < scene.edge.original_edges_ids.length - 1) {
      url += '+'
    }
  }
  url += '&ZDimOrder=' + selections.order.z
  return url
}

export function narrativeLink(d) {
  var curvature = 0.5;
  var x0, x1, y0, y1, cx0, cy0, cx1, cy1, ci;
  x0 = d[0][0]
  y0 = d[0][1]
  x1 = d[1][0]
  y1 = d[1][1]
  ci = d3.interpolateNumber(x0, x1);
  cx0 = ci(curvature);
  cy0 = y0;
  cx1 = ci(1 - curvature);
  cy1 = y1;
  return "M" + x0 + "," + y0 +
    "C" + cx0 + "," + cy0 +
    " " + cx1 + "," + cy1 +
    " " + x1 + "," + y1;
}
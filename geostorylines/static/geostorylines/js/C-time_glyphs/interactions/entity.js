import logInteraction from '../interactions/log.js'

export default function entityInteractions() {
  var interaction = {
    clickedOnce: false,
    timerClick: undefined
  }
  var _gsl
  interaction.gsl = function (gsl) {
    _gsl = gsl
    return interaction
  }
  interaction.click = function (entity, dim, label) {
    d3.event.stopPropagation();
    if (interaction.clickedOnce) {
      interaction.runDoubleClick(entity, dim, label)
    } else {
      interaction.timerClick = setTimeout(function () {
        interaction.runSimpleClick(entity, dim, label)
      }, 230)
      interaction.clickedOnce = true
    }
  }
  interaction.runSimpleClick = function (entity, dim, label) {
    click(entity, dim, label)
    interaction.clickedOnce = false
  }
  interaction.runDoubleClick = function (entity, dim, label) {
    interaction.clickedOnce = false
    clearTimeout(interaction.timerClick)
    doubleClick(entity, dim, label)
  }
  function click(entity, dim, label) {
    _gsl.select(entity)
    navigator.clipboard.writeText(entity.name).then(function () {
      d3
        .select('.copy-label')
        .html('Copied: ' + entity.name)
    }, function (err) {
      //console.error('Async: Could not copy text: ', err);
    });
    const interaction = {}
    interaction.type = 'select-' + entity.type
    interaction.argument = entity.name
    logInteraction(interaction)
  }
  function doubleClick(entity, dim, label) {
    
  }
  interaction.mouseOver = function (text) {
    d3.select('.tooltip')
      .transition()
      .duration(200)
      .style('opacity', 0.9)
    d3.select('.tooltip')
      .html(text)
      .style('left', d3.event.pageX + 'px')
      .style('top', d3.event.pageY - 28 + 'px')
  }
  interaction.mouseOut = function () {
    d3.select('.tooltip')
      .transition()
      .duration(500)
      .style('opacity', 0)
  }
  return interaction
}

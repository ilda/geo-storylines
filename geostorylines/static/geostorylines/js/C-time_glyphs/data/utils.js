export function sortScenes(scenes, orderType) {
  return scenes.sort(function (a, b) {
    if (orderType == 'time' && a.keyNode.type != 'time') {
      if ((a.keyFirstAppearance).localeCompare(b.keyFirstAppearance) == 0) {
        if ((a.key).localeCompare(b.key) == 0) {
          return (a.date).localeCompare(b.date)
        } else {
          return (a.key).localeCompare(b.key)
        }

      } else {
        return (a.keyFirstAppearance).localeCompare(b.keyFirstAppearance)
      }
    } else {
      if ((a.key).localeCompare(b.key) == 0) {
        return (a.date).localeCompare(b.date)
      } else {
        return (a.key).localeCompare(b.key)
      }
    }
  })
}
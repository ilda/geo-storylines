import { sortScenes } from './utils.js'

export function createScenes(data, selections, sceneEdges, sceneNodes, validEdges, orderType) {
  const _nodes = data.nodes()
  const _edges = data.edges()
  const xDim = selections.dim.x
  const yDim = selections.dim.y
  const zDim = selections.dim.z

  var relatedNodesY = []
  for (let index in sceneNodes) {
    var aNodeId = sceneNodes[index]
    var aNode = _nodes[aNodeId]
    var aNodeRelated = aNode.related_nodes[yDim]
    relatedNodesY = relatedNodesY.concat(aNodeRelated)
  }
  var scenes = []
  for (let index in sceneEdges) {
    const anEdgeId = sceneEdges[index]
    const anEdge = _edges[anEdgeId]
    var chtrs = []
    const yAxisIds = anEdge.node_types_ids[yDim]
    for (const idIndex in yAxisIds) {
      var yAxisId = yAxisIds[idIndex]
      if (relatedNodesY.indexOf(yAxisId) > -1) {
        const aNode = _nodes[yAxisId]
        chtrs.push(aNode)
      }
    }
    chtrs = chtrs.sort(function (a, b) {
      return ('' + a.id).localeCompare(b.id, undefined, { 'numeric': true })
    })
    var xAxisIds = anEdge.node_types_ids[xDim]
    for (let idIndex in xAxisIds) {
      var xAxisId = xAxisIds[idIndex]
      var aNode = _nodes[xAxisId]
      if (aNode == undefined) {
        continue
      }
      var newItem = getScene(data, chtrs, aNode, anEdge, zDim, false, validEdges)
      newItem.edge = anEdge
      scenes.push(newItem)
    }
  }
  return sortScenes(scenes, orderType)
}

export function createScenesNested(scene, nestedHypergraph, selections, orderType) {
  console.log('nested Hypergraph')
  console.log(nestedHypergraph)
  const xDim = selections.dim.x
  const yDim = selections.dim.y
  const zDim = selections.dim.z
  const xAggr = selections.aggr.x
  const zAggr = selections.aggr.z
  //const nested = data.getNestedHypergraph(scene.edgeId, yDim, zAggr)
  const extraNodeId = 'No ' + zDim
  var aNodeId = Object.keys(nestedHypergraph.nodes)[0]
  var extraNode = _.cloneDeep(nestedHypergraph.nodes[aNodeId])
  extraNode.isEmptyEntityType = true
  extraNode.name = extraNodeId
  extraNode.id = extraNodeId
  extraNode.type = zDim
  extraNode.related_nodes = []
  extraNode.related_edges = []
  nestedHypergraph.nodes[extraNodeId] = extraNode

  const extraNodeIdX = 'No ' + xDim
  var aNodeId = Object.keys(nestedHypergraph.nodes)[0]
  var extraNodeX = _.cloneDeep(nestedHypergraph.nodes[aNodeId])
  extraNodeX.isEmptyEntityType = true
  extraNodeX.name = extraNodeIdX
  extraNodeX.id = extraNodeIdX
  extraNodeX.type = xDim
  extraNodeX.related_nodes = []
  extraNodeX.related_edges = []
  nestedHypergraph.nodes[extraNodeIdX] = extraNodeX

  var relatedNodesY = []
  for (var nodeId in nestedHypergraph.nodes) {
    var node = nestedHypergraph.nodes[nodeId]
    var related = node.related_nodes[yDim]
    relatedNodesY = relatedNodesY.concat(related)
  }

  var scenes = []
  for (var edgeId in nestedHypergraph.edges) {
    var edge = nestedHypergraph.edges[edgeId]
    // Ignore any edge that doesn't have entities of the Y axis
    if (edge.node_types_ids[yDim].length == 0) continue 
    var chtrs = []
    const yAxisIds = edge.node_types_ids[yDim]
    for (const idIndex in yAxisIds) {
      var yAxisId = yAxisIds[idIndex]
      if (relatedNodesY.indexOf(yAxisId) > -1) {
        const node = nestedHypergraph.nodes[yAxisId]
        chtrs.push(node)
      }
    }
    chtrs = chtrs.sort(function (a, b) {
      return ('' + a.id).localeCompare(b.id, undefined, { 'numeric': true })
    })
    // I always have to have X, but not necesarily Z
    var xAxisIds = edge.node_types_ids[xDim]
    for (let idXIndex in xAxisIds) {
      var xAxisId = xAxisIds[idXIndex]
      var nodeX = nestedHypergraph.nodes[xAxisId]
      var zAxisIds = edge.node_types_ids[zDim]
      if (zAxisIds.length == 0) {
        zAxisIds = [extraNodeId]
      }
      for (let idIndex in zAxisIds) {
        var zAxisId = zAxisIds[idIndex]
        var nodeZ = nestedHypergraph.nodes[zAxisId]
        // TODO: not sure why I reach this point
        if(nodeZ === undefined) continue
        var newItem = getSceneNested(chtrs, nodeX, nodeZ, edge, xDim, false, nestedHypergraph.nodes)
        scenes.push(newItem)
      }
    }
  }

  var sorted = sortScenes(scenes, orderType)
  sorted = sorted.sort(function (a, b) {
    if (a.keyX == extraNodeId) return -1
    if (b.keyX == extraNodeId) return 1
    return -1
  })
  return sorted
}

export function getScene(data, chtrs, node, edge, zDim, nested, validEdges) {
  var newItem = {}
  newItem.characters = chtrs
  newItem.key = node.name
  // I'm assuming each edge has only one date, this ight not be generalizable
  var date
  if (edge.isFake) {
    date = edge.id
  } else {
    const dateId = edge.node_types_ids['time'][0]
    date = data.getNode(dateId)
    date = date.name
  }
  newItem.date = date
  newItem.keyFirstAppearance = node.first_appearance
  newItem.keyNode = node
  newItem.id = (edge.id + '_' + node.id)
  newItem.edgeId = edge.id
  newItem.isOpen = false
  newItem.allRelatedEntities = edge.nodes_ids
  newItem.isFake = false
  newItem.sources = edge.sources
  chtrs.forEach(n => {
    if (n != undefined) {
      if (n.isFake) {
        newItem.isFake = true
      }
    }
  })
  if (!nested) {
    var nNested = 0
    var edgesToConsider = []
    var zDimToConsider = []
    for (let index in edge.original_edges_ids) {
      let edgeId = edge.original_edges_ids[index]
      if (validEdges.indexOf(edgeId) >= 0)
        edgesToConsider.push(edgeId)
    }
    for (let index in edgesToConsider) {
      var edgeId = edgesToConsider[index]
      let tmpEdge = data.getOriginalEdge(edgeId)
      let zDimEdge = tmpEdge.node_types_ids[zDim]
      if (zDimEdge.length == 0) {
        //zDimToConsider = zDimToConsider.concat(['no-zDim'])
      } else {
        zDimToConsider = zDimToConsider.concat(zDimEdge)
      }
    }
    zDimToConsider = Array.from(new Set(zDimToConsider))
    //newItem.nNested = edge.node_types_ids[zDim].length
    newItem.nNested = zDimToConsider.length
  }
  return newItem
}

// TODO: CLEAN!
export function getSceneNested(chtrs, keyNodeXDim, keyNodeZDim, edge, zDim, nested, nodes) {
  var newItem = {}
  newItem.characters = chtrs
  newItem.key = keyNodeZDim.name
  newItem.keyX = keyNodeXDim.name
  newItem.keyNodeXDim = keyNodeXDim
  newItem.keyNodeZDim = keyNodeZDim
  // I'm assuming each edge has only one date, this might not be generalizable
  var date
  if (edge.isFake) {
    date = edge.id
  } else {
    const dateId = edge.node_types_ids['time'][0]
    date = nodes[dateId]
    date = date.name
  }
  newItem.date = date
  if (keyNodeXDim.first_appearance > keyNodeZDim.first_appearance) {
    newItem.first_appearance = keyNodeZDim.first_appearance
  } else {
    newItem.first_appearance = keyNodeXDim.first_appearance
  }
  newItem.keyFirstAppearance = keyNodeXDim.first_appearance
  //newItem.keyFirstAppearance = node.first_appearance
  newItem.keyNode = keyNodeZDim
  newItem.id = (edge.id + '_' + keyNodeXDim.id + '_' + keyNodeZDim.id)
  newItem.edgeId = edge.id
  newItem.isOpen = false
  //newItem.firstAppearance = node.first_appearance
  newItem.allRelatedEntities = edge.nodes_ids
  newItem.isFake = false
  newItem.sources = edge.sources
  chtrs.forEach(n => {
    if (n != undefined) {
      if (n.isFake) {
        newItem.isFake = true
      }
    }
  })
  if (!nested) {
    newItem.nNested = edge.node_types_ids[zDim].length
  }
  return newItem
}
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView
from . import views

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'), name='home'),
    path('gsl-base', TemplateView.as_view(template_name='geostorylines/gsl-base.html'), name='home'),
    path('map-glyphs', TemplateView.as_view(template_name='geostorylines/A-map_glyphs.html'), name='map-glyphs'),
    path('coordinated-views', TemplateView.as_view(template_name='geostorylines/B-coordinated_views.html'), name='coordinated-views'),
    path('time-glyphs', TemplateView.as_view(template_name='geostorylines/C-time_glyphs.html'), name='time-glyphs'),
    path('search/', views.search, name='search'),
    path('datasets-available/', views.get_available_datasets, name='available_datasets'),
    path('get_data/', views.get_data, name='get_data'),
    path('get_complete_dataset/', views.get_complete_dataset, name='get_complete_dataset'),
    path('get_source/', views.get_source, name='get_source'),
    path('get_scenes/', views.get_scenes, name='get_scenes'),
    path('get_nested_scenes/', views.get_nested_scenes, name='get_nested_scenes'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
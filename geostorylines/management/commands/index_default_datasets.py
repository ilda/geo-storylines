from django.core.management.base import BaseCommand
from geostorylines.index import Index
from django.templatetags.static import static
import json

def index_news_articles():
    index = Index('articles')
    index.delete()
    index.create()

    path_base = './geostorylines/static/datasets/'
    path = path_base + 'storylines/articles.json'
    with open(path, 'r') as f:
        data = json.load(f)
    for e in data['edges']:
        sources = e['sources']
        for s in sources:
            with open(path_base+'sources/articles/'+s) as sfile:
                text = json.load(sfile)
            index.insert(s, text)

def index_dblp():
    index = Index('dblp_eurovis')
    index.delete()
    index.create()

    path_base = './geostorylines/static/datasets/'
    path = path_base + 'storylines/dblp_eurovis.json'
    with open(path, 'r') as f:
        data = json.load(f)
    for e in data['edges']:
        sources = e['sources']
        for s in sources:
            with open(path_base+'sources/dblp_eurovis/'+s) as sfile:
                text = json.load(sfile)
            index.insert(s, text['title'])

def index_inriahal():
    index = Index('hci-coauthorship_all')
    index.delete()
    index.create()

    path_base = './geostorylines/static/datasets/'
    path = path_base + 'storylines/hci-coauthorship_all.json'
    with open(path, 'r') as f:
        data = json.load(f)
    for e in data['edges']:
        sources = e['sources']
        for s in sources:
            with open(path_base+'sources/hci-coauthorship_all/'+s) as sfile:
                text = json.load(sfile)
            index.insert(s, text['title_s'])

from django.templatetags.static import static
class Command(BaseCommand):
    def handle(self, **options):
        index_news_articles()
        index_dblp()
        index_inriahal()
from django.http import HttpResponse
from geostorylines.libs.index import Index
import json
from django.templatetags.static import static
from pathlib import Path
import geostorylines.libs.hypergraph.transformer as hypergraph_transformer
from geostorylines.libs.scenes.generator import ScenesGenerator
from geostorylines.libs.scenes.generator_nested import NestedScenesGenerator
import json


def search(request):
    if request.method == 'GET':
        query = request.GET.get('q', None)
        index_name = request.GET.get('i', None)
        if query is not None and index_name is not None:
            i = Index(index_name)
            sources_ids = i.search(query)
            sources_ids = list(set(sources_ids))
            data = {'sources_ids': sources_ids}
            return HttpResponse(json.dumps(data), content_type='application/json')


def get_available_datasets(request):
    txt_folder = Path(static('geostorylines/datasets/storylines/')).rglob('*.json')
    files = [x for x in txt_folder]
    datasets = []
    for f in files:
        datasets.append(f.name)
    datasets.sort()
    return HttpResponse(json.dumps({"datasets": datasets}), content_type='application/json')

def get_complete_dataset(request):
  if request.method == 'GET':
        name = request.GET.get('q', None)
        if name is not None:
            file_data = open(static('geostorylines/datasets/storylines/' + name))
            data = json.load(file_data)
            return HttpResponse(json.dumps(data), content_type='application/json')

def get_data(request):
    if request.method == 'GET':
        name = request.GET.get('q', None)
        if name is not None:
            request.session['dataset_name'] = name
            file_data = open(static('geostorylines/datasets/storylines/' + name))
            data = json.load(file_data)
            return HttpResponse(json.dumps(data['dimensions']), content_type='application/json')


def get_scenes(request):
    if request.method == 'GET':
        name = request.GET.get('name', None)
        dim_x = request.GET.get('xDim', None)
        dim_y = request.GET.get('yDim', None)
        dim_z = request.GET.get('zDim', None)
        aggr_x = request.GET.get('xAggr', None)
        aggr_z = request.GET.get('zAggr', None)
        order_x = request.GET.get('xDimOrder', None)
        if name is not None:
            file_data = open(static('geostorylines/datasets/storylines/' + name))
            data = json.load(file_data)
            scenes, nodes_dict = ScenesGenerator().get_scenes(
                data, dim_x, dim_y, dim_z, aggr_x, aggr_z, order_x)
            if 'mapName' in data:
                map_name = data['mapName']
                file_data = open(static('geostorylines/datasets/locations/' + map_name))
                map_geojson = json.load(file_data)
            else:
                map_geojson = {}            
            return HttpResponse(json.dumps({'scenes': scenes, 'nodes_dict': nodes_dict, 'map': map_geojson}), content_type='application/json')


def get_nested_scenes(request):
    if request.method == 'GET':
        name = request.GET.get('name', None)
        dim_x = request.GET.get('xDim', None)
        dim_y = request.GET.get('yDim', None)
        dim_z = request.GET.get('zDim', None)
        xAggr = request.GET.get('xAggr', None)
        zAggr = request.GET.get('zAggr', None)
        order_z = request.GET.get('zDimOrder', None)
        scene_edges = request.GET.get('scene_edges', None)
        if name is not None:
            file_data = open(static('geostorylines/datasets/storylines/' + name))
            data = json.load(file_data)
            scene_edges = scene_edges.split(' ')
            scenes, nodes_dict = NestedScenesGenerator().get_scenes(
                data, scene_edges, dim_x, dim_y, dim_z, zAggr, order_z)
            return HttpResponse(json.dumps({'scenes': scenes, 'nodes_dict': nodes_dict}), content_type='application/json')


def get_source(request):
    if request.method == 'GET':
        name = request.GET.get('q', None)
        index_name = request.GET.get('i', None)
        if name is not None:
            index_name = index_name[:-5]
            file_data = open(
                static('geostorylines/datasets/sources/' + index_name + '/' + name))
            data = json.load(file_data)
            return HttpResponse(json.dumps(data), content_type='application/json')

# This class receives a hypergraph in the form of a dictionary,
# not an object of type Hypergraph.
# Reason: don't want to loose time in an innecesary transformation

import copy

class HypergraphAggregator:

  def __init__(self, hyp, levels, entity_type):
    self.hypergraph = hyp
    self.levels = levels
    self.entity_type = entity_type

  def ignored_levels(self):
    return ['aggr_article']

  def aggregate_element(self, elto, aggr_level):
    return elto['name']

  def apply(self, aggr_level):
    edges = {}
    nodes = {}
    to_ignore = self.ignored_levels()
    # If no need to aggregate, just add the additional values
    if aggr_level in to_ignore:
      for node in self.hypergraph['nodes']:        
        node_id = node['id']
        node['original_edges_ids'] = node['related_edges']
        # TODO: not sure why I need to do this
        node['related_edges'] = []
        node['aggregated_nodes_ids'] = []
      for edge in self.hypergraph['edges']:
        edge_id = edge['id']
        edge['original_edges_ids'] = [edge_id]
      return self.hypergraph
    else:
      nodesBuiltRawMap = {}
      nodesRawBuiltMap = {}
      nodesRawBuiltMapAll = {}
      for node in self.hypergraph['nodes']:
        node_id = node['id']
        # We have to merge because it's the correct type of entity
        if node['type'] == self.entity_type:
          new_value = self.aggregate_element(node, aggr_level)
          # First time I find this element
          if new_value not in nodesBuiltRawMap:
            nodesBuiltRawMap[new_value] = []
            nodes[new_value] = node
            nodes[new_value]['name'] = new_value
            nodes[new_value]['id'] = new_value
            nodes[new_value]['original_nodes'] = []
            nodes[new_value]['original_edges_ids'] = []
            nodes[new_value]['aggregated_nodes_ids'] = []
          # Merge information
          nodesBuiltRawMap[new_value].append(node_id)
          nodesRawBuiltMap[node_id] = new_value
          nodesRawBuiltMapAll[node_id] = new_value
          nodes[new_value]['original_nodes'].append(node_id)
          nodes[new_value]['original_edges_ids'] = nodes[new_value]['original_edges_ids'] + node['related_edges']
          nodes[new_value]['related_edges'] = []
          nodes[new_value]['aggregated_nodes_ids'].append(node_id)
          for key in nodes[new_value]['related_nodes']:
            nodes[new_value]['related_nodes'][key] = nodes[new_value]['related_nodes'][key] + node['related_nodes'][key]
            nodes[new_value]['related_nodes'][key] = list(set(nodes[new_value]['related_nodes'][key]))
        # No need to merge, just copy
        else:
          nodes[node_id] = node
          nodes[node_id]['original_edges_ids'] = node['related_edges']
          nodes[node_id]['original_nodes'] = [node_id]
          nodes[node_id]['related_edges'] = []
          nodes[node_id]['aggregated_nodes_ids'] = []
          nodesRawBuiltMapAll[node_id] = node_id
      for edge in self.hypergraph['edges']:
        edge_id = edge['id']
        key = None
        for node_id in edge['nodes_ids']:
          if node_id in nodesRawBuiltMap:
            key = nodesRawBuiltMap[node_id]
        if key == None:
          print('I REACH IMPOSSIBLE POINT!!')
          continue # I should never reach this point bacause each edge has ONE date!
        if key not in edges:
          edges[key] = copy.deepcopy(edge)
          edges[key]['id'] = key
          edges[key]['node_types_ids'][self.entity_type] = [key]
          edges[key]['nodes_ids'].append(key)
          edges[key]['original_edges_ids'] = [edge_id]
          edges[key]['original_edges'] = [edge]
        else:
          edges[key]['original_edges_ids'].append(edge_id)
          edges[key]['original_edges'].append(edge) 
          edges[key]['nodes_ids'] = edges[key]['nodes_ids'] + edge['nodes_ids']
          edges[key]['nodes_ids'] = list(set(edges[key]['nodes_ids']))
          edges[key]['sources'] = edges[key]['sources'] + edge['sources']          
          for key2 in edges[key]['node_types_ids']:
            edges[key]['node_types_ids'][key2] = edges[key]['node_types_ids'][key2] + edge['node_types_ids'][key2]
            edges[key]['node_types_ids'][key2] = list(set(edges[key]['node_types_ids'][key2]))
    for built_edge_id, built_edge in edges.items():
      for n_id in built_edge['nodes_ids']:
        if n_id in nodes:
          nodes[n_id]['related_edges'].append(built_edge_id)
          for entity_type in built_edge['node_types_ids']:
            nodes[n_id]['related_nodes'][entity_type] = nodes[n_id]['related_nodes'][entity_type] + built_edge['node_types_ids'][entity_type]
            
    self.hypergraph['nodes'] = list(nodes.values())
    self.hypergraph['edges'] = list(edges.values())
    self.compute_relationship_matrix()
    return self.hypergraph
  
  # I'll be modifying the same data all the time
  def compute_relationship_matrix(self):
    self.hypergraph['relations_matrix'] = {}
    for dim1 in self.hypergraph['dimensions']:
      self.hypergraph['relations_matrix'][dim1] = {}
      for dim2 in self.hypergraph['dimensions']:
        self.hypergraph['relations_matrix'][dim1][dim2] = []
    for edge in self.hypergraph['edges']:
      edge_id = edge['id']
      for dim1 in edge['node_types_ids']:
        for dim2 in edge['node_types_ids']:
          if len(edge['node_types_ids'][dim1]) > 0 and len(edge['node_types_ids'][dim2]) > 0:
            self.hypergraph['relations_matrix'][dim1][dim2].append(edge_id)
            self.hypergraph['relations_matrix'][dim2][dim1].append(edge_id)
    for dim1 in self.hypergraph['dimensions']:
      for dim2 in self.hypergraph['dimensions']:
        self.hypergraph['relations_matrix'][dim1][dim2] = list(set(self.hypergraph['relations_matrix'][dim1][dim2]))


  # TODO: I think I won't use this
  def is_child (self, parent, node, aggr_level):
    transformed = self.aggregateElement(node['name'], aggr_level)
    return transformed == parent.name

  def getChildNodes(self, parent, nodes, aggr_level):
    results = []
    for n in nodes:
      if self.is_child(parent, n, aggr_level):
        results.append(n)
    return results
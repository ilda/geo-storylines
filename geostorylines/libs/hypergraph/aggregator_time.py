from .aggregator import HypergraphAggregator


class HypergraphTimeAggregator(HypergraphAggregator):

    def __init__(self, hyp):
        levels = {'aggr_article': 'Article',
                  'aggr_day': 'Day',
                  'aggr_month': 'Month',
                  'aggr_year': 'Year',
                  }
        entity_type = 'time'
        super().__init__(hyp, levels, entity_type)

    def ignored_levels(self):
        return ['aggr_article']

    def aggregate_element(self, elto, aggr_level):
        if aggr_level == 'aggr_day' or aggr_level == 'aggr_article':
            return elto['name']
        elif aggr_level == 'aggr_month':
            return elto['name'][0:7]
        elif aggr_level == 'aggr_year':
            return elto['name'][0:4]

from .aggregator import HypergraphAggregator


class HypergraphDefaultAggregator(HypergraphAggregator):

    def __init__(self, hyp, entity_type):
        levels = {'aggr_article': 'Article'}
        super().__init__(hyp, levels, entity_type)

    def ignored_levels(self):
        return ['aggr_article']

    def aggregate_element(self, elto, aggr_level):
        if aggr_level == 'aggr_day' or aggr_level == 'aggr_article':
            return elto['name']
        elif aggr_level == 'aggr_month':
            return elto['name'][0:7]
        elif aggr_level == 'aggr_year':
            return elto['name'][0:4]

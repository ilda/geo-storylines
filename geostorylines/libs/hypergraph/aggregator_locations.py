from .aggregator import HypergraphAggregator


class HypergraphLocationsAggregator(HypergraphAggregator):

    def __init__(self, hyp):
        levels = {'aggr_article': 'Article',
                  'aggr_commune': 'Commune',
                  'aggr_department': 'Department',
                  'aggr_region': 'Region',
                  'aggr_country': 'Country'}
        entity_type = 'locations'
        super().__init__(hyp, levels, entity_type)

    def ignored_levels(self):
        return ['aggr_article', 'aggr_commune']

    def aggregate_element(self, elto, aggr_level):
        name = elto['name']
        if aggr_level == 'aggr_commune' or aggr_level == 'aggr_article':
            return name
        else:
            locations = self.get_locations_dict()
            ag = aggr_level[5:]
            return locations[name][ag]

    def get_locations_dict(self):
        r = {}
        r['Paris'] = {'department': 'Paris',
                      'region': 'Île-de-France', 'country': 'France'}
        r['Nanterre'] = {'department': 'Hauts-de-Seine',
                         'region': 'Île-de-France', 'country': 'France'}
        r['Clichy'] = {'department': 'Hauts-de-Seine',
                       'region': 'Île-de-France', 'country': 'France'}
        r['Bagneux'] = {'department': 'Hauts-de-Seine',
                        'region': 'Île-de-France', 'country': 'France'}
        r['Antony'] = {'department': 'Hauts-de-Seine',
                       'region': 'Île-de-France', 'country': 'France'}
        r['Courbevoie'] = {'department': 'Hauts-de-Seine',
                           'region': 'Île-de-France', 'country': 'France'}
        r['Saint-Denis'] = {'department': 'Seine-Saint-Denis',
                            'region': 'Île-de-France', 'country': 'France'}
        r['Bobigny'] = {'department': 'Seine-Saint-Denis',
                        'region': 'Île-de-France', 'country': 'France'}
        r['Romainville'] = {'department': 'Seine-Saint-Denis',
                            'region': 'Île-de-France', 'country': 'France'}
        r['Gagny'] = {'department': 'Seine-Saint-Denis',
                      'region': 'Île-de-France', 'country': 'France'}
        r['Corbeil-Essonnes'] = {'department': 'Essonne',
                                 'region': 'Île-de-France', 'country': 'France'}
        r['Massy'] = {'department': 'Essonne',
                      'region': 'Île-de-France', 'country': 'France'}
        r['Savigny-sur-Orge'] = {'department': 'Essonne',
                                 'region': 'Île-de-France', 'country': 'France'}
        r['Palaiseau'] = {'department': 'Essonne',
                          'region': 'Île-de-France', 'country': 'France'}
        r['Athis-Mons'] = {'department': 'Essonne',
                           'region': 'Île-de-France', 'country': 'France'}
        # OCCITANIE
        r['Montpellier'] = {'department': 'Hérault',
                            'region': 'Occitanie', 'country': 'France'}
        r['Béziers'] = {'department': 'Hérault',
                        'region': 'Occitanie', 'country': 'France'}
        r['Sète'] = {'department': 'Hérault',
                     'region': 'Occitanie', 'country': 'France'}
        # GRAND EST
        r['Strasbourg'] = {'department': 'Bas-Rhin',
                           'region': 'Grand Est', 'country': 'France'}
        r['Haguenau'] = {'department': 'Bas-Rhin',
                         'region': 'Grand Est', 'country': 'France'}
        r['Schiltigheim'] = {'department': 'Bas-Rhin',
                             'region': 'Grand Est', 'country': 'France'}
        # Normandie
        r['Caen'] = {'department': 'Calvados',
                     'region': 'Normandie', 'country': 'France'}
        r['Le Havre'] = {'department': 'Seine-Maritime',
                         'region': 'Normandie', 'country': 'France'}
        r['Rouen'] = {'department': 'Seine-Maritime',
                      'region': 'Normandie', 'country': 'France'}
        r['Évreux'] = {'department': 'Eure',
                       'region': 'Normandie', 'country': 'France'}
        # Bretagne
        r['Brest'] = {'department': 'Finistère',
                      'region': 'Bretagne', 'country': 'France'}
        r['Quimper'] = {'department': 'Finistère',
                        'region': 'Bretagne', 'country': 'France'}
        r['Lorient'] = {'department': 'Morbihan',
                        'region': 'Bretagne', 'country': 'France'}
        r['Vannes'] = {'department': 'Morbihan',
                       'region': 'Bretagne', 'country': 'France'}
        r['Saint-Malo'] = {'department': 'Ille-et-Vilaine',
                           'region': 'Bretagne', 'country': 'France'}
        r['Rennes'] = {'department': 'Ille-et-Vilaine',
                       'region': 'Bretagne', 'country': 'France'}
        # Pays de la Loire
        r['Saint-Herblain'] = {'department': 'Loire-Atlantique',
                               'region': 'Pays de la Loire', 'country': 'France'}
        r['Rezé'] = {'department': 'Loire-Atlantique',
                     'region': 'Pays de la Loire', 'country': 'France'}
        r['Nantes'] = {'department': 'Loire-Atlantique',
                       'region': 'Pays de la Loire', 'country': 'France'}
        r['Saint-Nazaire'] = {'department': 'Loire-Atlantique',
                              'region': 'Pays de la Loire', 'country': 'France'}
        r['La Roche-sur-Yon'] = {'department': 'Vendée',
                                 'region': 'Pays de la Loire', 'country': 'France'}
        r['Angers'] = {'department': 'Maine-et-Loire',
                       'region': 'Pays de la Loire', 'country': 'France'}
        r['Cholet'] = {'department': 'Maine-et-Loire',
                       'region': 'Pays de la Loire', 'country': 'France'}
        # Hauts-de-France
        r['Saint-Quentin'] = {'department': 'Aisne',
                              'region': 'Hauts-de-France', 'country': 'France'}
        r['Roubaix'] = {'department': 'Nord',
                        'region': 'Hauts-de-France', 'country': 'France'}
        r['Tourcoing'] = {'department': 'Nord',
                          'region': 'Hauts-de-France', 'country': 'France'}
        r['Dunkerque'] = {'department': 'Nord',
                          'region': 'Hauts-de-France', 'country': 'France'}
        r['Villeneuve-d\'Ascq'] = {'department': 'Nord',
                                   'region': 'Hauts-de-France', 'country': 'France'}
        r['Calais'] = {'department': 'Pas-de-Calais',
                       'region': 'Hauts-de-France', 'country': 'France'}
        return r

from .aggregator_time import HypergraphTimeAggregator
from .aggregator_locations import HypergraphLocationsAggregator
from .aggregator_default import HypergraphDefaultAggregator


def get_aggregation(hypergraph, entity_type, aggr_level):
    if entity_type == 'time':
        return HypergraphTimeAggregator(hypergraph).apply(aggr_level)
    elif entity_type == 'locations':
        return HypergraphLocationsAggregator(hypergraph).apply(aggr_level)
    else:
        return HypergraphDefaultAggregator(hypergraph, entity_type).apply(aggr_level)


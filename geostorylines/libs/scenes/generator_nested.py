
import copy
import locale
import functools
import geostorylines.libs.hypergraph.transformer as hypergraph_transformer

class NestedScenesGenerator:

    def get_scenes(self, data, scene_edges, dim_x, dim_y, dim_z, aggr_z, order_z):
        self.compute_hypergraph(data, scene_edges, dim_y, dim_z, aggr_z)
        return self.compute_scenes(dim_x, dim_y, dim_z, order_z)

    def compute_hypergraph(self, data, scene_edges, dim_y, dim_z, aggr_z):
        edges_ori_dict = {}
        scene_nodes = []
        hyp_nodes = []
        hyp_edges = []
        for edge in data['edges']:
            if edge['id'] in scene_edges and len(edge['node_types_ids'][dim_y]) > 0:
                edges_ori_dict[edge['id']] = edge
                scene_nodes = scene_nodes + edge['nodes_ids']
                hyp_edges.append(edge)

        scene_nodes = list(set(scene_nodes))
        nodes_ori_dict = {}
        for node in data['nodes']:
            if node['id'] in scene_nodes:
                nodes_ori_dict[node['id']] = node
                hyp_nodes.append(node)

        data['edges'] = hyp_edges
        data['nodes'] = hyp_nodes

        ori_hyp = {}
        ori_hyp['edges'] = edges_ori_dict
        ori_hyp['nodes'] = nodes_ori_dict
        ori_hyp['relations_matrix'] = data['relations_matrix']
        self.original_hyp = ori_hyp
        self.aggr_hyp = hypergraph_transformer.get_aggregation(data, dim_z, aggr_z)

    # IT ONLY CREATES THEM, DOES NOT SORT THEM
    def compute_scenes(self, dim_x, dim_y, dim_z, order_z):
        extra_node_z_id = 'No ' + dim_z
        extra_node_z = copy.deepcopy(self.aggr_hyp['nodes'][0])
        extra_node_z['isEmptyEntityType'] = True
        extra_node_z['name'] = extra_node_z_id
        extra_node_z['id'] = extra_node_z_id
        extra_node_z['type'] = dim_z
        for t in extra_node_z['related_nodes']:
            extra_node_z['related_nodes'][t] = []
        extra_node_z['related_edges'] = []
        self.aggr_hyp['nodes'].append(extra_node_z)

        nodes = self.aggr_hyp['nodes']
        edges = self.aggr_hyp['edges']
        # AUXILIARY DICTIONARY
        nodes_dict = {}
        related_nodes_y = []
        for node in nodes:
            related = node['related_nodes'][dim_y]
            related_nodes_y += related
            nodes_dict[node['id']] = node

        # CREATE ONE OR MORE SCENES PER EDGE
        scenes = []
        for edge in edges:
            edge_id = edge['id']
            # IGNORE ANY EDGE WITHOUT ENTITIES IN THE Y DIM
            y_dim_nodes = edge['node_types_ids'][dim_y]
            if len(y_dim_nodes) == 0:
                continue
            chtrs = []
            for node_id in y_dim_nodes:
                if node_id in related_nodes_y:
                    chtrs.append(node_id)
            chtrs = sorted(chtrs)

            # CREATE ONE SCENE PER X AND Z COMBINATION
            x_dim_ids = edge['node_types_ids'][dim_x]
            z_dim_ids = edge['node_types_ids'][dim_z]
            if len(z_dim_ids) == 0:
                z_dim_ids = [extra_node_z_id]
                for t in nodes_dict[extra_node_z_id]['related_nodes']:
                    nodes_dict[extra_node_z_id]['related_nodes'][t] = nodes_dict[extra_node_z_id]['related_nodes'][t] + edge['node_types_ids'][t]
                    nodes_dict[extra_node_z_id]['related_nodes'][t] = list(set(nodes_dict[extra_node_z_id]['related_nodes'][t]))
            for node_x_id in x_dim_ids:
                if node_x_id not in nodes_dict:
                    continue
                node_x = nodes_dict[node_x_id]
                for node_z_id in z_dim_ids:
                    # TODO: NOT SURE HOW I COULD REACH THIS POINT
                    if node_z_id not in nodes_dict:
                        continue
                    node_z = nodes_dict[node_z_id]
                    date = self._get_date_for_scene(edge, nodes_dict)
                    new_scene = self._create_scene(
                        chtrs, node_x, node_z, date, edge)
                    new_scene['edge'] = edge
                    scenes.append(new_scene)
        scenes = self.sort_scenes(scenes, order_z)
        print(nodes_dict[extra_node_z_id])
        return (scenes, nodes_dict)

    def _create_scene(self, chtrs, keynode_dim_x, keynode_dim_z, date, edge):
        new_scene = {}
        new_scene['charactersIds'] = chtrs
        # TODO: WHY DO I NEED THE KEY AND THE NODE? REDUNDANT INFO
        new_scene['keyZ'] = keynode_dim_z['name']
        new_scene['keyNodeZ'] = keynode_dim_z
        new_scene['keyX'] = keynode_dim_x['name']
        new_scene['keyNodeX'] = keynode_dim_x
        new_scene['date'] = date
        new_scene['first_appearance'] = keynode_dim_x['first_appearance']
        new_scene['keyNode'] = keynode_dim_z
        new_scene['id'] = edge['id'] + '_' + \
            keynode_dim_x['id'] + '_' + keynode_dim_z['id']
        new_scene['edgeId'] = edge['id']
        new_scene['isOpen'] = False
        new_scene['allRelatedEntities'] = edge['nodes_ids']
        new_scene['isFake'] = False
        new_scene['sources'] = edge['sources']
        # TODO: ADD INFO OF FAKE CHARACTERS
        # chtrs.forEach(n => {
        # if (n != undefined) {
        # if (n.isFake) {
        # newItem.isFake = true
        # }
        # }
        # })
        return new_scene

    def _get_date_for_scene(self, edge, nodes_dict):
        # Assuming each edge has only one date, this ight not be generalizable
        # TODO: MODIFY WHEN INCLUDING THE FILTERED WORDS
        # if edge['isFake']:
        if False:
            return edge['id']
        else:
            possible_dates = []
            for node_id in edge['node_types_ids']['time']:
                if node_id in nodes_dict:
                    possible_dates.append(nodes_dict[node_id]['name'])
            possible_dates.sort()
            return possible_dates[0]

    # TODO: CHECK IF THE ORDER Z ACTUALLY WORKS
    def sort_scenes(self, scenes, order):
        def compare_time(a, b):
            if 'No ' in a['keyZ']:
                return -1
            if 'No ' in b['keyZ']:
                return 1
            # KEYZ IS THE FIRST ORDER CRITERIA, THEN order AND THEN KEYX
            if a['KeyZ'] == b['KeyZ']:
                if a['date'] == b['date']:
                    return locale.strcoll(a['keyX'], b['keyX'])
                else:
                    return locale.strcoll(a['date'], b['date'])
            else:
                return locale.strcoll(a['KeyZ'], b['KeyZ'])

        def compare(a, b):
            if 'No ' in a['keyZ']:
                return -1
            if 'No ' in b['keyZ']:
                return 1
            if a['keyZ'] == b['keyZ']:
                return locale.strcoll(a['keyX'], b['keyX'])
            else:
                return locale.strcoll(a['keyZ'], b['keyZ'])

        keynode_type = scenes[0]['keyNode']['type']
        if order == 'time' and keynode_type != 'time':
            return sorted(scenes, key=functools.cmp_to_key(compare_time))
        else:
            return sorted(scenes, key=functools.cmp_to_key(compare))

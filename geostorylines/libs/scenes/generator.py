import functools
import locale
import geostorylines.libs.hypergraph.transformer as hypergraph_transformer

class ScenesGenerator:

    def get_scenes(self, data, dim_x, dim_y, dim_z, aggr_x, aggr_z, order_x):
        self.compute_hypergraph(data, dim_x, dim_z, aggr_x, aggr_z)
        return self.compute_scenes(dim_x, dim_y, dim_z, order_x)
    
    def compute_hypergraph(self, data, dim_x, dim_z, aggr_x, aggr_z):
        edges_ori_dict = {}
        for edge in data['edges']:
            edges_ori_dict[edge['id']] = edge

        nodes_ori_dict = {}
        for node in data['nodes']:
            nodes_ori_dict[node['id']] = node

        ori_hyp = {}
        ori_hyp['edges'] = edges_ori_dict
        ori_hyp['nodes'] = nodes_ori_dict
        ori_hyp['relations_matrix'] = data['relations_matrix']
        self.original_hyp = ori_hyp
        self.aggr_hyp = hypergraph_transformer.get_aggregation(data, dim_x, aggr_x)    
        # TODO: ACA TO UPDATE NUMBER OF NESTED ENTITIES WHEN CHANGING THE AGGREGATION IN Z
        self.aggr_hyp = hypergraph_transformer.get_aggregation(self.aggr_hyp, dim_z, aggr_z)

    def compute_scenes(self, dim_x, dim_y, dim_z, order_x):
        valid_edges = self.aggr_hyp['relations_matrix'][dim_x][dim_y]
        ori_valid_edges = self.original_hyp['relations_matrix'][dim_x][dim_y]
        nodes = self.aggr_hyp['nodes']
        edges = self.aggr_hyp['edges']

        # AUXILIARY DICTIONARY
        nodes_dict = {}
        edges_ori_dict = self.original_hyp['edges']
        nodes_ori_dict = self.original_hyp['nodes']

        # CONTAINS IDS ONLY
        related_nodes_y = []
        for node in nodes:
            related = node['related_nodes'][dim_y]
            related_nodes_y += related
            nodes_dict[node['id']] = node

        # CREATE ONE OR MORE SCENES PER EDGE
        scenes = []
        for edge in edges:
            edge_id = edge['id']
            if edge_id not in valid_edges: continue
            chtrs = []
            y_dim_nodes = edge['node_types_ids'][dim_y]
            for node_id in y_dim_nodes:
                if node_id in related_nodes_y:
                    chtrs.append(node_id)

            chtrs = sorted(chtrs)

            x_dim_ids = edge['node_types_ids'][dim_x]
            for node_id in x_dim_ids:
                if node_id not in nodes_dict: continue
                node = nodes_dict[node_id]
                date = self._get_date_for_scene(edge, nodes_dict)
                new_scene = self._create_scene(chtrs, node, date, edge, dim_z, dim_y, ori_valid_edges, edges_ori_dict)
                new_scene['edge'] = edge
                scenes.append(new_scene)

        ## ADD ALL ORIGINAL NODES TO SHOW THEM WITH THE ARTICLES
        #for node_id, node in self.original_hyp['nodes'].items():
        #    if node_id not in nodes_dict:
        #        nodes_dict[node['id']] = node
        for node in nodes:
            if 'aggregated_nodes_ids' in node:
                for n_id in node['aggregated_nodes_ids']:
                    if n_id not in nodes_dict:
                        nodes_dict[n_id] = nodes_ori_dict[n_id]

        scenes = self.sort_scenes(scenes, order_x)
        return (scenes, nodes_dict)

    def _create_scene(self, chtrs, node, date, edge, dim_z, dim_y, valid_edges, edges_dict):
        new_scene = {}
        new_scene['charactersIds'] = chtrs
        new_scene['key'] = node['name']
        new_scene['date'] = date
        new_scene['keyFirstAppearance'] = node['first_appearance']
        new_scene['keyNode'] = node
        new_scene['id'] = (edge['id'] + '_' + node['id'])
        new_scene['edgeId'] = edge['id']
        new_scene['isOpen'] = False
        new_scene['allRelatedEntities'] = edge['nodes_ids']
        new_scene['isFake'] = False
        # TODO: WHY DO I NEED THE SOURCES IN THE SCENES IF I CAN GET THEM FROM THE EDGE?
        new_scene['sources'] = edge['sources']

        # TODO: WHEN ADDING TERM ENTITIES
        # chtrs.forEach(n => {
        #     if (n != undefined) {
        #     if (n.isFake) {
        #         new_scene.isFake = true
        #     }
        #     }
        # })
        # IM ASSUMING THIS IS ALWAYS A NON NESTED SCENES
        edges_to_consider = []
        z_dim_to_consider = []
        for edge_id in edge['original_edges_ids']:
            #if edge['id'] == '2013-05':
            #    print(edge_id)
            if edge_id in valid_edges:
                edges_to_consider.append(edge_id)
        for edge_id in edges_to_consider:
            edge_to_consider = edges_dict[edge_id]
            if len(edge_to_consider['node_types_ids'][dim_y]) == 0: continue
            if len(edge_to_consider['node_types_ids'][dim_z]) == 0:
                #z_dim_to_consider.append('no-zDim')
                pass
            else:
                z_dim_to_consider = z_dim_to_consider + edge_to_consider['node_types_ids'][dim_z]
        z_dim_to_consider = list(set(z_dim_to_consider))
        #if edge['id'] == '2013-05':
        #    for node_id in z_dim_to_consider:
        #        print(self.original_hyp['nodes'][node_id]['name'])
        #if edge['id'] == '2013-05':
        #    print(z_dim_to_consider)
        new_scene['nNested'] = len(z_dim_to_consider)
        return new_scene

    def _get_date_for_scene(self, edge, nodes_dict):
        # Assuming each edge has only one date, this ight not be generalizable
        # TODO: MODIFY WHEN INCLUDING THE FILTERED WORDS
        #if edge['isFake']:
        if False:
            return edge['id']
        else:
            possible_dates = []
            for node_id in edge['node_types_ids']['time']:
                if node_id in nodes_dict:
                    possible_dates.append(nodes_dict[node_id]['name'])
            possible_dates.sort()
            return possible_dates[0]

    def sort_scenes(self, scenes, order):
        def compare_time(a, b):
            if a['keyFirstAppearance'] == b['keyFirstAppearance']:
                if a['key'] == b['key']:
                    return locale.strcoll(a['date'], b['date'])
                else:
                    return locale.strcoll(a['key'], b['key'])
            else:
                return locale.strcoll(a['keyFirstAppearance'], b['keyFirstAppearance'])
    
        def compare(a, b):
            if a['key'] == b['key']:
                return locale.strcoll(a['date'], b['date'])
            else:
                return locale.strcoll(a['key'], b['key'])

        keynode_type = scenes[0]['keyNode']['type']
        if order == 'time' and keynode_type != 'time':
            return sorted(scenes, key=functools.cmp_to_key(compare_time))
        else:
            return sorted(scenes, key=functools.cmp_to_key(compare))
        
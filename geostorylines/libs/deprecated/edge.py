################################################################
# Hyperedge class
# 2021 Vanessa Pena-Araya
################################################################


class Edge:        
    def __init__(self, _id, node_types):
        self.id = _id
        self.node_types = node_types
        self.sources = []
        self.init_links()
    
    def init_links(self):
        self.nodes_ids = []
        self.node_types_ids = {}
        for et in self.node_types:
            self.node_types_ids[et] = []
    
    def set_links(self, nodes):
        self.init_links()
        for n in nodes:
            self.nodes_ids.append(n.id)
            self.node_types_ids[n.type].append(n.id)
            
    def get_n_nodes(self, type_exceptions = []):
        nnodes = 0
        for ntype, nids in self.node_types_ids.items():
            if ntype in type_exceptions:
                continue
            else:
                nnodes += len(nids)
        return nnodes

    def add_node(self, node):
        self.nodes_ids.append(node.id)
        self.node_types_ids[node.type].append(node.id)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, Edge):
            return self.id == other.id
        return False
        
        

        
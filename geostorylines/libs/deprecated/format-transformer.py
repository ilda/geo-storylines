
class FormatTransformer:

  ##############################################################################
  ## PAOHVIS
  ##############################################################################
  @staticmethod
  def hypergraph_to_paohvis(hypergraph, file_name, meta_tstart, meta_tend, aggregatingType):
		# METADATA
		jsondata = {}
		jsondata["metadata"] = {
			"name": "Person%20OR%20Location",
			"ts_start": meta_tstart,
			"ts_end": meta_tend,
			"ts_step": "0001/00/00",
			"hyperedge_meta": "publication",
			"node_meta": "author",
			"group_meta": "team",
			"ts_meta": aggregatingType
		}
		
		# NODES
		export_nodes = []
		communities = copy.deepcopy(hypergraph.node_types)
		communities.remove("time")
		for n in hypergraph.nodes:
			if n.type == "time":
				continue
			new_node = {}
			new_node["id"] = n.id
			new_node["name"] = n.name
			new_node["community"] = n.type
			new_node["data"] = []
			n_values = 0
			for n_edge_id in n.related_edges:
				n_edge = hypergraph.get_edge_id(n_edge_id)
				n_communities = 0
				for c in communities:
					n_communities += len(n_edge.node_types_ids[c])
				if n_communities <2:
					continue
				new_data = {}
				# ASSUMING EACH EDGE HAS ONLY ONE TIME
				metatime = n_edge.node_types_ids["time"][0]
				metatime = (hypergraph.get_node_id(metatime)).name
				new_data["ts"] = metatime
				new_data["value"] = [n_values]
				new_data["community"] = [n.type]
				new_node["data"].append(new_data)
				n_values += 1
			export_nodes.append(new_node)
		jsondata["nodes"] = export_nodes
		
		# EDGES
		export_edges = {}
		for e in hypergraph.edges:
			# ASSUMING EACH EDGE HAS ONLY ONE TIME
			edge_time = e.node_types_ids["time"][0]
			edge_time = hypergraph.get_node_id(edge_time)
			edge_time = edge_time.name

			n_communities = 0
			for c in communities:
				n_communities += len(e.node_types_ids[c])
			if n_communities < 2:
				continue

			if edge_time not in export_edges:
				export_edges[edge_time] = {}
				export_edges[edge_time]["ts"] = edge_time
				export_edges[edge_time]["list"] = []
			new_edge = {}
			new_edge["id"] = int(e.id)
			new_edge["w"] = 1
			new_edge["meta"] = {"name": "A Name"}
			new_edge["ids"] = []
			for c in communities:
				for node_id in e.node_types_ids[c]:
					new_edge["ids"].append(node_id)
			export_edges[edge_time]["list"].append(new_edge)
		jsondata["edges"] = list(export_edges.values())
		
		data_json = jsonpickle.encode(jsondata)
		with open(file_name, 'w') as outfile:
			outfile.write(data_json)


  @staticmethod
	def data_from_paohvis(filename):
		with open(filename) as f:
			data = json.load(f)
		nodes_dict = {}
		entity_types = ['time']
		for n in data['nodes']:
			n['community'] = n['community']
			nodes_dict[n['id']] = n
			if n['community'] not in entity_types:
				entity_types.append(n['community'])
		dims = {}
		for c in entity_types:
			if c == 'time':
				dims[c] = {}
				dims[c]['available'] = ['x', 'z']
				dims[c]['default'] = 'x'
			else:
				dims[c] = {}
				dims[c]['available'] = ['x', 'y', 'z']
				if c == 'people':
					dims[c]['default'] = 'y'
				else:
					dims[c]['default'] = 'z'

		entity_types = dims

		sources = []
		times_list = []
		for ts_edge in data['edges']:
			times_list.append(ts_edge['ts'])
		times_dict = Graph.time_compatibility(times_list)
		for ts_edge in data['edges']:
			for i, edge in enumerate(ts_edge['list']):
				pub = {}
				pub['publication_id'] = str(ts_edge) + "_" + str(i)
				for t in entity_types:
					pub[t] = []
				time = times_dict[ts_edge['ts']]
				pub['time'].append(time)
				for n in edge['ids']:
					if n not in nodes_dict:
						continue
					node = nodes_dict[n]
					pub[node['community']].append(node['name'])
				sources.append(pub)
		return (sources, entity_types)

	def time_compatibility(times_list):
		times_dict = {}
		times_list.sort()
		for index, time in enumerate(times_list):
			semester_raw = time[-2]
			print(semester_raw)
			semester = '-01'
			if semester_raw == 'I':
				semester = '-06'
			times_dict[time] = time[0:4] + semester + '-01'
		return times_dict
################################################################################
# Hypergraph class
# 2021 Vanessa Pena-Araya
################################################################################

from geostorylines.libs.node import Node
from geostorylines.libs.edge import Edge
from geostorylines.libs.index import Index
import json
import jsonpickle
import copy
from datetime import datetime
from datetime import timedelta
import copy
from random import shuffle

class Hypergraph:

	##############################################################################
	## GRAPH BUILDING & UPDATING ##
	##############################################################################
	def __init__(self, dims, name):
		self.nodes = []
		self.nodes_ids = []
		self.edges = []
		self.dims = dims
		self.node_types = list(dims.keys())
		self.relations_matrix = {}
		self.name = name
				
	def build_relation_matrix(self):
		self.relations_matrix = {}
		for t1 in self.node_types:
			self.relations_matrix[t1] = {}
			for t2 in self.node_types:
				self.relations_matrix[t1][t2] = []
		
		for e in self.edges:
			for t1, tids1 in e.node_types_ids.items():
				for t2, tids2 in e.node_types_ids.items():
					if t1 == t2:
						continue
					if tids1 and tids2:
						self.relations_matrix[t1][t2].append(e.id)

	def get_empty_edge(self, edge_id):
		return Edge('edge_'+str(edge_id), self.node_types)

	def get_empty_node(self, node_id, name, type):
		return Node('node_'+str(node_id), name, type, self.node_types)

	def update_nodes_edges(self):
		for n in self.nodes:
			n.init_related(self.node_types)
		for e in self.edges:
			for nid in e.nodes_ids:
				node = self.get_node_id(nid)
				node.related_edges.append(e.id)
				node.add_related_nodes(e.node_types_ids)
		new_nodes = []
		for n in self.nodes:
			n_rels = n.get_n_complex_relation_nodes([])
			if n_rels > 0:
				new_nodes.append(n)
		self.nodes = new_nodes
		for n in self.nodes:
			if n.type == 'time':
				continue
			dates = list(n.related_nodes['time'])
			dates.sort()
			n.first_appearance = self.get_node_id(dates[0]).name

	##############################################################################
	## GETTERS AND SETTERS ##
	##############################################################################
	def get_node_id(self, _id):
		try:
			return next(x for x in self.nodes if x.id == _id)
		except:
			return None

	def get_node_name(self, name):
		try:
			return next(x for x in self.nodes if x.name == name)
		except:
			return None

	def get_node_name_type(self, name, node_type):
		try:
			return next(x for x in self.nodes if x.name == name and x.type == node_type)
		except:
			return None	

	def get_node_index(self, node):
		for i, n in enumerate(self.nodes):
			if n.id == node.id:
				return i
		return -1
	
	def get_edge_id(self, _id):
		try:
			return next(x for x in self.edges if x.id == _id)
		except:
			return None
	
	def get_edge_ids(self, nodes_ids):
		ids = set(nodes_ids)
		for e in self.edges:
			if set(e.nodes_ids) == ids:
				return e
		return None

	def get_edge_index(self, edge):
		for i, e in enumerate(self.edges):
			if e.id == edge.id:
				return i
		return -1

	def set_node_name(self, node_id, new_name):
		n = self.get_node_id(node_id)
		n.name = new_name

	##############################################################################
	## EDGES MODIFICATIONS ##
	##############################################################################
	def add_edges(self, to_add):
		id_edge = 0
		for e in self.edges:
			id_edge = max(id_edge, int(e.id))
		id_edge += 1

		for edge_nodes in to_add:
			new_edge = Edge(str(id_edge), self.node_types)
			for n_id in edge_nodes:
				node = self.get_node_id(n_id)
				new_edge.nodes_ids.append(node.id)
				new_edge.node_types_ids[node.type].append(node.id)
			self.edges.append(new_edge)
			id_edge += 1
		self.update_nodes_edges()

	def remove_edges(self, to_remove):
		for edge_id in to_remove:
			edge = self.get_edge_id(edge_id)
			index = self.get_edge_index(edge)
			del self.edges[index]
		self.update_nodes_edges()	

	def add_nodes_to_edges(self, edges_nodes):
		for edge_id, nodes_ids in edges_nodes.items():
			edge = self.get_edge_id(edge_id)
			for n_id in nodes_ids:
				node = self.get_node_id(n_id)
				edge.nodes_ids.append(node.id)
				edge.node_types_ids[node.type].append(node.id)
		self.update_nodes_edges()	

	def remove_nodes_to_edge(self, edge_id, node_type, left = 0):
		edge = self.get_edge_id(edge_id)
		n_ids = edge.node_types_ids[node_type]
		n_ids = n_ids[0:-left]
		for n_id in n_ids:
			edge.nodes_ids.remove(n_id)
		if left == 0:
			edge.node_types_ids[node_type] = []
		else:
			edge.node_types_ids[node_type] = edge.node_types_ids[node_type][-left:]
		self.update_nodes_edges()

	def replace_nodes_from_edge(self, edge_id, node_old_id, node_new_id):
		edge = self.get_edge_id(edge_id)
		node_old = self.get_node_id(node_old_id)
		node_new = self.get_node_id(node_new_id)
		edge.nodes_ids.remove(node_old_id)
		edge.node_types_ids[node_old.type].remove(node_old_id)
		edge.nodes_ids.append(node_new_id)
		edge.node_types_ids[node_new.type].append(node_new_id)
		self.update_nodes_edges()

	def remove_type_all_edges(self, node_type):
		for edge in self.edges:
			n_ids = edge.node_types_ids[node_type]
			for n_id in n_ids:
				edge.nodes_ids.remove(n_id)
			edge.node_types_ids[node_type] = []
		self.update_nodes_edges()

	def remove_nodes(self, to_remove):
		for node_id in to_remove:
			node = self.get_node_id(node_id)
			for edge_id in node.related_edges:
				edge = self.get_edge_id(edge_id)
				if edge is None:
					continue
				edge.nodes_ids.remove(node_id)
				edge.node_types_ids[node.type].remove(node_id)
				if len(edge.node_types_ids['time']) == 0:
					index = self.get_edge_index(edge)
					del self.edges[index]
		self.update_nodes_edges()

	##############################################################################
	## SORTING AND FILTERING ##	
	##############################################################################
	def sort_nodes(self, exceptions_types):
		sorted_nodes = sorted(self.nodes, key=lambda x: x.get_n_complex_relation_nodes(exceptions_types), reverse = True)
		self.sorted = sorted_nodes
		self.nodes = sorted_nodes
		
	def filter_nodes(self, num, exceptions_types, max_same_n_relations):
		self.sort_nodes(exceptions_types)	
		# Version for preserving time entities
		new_nodes = []
		new_nodes_ids = []
		count = 0
		for n in self.nodes:
			if self.get_n_same_related_nodes(n) > max_same_n_relations:
				continue
			if count < num:
				new_nodes.append(n)
				new_nodes_ids.append(n.id)
				count +=1
		# Add missing exceptions
		extra_new_nodes = []
		extra_new_nodes_ids = []
		for a_new_node in new_nodes:
			for an_et in exceptions_types:
				for possible_new in a_new_node.related_nodes[an_et]:
					if possible_new not in new_nodes_ids:
						another_new_node = self.get_node_id(possible_new)
						extra_new_nodes.append(another_new_node)
						extra_new_nodes_ids.append(possible_new)
						new_nodes_ids.append(possible_new)
		new_nodes.extend(extra_new_nodes)
		self.nodes = new_nodes
		self.nodes_ids = new_nodes_ids        
		self.filter_edges(exceptions_types)
		self.update_nodes_edges()
		
	def filter_edges(self, exceptions_types):
		new_edges = []
		for edge in self.edges:
			new_ids = list(filter(lambda x: x in self.nodes_ids, edge.nodes_ids))
			if len(new_ids) == 0:
				continue
			new_nodes = []
			for nid in new_ids:
				node = self.get_node_id(nid)
				new_nodes.append(node)
			edge.set_links(new_nodes)
			if edge.get_n_nodes(exceptions_types) != 0:
				new_edges.append(edge)
		self.edges = new_edges
		
	def update_node_related_info(self, node):
		for t, tids in node.related_nodes:
			new_ids = list(filter(lambda x: x in self.nodes_ids, tids))
			node.related_nodes[t] = tids			

	##############################################################################
	## MIRRORING ##
	##############################################################################
	def get_mirror(self):
		new_g = copy.deepcopy(self)
		dates = []
		for n in new_g.nodes:
			if n.type == 'time':
				dates.append(n.name)
		dates = list(set(dates))
		dates.sort()
		last = len(dates) - 1
		for n in new_g.nodes:
			if n.type == 'time':
				index = dates.index(n.name)
				n.name = dates[last - index]
			index = dates.index(n.first_appearance)
			new_fa = dates[last - index]
			n.first_appearance = new_fa
		return new_g

	##############################################################################
	## IMPORT & EXPORT ##
	##############################################################################
	def export_to_json(self, file_name):
		data = {}
		data['name'] = self.name        
		data['dimensions'] = self.dims
		data['nodes'] = copy.deepcopy(self.nodes)
		# Ugly hack to flatten sets
		for n in data['nodes']:
			for t, tids in n.related_nodes.items():
				n.related_nodes[t] = list(tids)
		data['edges'] = self.edges
		data['relations_matrix'] = self.relations_matrix
		data_json = jsonpickle.encode(data)
		with open(file_name, 'w') as outfile:
			outfile.write(data_json)

	@staticmethod
	def import_from_json(filename):		
		with open(filename) as f:
			data = json.load(f)
		g = Graph(data['dimensions'], filename)

		for n in data['nodes']:
			node = Node(n['id'], n['name'], n['type'], g.node_types)
			node.first_appearance = n['first_appearance']
			g.nodes.append(node)
			g.nodes_ids.append(node.id)

		for e in data['edges']:
			edge = Edge(e['id'], g.node_types)
			edge.sources = e['sources']
			edge.nodes_ids = e['nodes_ids']
			edge.node_types_ids = e['node_types_ids']
			g.edges.append(edge)

		g.update_nodes_edges()
		g.build_relation_matrix()
		return g
	
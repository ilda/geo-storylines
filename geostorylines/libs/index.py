################################################################
# Class to index text
# 2021 Vanessa Pena-Araya
################################################################

from datetime import datetime
from elasticsearch import Elasticsearch

class Index:
	def __init__(self, index_name):
		self.es = Elasticsearch()
		self.index_name = index_name

	def create(self):
		self.es.indices.create(index=self.index_name, ignore=400)

	def delete(self):
		self.es.indices.delete(index=self.index_name, ignore=[400, 404])

	def insert(self, source_id, text):
		doc = {}
		doc['source_id'] = source_id
		doc['text'] = text
		self.es.index(index=self.index_name, body=doc)

	def search(self, term):
		sources_ids = []
		body = {}
		body['query'] = {'match':{'text': {'query': term}}}
		res= self.es.search(index=self.index_name, body=body, size=10000)
		for hit in res['hits']['hits']:
			sources_ids.append(hit['_source']['source_id'])
		return sources_ids

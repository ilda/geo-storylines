# Geo-Storylines

![](repo_media/3_most_promising.png)

## Associated publication

- [DOI](https://doi.org/10.1109/TVCG.2022.3209480)
- [PDF @ HAL](https://hal.inria.fr/hal-03752409/document)
- [Pre-registration](https://osf.io/5wnyg)
- [Sup. material](https://ilda.saclay.inria.fr/geo-storylines/)

## Demo

A running version of the tool can be found [here](https://geostorylines.lisn.upsaclay.fr/).

## VIDEOS

### Overview of the project:

![](repo_media/companion-video.mp4)

### Demo of the study for each task used:

![](repo_media/geostorylines_5tasks.mp4)

### Presentation

![](repo_media/v-full_1180_Hulstein_Presentation.mp4)

## Installing  

Below are the instructions to install the tool using Django in a virtual enviroment. Check out our [dockerise branch](https://gitlab.inria.fr/ilda/geo-storylines/-/tree/dockerise) if you want to install it with Docker.

### Requirements

- Python 3
- pipenv (https://pipenv.pypa.io/en/latest/)


### 1.- Create the virtual environment

**To install**:

`pipenv install`

**To enter in the virtual environment**

`pipenv shell`

### 2.- Create default database (so Django doesn't complain)

`python manage.py makemigrations`
`python manage.py migrate`


### 3.- Collect static files (including datasets)

`python manage.py collectstatic`

### 4.- To run the server

`python manage.py runserver`

And then go to: http://localhost:8000/


## Dataset generation

1. In the main folder (geo-hyperstorylines-dev) execute `pipenv shell` to activate the virtual environment. 
2. Enter to the folder (`cd dataset_generation/`) and start Jupyter lab (`jupyter lab`)In particular:
    - 1.- Create map 3 Provinces Netherlands.ipynb -> just create the map of the 3 provinces of the Netherlands
    - 2.- Create map Rwanda & Burundi.ipynb -> create a map with the provinces of Rwanda and Burundi
    - 3.- Create example dataset.ipynb
    - 4.- Get trajectories from original articles dataset.ipynb -> just extract some datasets to see trajectories if the original iCoda dataset

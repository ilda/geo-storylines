# Faker is a Python library that generates fake data for testing or filling databases
from faker import Faker
from faker.providers.person.en import Provider
from random import shuffle,seed

class Anonimizer:
    
    def __init__(self, apply_shuffle=False):
        faker = Faker()
        person_names = []
        city_names = []
        seed(1)
        for i in range (2000):
            if(faker.city() not in city_names):
                city_names.append(faker.city())

        while len(person_names) < 10000:
            if(faker.name() not in person_names):
                person_names.append(faker.name())
                
        
        organizations_names = self.get_organization_fake_names()
        if apply_shuffle:
            shuffle(person_names)    # generate 10000 random person names (first name + last name)
            shuffle(city_names)      # generate 2000 random city names
            shuffle(organizations_names)

        self.fakepeopleNames = person_names#[0:10000]
        self.fakecityNames = city_names#[0:10000]
        self.fakeorgsNames = organizations_names
        
    def people_names (self):
        return self.fakepeopleNames

    def locations_names (self):
        return self.fakecityNames

    def organizations_names (self):
        return self.fakeorgsNames
        
    def anonymizeTime(self, aggregatingType, data): 
        for article in data:
            y = article["year"]
            m = article["month"]
            d = article["day"]
            if(aggregatingType == "Month"):
                metatime = 1999
                metatime = metatime + int(m)
                article["metatime"] = str(metatime)
            elif(aggregatingType == "Day"): 
                metatime = y + "-" + m + "-" + d
                article["metatime"] = str(metatime)
                
    def anonymizeNames(self, data):
        
        peopleNames = []
        locationNames = []
        organizationNames = []
        
        # get original names
        for article in data:  
            raw_pnames = article["people"]
            raw_lnames = article["locations"]
            #raw_onames = article["organizations"]
            for name in raw_pnames:
                if(name not in peopleNames):
                    peopleNames.append(name)
            for name in raw_lnames:
                if(name not in locationNames):
                    locationNames.append(name)
            #for name in raw_onames:
            #    if(name not in organizationNames):
            #        organizationNames.append(name)
        
        # mask names
        for article in data:
            # mask people names
            raw_pnames = article["people"]
            fake_pnames = []
            
            for name in raw_pnames:
                index1 = peopleNames.index(name)
                fake_pname = self.fakepeopleNames[index1]
                fake_pnames.append(fake_pname)    
            article["people"] = fake_pnames
  
            # mask location names
            raw_lnames = article["locations"]
            fake_lnames = []
            
            for name in raw_lnames:
                index2 = locationNames.index(name)
                fake_lname = self.fakecityNames[index2]
                fake_lnames.append(fake_lname)
            article["locations"] = fake_lnames

            # mask organization names
            #raw_onames = article["organizations"]
            #fake_onames = []
            
            #for name in raw_onames:
            #    index2 = organizationNames.index(name)
            #    fake_oname = self.fakeorgsNames[index2]
            #    fake_onames.append(fake_oname)
            #article["organizations"] = fake_onames
            
    def get_organization_fake_names(self):
        # List obtained from https://www.guidestar.org/ 
        # For some reason I missed someof them
        names = set()
        fileObj = open("organizations_long.txt", "r")
        for line in fileObj.readlines():
            names.add(line[:-1])
        fileObj.close()
        return list(names)
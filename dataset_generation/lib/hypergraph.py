################################################################
# Hypergraph class
# 2021 Vanessa Pena-Araya
################################################################

from lib.node import Node
from lib.edge import Edge
from lib.index import Index
import json
import jsonpickle
import copy
from datetime import datetime
from datetime import timedelta
import copy
from random import shuffle

class Hypergraph:

	####################
	## GRAPH BUILDING ##
	def __init__(self, dims, name):
		self.nodes = []
		self.nodes_ids = []
		self.edges = []
		self.dims = dims
		self.node_types = list(dims.keys())
		self.relations_matrix = {}
		self.name = name
		self.map_name = None

	def filter_simple_relationship(self, nodes_types):
		edges_to_remove = []

		# Get which edges to remove
		for edge in self.edges:
			n_complex_nodes = 0
			for c in nodes_types:
				n_complex_nodes += len(edge.node_types_ids[c])
			if n_complex_nodes <2:
				edges_to_remove.append(edge)

		# Remove the edges from edges list
		for edge in edges_to_remove:
			edge_index = self.get_edge_index(edge)
			del self.edges[edge_index]

		# Update internal relationships
		self.update_nodes_edges()
		
	def filter_small_relationships(self, n, nodes_types):
		edges_to_remove = []

		# Get which edges to remove
		for edge in self.edges:
			n_complex_nodes = 0
			for c in nodes_types:
				n_complex_nodes += len(edge.node_types_ids[c])
			if n_complex_nodes < n:
				edges_to_remove.append(edge)

		# Remove the edges from edges list
		for edge in edges_to_remove:
			edge_index = self.get_edge_index(edge)
			del self.edges[edge_index]

		# Update internal relationships
		self.update_nodes_edges()

	def filter_big_relationships(self, n, nodes_types):
		edges_to_remove = []

		# Get which edges to remove
		for edge in self.edges:
			n_complex_nodes = 0
			for c in nodes_types:
				n_complex_nodes += len(edge.node_types_ids[c])
			if n_complex_nodes >= n:
				edges_to_remove.append(edge)

		# Remove the edges from edges list
		for edge in edges_to_remove:
			edge_index = self.get_edge_index(edge)
			del self.edges[edge_index]

		# Update internal relationships
		self.update_nodes_edges()

	def filter_massive_events(self, node_type):
		edges_to_remove = []
		event_massive = None
		max_n_massive = 0

		# Get which edges to remove
		# I'm assuming that massive events is only related to people
		for edge in self.edges:
			n_massive = len(edge.node_types_ids[node_type])
			if n_massive > max_n_massive:
				max_n_massive = n_massive
				event_massive = edge.id

		for edge in self.edges:
			n_massive = len(edge.node_types_ids[node_type])
			if n_massive >= max_n_massive/2 and edge.id != event_massive:
				new_people = edge.node_types_ids[node_type]
				#shuffle(new_people)
				new_people = new_people[0:int(n_massive/4)]
				if len(new_people) >= 2:
					edge.node_types_ids[node_type] = new_people

		# Update internal relationships
		self.update_nodes_edges()
				
	def build_relation_matrix(self):
		self.relations_matrix = {}
		for t1 in self.node_types:
			self.relations_matrix[t1] = {}
			for t2 in self.node_types:
				self.relations_matrix[t1][t2] = []
		
		for e in self.edges:
			for t1, tids1 in e.node_types_ids.items():
				for t2, tids2 in e.node_types_ids.items():
					if t1 == t2:
						continue
					if tids1 and tids2:
						self.relations_matrix[t1][t2].append(e.id)

	def remove_edges(self, to_remove):
		for edge_id in to_remove:
			edge = self.get_edge_id(edge_id)
			index = self.get_edge_index(edge)
			del self.edges[index]
		self.update_nodes_edges()

	def add_edges(self, to_add):
		id_edge = 0
		for e in self.edges:
			id_edge = max(id_edge, int(e.id))
		id_edge += 1

		for edge_nodes in to_add:
			new_edge = Edge(str(id_edge), self.node_types)
			for n_id in edge_nodes:
				node = self.get_node_id(n_id)
				new_edge.nodes_ids.append(node.id)
				new_edge.node_types_ids[node.type].append(node.id)
			self.edges.append(new_edge)
			id_edge += 1
		self.update_nodes_edges()

	def add_nodes_to_edges(self, edges_nodes):
		for edge_id, nodes_ids in edges_nodes.items():
			edge = self.get_edge_id(edge_id)
			for n_id in nodes_ids:
				node = self.get_node_id(n_id)
				edge.nodes_ids.append(node.id)
				edge.node_types_ids[node.type].append(node.id)
		self.update_nodes_edges()

	def add_new_node(self, node):
		self.nodes.append(node)
		self.nodes_ids.append(node.id)
  
	def add_or_update_new_edge(self, edge, pub_id):
		existing_edge = self.get_edge_ids(edge.nodes_ids)
		if existing_edge is None:
			self.edges.append(edge)
			edge.sources.append(str(pub_id))
			
			# Update nodes related info
			for n_id in edge.nodes_ids:
				node = self.get_node_id(n_id)
				node.related_edges.append(edge.id)
			node.add_related_nodes(edge.node_types_ids)
		else:
			existing_edge.sources.append(str(pub_id))

		return existing_edge

	def remove_nodes_to_edge(self, edge_id, node_type, left = 0):
		edge = self.get_edge_id(edge_id)
		n_ids = edge.node_types_ids[node_type]
		n_ids = n_ids[0:-left]
		for n_id in n_ids:
			edge.nodes_ids.remove(n_id)
		if left == 0:
			edge.node_types_ids[node_type] = []
		else:
			edge.node_types_ids[node_type] = edge.node_types_ids[node_type][-left:]
		self.update_nodes_edges()

	def replace_nodes_from_edge(self, edge_id, node_old_id, node_new_id, update = True):
		edge = self.get_edge_id(edge_id)
		node_old = self.get_node_id(node_old_id)
		node_new = self.get_node_id(node_new_id)
		edge.nodes_ids = list(filter((node_old_id).__ne__, edge.nodes_ids))
		edge.node_types_ids[node_old.type] = list(filter((node_old_id).__ne__, edge.node_types_ids[node_old.type])) 
		edge.nodes_ids.append(node_new_id)
		edge.node_types_ids[node_new.type].append(node_new_id)
		if update:
			self.update_nodes_edges()

	def remove_type_all_edges(self, node_type):
		for edge in self.edges:
			n_ids = edge.node_types_ids[node_type]
			for n_id in n_ids:
				edge.nodes_ids.remove(n_id)
			edge.node_types_ids[node_type] = []
		self.update_nodes_edges()

	def remove_nodes(self, to_remove):
		for node_id in to_remove:
			node = self.get_node_id(node_id)
			for edge_id in node.related_edges:
				edge = self.get_edge_id(edge_id)
				if edge is None:
					continue
				edge.nodes_ids.remove(node_id)
				edge.node_types_ids[node.type].remove(node_id)
				if len(edge.node_types_ids['time']) == 0:
					print('TO REMOVE')
					print(node_id)
					index = self.get_edge_index(edge)
					del self.edges[index]
			node_index = self.get_node_index(node)
			self.nodes.pop(node_index)
			self.nodes_ids.remove(node_id)
		self.update_nodes_edges()

	def remove_island_edges(self, n):
		edges_to_remove = []
		for edge in self.edges:
			target_nodes = 0
			for n_id in edge.nodes_ids:
				node = self.get_node_id(n_id)
				if len(node.related_edges) == n:
					target_nodes += 1
			if len(edge.nodes_ids) > 6 and target_nodes >= len(edge.nodes_ids)/2:
				edges_to_remove.append(edge.id)
		print(edges_to_remove)
		self.remove_edges(edges_to_remove)


	#########################
	## GETTERS AND SETTERS ##

	def get_node_id(self, _id):
		try:
			return next(x for x in self.nodes if x.id == _id)
		except:
			return None

	def get_node_name(self, name):
		try:
			return next(x for x in self.nodes if x.name == name)
		except:
			return None

	def get_node_name_type(self, name, node_type):
		try:
			return next(x for x in self.nodes if x.name == name and x.type == node_type)
		except:
			return None	

	def get_node_index(self, node):
		for i, n in enumerate(self.nodes):
			if n.id == node.id:
				return i
		return -1
	
	def get_edge_id(self, _id):
		try:
			return next(x for x in self.edges if x.id == _id)
		except:
			return None
	
	def get_edge_ids(self, nodes_ids):
		ids = set(nodes_ids)
		for e in self.edges:
			if set(e.nodes_ids) == ids:
				return e
		return None

	def get_edge_index(self, edge):
		for i, e in enumerate(self.edges):
			if e.id == edge.id:
				return i
		return -1
	
	# This name sucks, change some day
	def get_n_same_related_nodes(self, node):
		max_rels = 0
		for e in node.related_edges:
			an_edge = self.get_edge_id(e)
			n_rels = len(an_edge.node_types_ids[node.type])
			max_rels = max(max_rels, n_rels)
		return max_rels

	def set_node_name(self, node_id, new_name):
		n = self.get_node_id(node_id)
		n.name = new_name

	def get_empty_edge(self, edge_id):
		return Edge('edge_'+str(edge_id), self.node_types)

	def get_empty_node(self, node_id, name, type):
		return Node('node_'+str(node_id), name, type, self.node_types)
		
	def get_last_node_id(self):
		last_id = 0
		for n in self.nodes:
			number_id = int(n.id[5:])
			if number_id > last_id:
				last_id = number_id
		return last_id
		
	def set_map(self, map_name):
		self.map_name = map_name
	

	###########################
	## SORTING AND FILTERING ##
	
	def sort_nodes(self, exceptions_types):
		sorted_nodes = sorted(self.nodes, key=lambda x: x.get_n_complex_relation_nodes(exceptions_types), reverse = True)
		self.sorted = sorted_nodes
		self.nodes = sorted_nodes
		
	def filter_nodes(self, num, exceptions_types, max_same_n_relations):
		self.sort_nodes(exceptions_types)
	
		# Version for preserving time entities
		new_nodes = []
		new_nodes_ids = []
		count = 0
		for n in self.nodes:
			if self.get_n_same_related_nodes(n) > max_same_n_relations:
				continue
			if count < num:
				new_nodes.append(n)
				new_nodes_ids.append(n.id)
				count +=1
		# Add missing exceptions
		extra_new_nodes = []
		extra_new_nodes_ids = []
		for a_new_node in new_nodes:
			for an_et in exceptions_types:
				for possible_new in a_new_node.related_nodes[an_et]:
					if possible_new not in new_nodes_ids:
						another_new_node = self.get_node_id(possible_new)
						extra_new_nodes.append(another_new_node)
						extra_new_nodes_ids.append(possible_new)
						new_nodes_ids.append(possible_new)
		new_nodes.extend(extra_new_nodes)
		self.nodes = new_nodes
		self.nodes_ids = new_nodes_ids        
		self.filter_edges(exceptions_types)
		self.update_nodes_edges()
		
	def filter_edges(self, exceptions_types):
		new_edges = []
		for edge in self.edges:
			new_ids = list(filter(lambda x: x in self.nodes_ids, edge.nodes_ids))
			if len(new_ids) == 0:
				continue
			new_nodes = []
			for nid in new_ids:
				node = self.get_node_id(nid)
				new_nodes.append(node)
			edge.set_links(new_nodes)
			if edge.get_n_nodes(exceptions_types) != 0:
				new_edges.append(edge)
		self.edges = new_edges
		
	def update_nodes_edges(self):
		for n in self.nodes:
			n.init_related(self.node_types)
		for e in self.edges:
			for nid in e.nodes_ids:
				node = self.get_node_id(nid)
				node.related_edges.append(e.id)
				node.add_related_nodes(e.node_types_ids)
		new_nodes = []
		for n in self.nodes:
			n_rels = n.get_n_complex_relation_nodes([])
			if n_rels > 0 and len(n.related_edges) > 0:
				new_nodes.append(n)
		self.nodes = new_nodes
		self.nodes_ids = []
		for n in self.nodes:
			if n.type == 'time':
				continue
			dates = list(n.related_nodes['time'])
			dates.sort()
			n.first_appearance = self.get_node_id(dates[0]).name
			self.nodes_ids.append(n.id)
		
	def update_node_related_info(self, node):
		for t, tids in node.related_nodes:
			new_ids = list(filter(lambda x: x in self.nodes_ids, tids))
			node.related_nodes[t] = tids
			

	###############
	## MIRRORING ##
	def get_mirror(self):
		new_g = copy.deepcopy(self)
		dates = []
		for n in new_g.nodes:
			if n.type == 'time':
				dates.append(n.name)
		dates = list(set(dates))
		dates.sort()
		last = len(dates) - 1
		for n in new_g.nodes:
			if n.type == 'time':
				index = dates.index(n.name)
				n.name = dates[last - index]
			index = dates.index(n.first_appearance)
			new_fa = dates[last - index]
			n.first_appearance = new_fa
		return new_g

	############
	## IMPORT & EXPORT ##
	def export_to_json(self, file_name):
		data = {}
		data['name'] = self.name        
		data['dimensions'] = self.dims
		data['nodes'] = copy.deepcopy(self.nodes)
		if self.map_name:
			data['mapName'] = self.map_name
		# Ugly hack to flatten sets
		for n in data['nodes']:
			for t, tids in n.related_nodes.items():
				n.related_nodes[t] = list(tids)
		data['edges'] = self.edges
		data['relations_matrix'] = self.relations_matrix
		data_json = jsonpickle.encode(data)
		with open(file_name, 'w') as outfile:
			outfile.write(data_json)

	@staticmethod
	def import_from_json(filename):		
		with open(filename) as f:
			data = json.load(f)
		g = Hypergraph(data['dimensions'], filename)

		for n in data['nodes']:
			node = Node(n['id'], n['name'], n['type'], g.node_types)
			node.first_appearance = n['first_appearance']
			g.nodes.append(node)
			g.nodes_ids.append(node.id)

		for e in data['edges']:
			edge = Edge(e['id'], g.node_types)
			edge.sources = e['sources']
			edge.nodes_ids = e['nodes_ids']
			edge.node_types_ids = e['node_types_ids']
			g.edges.append(edge)

		g.update_nodes_edges()
		g.build_relation_matrix()
		return g


	###########################
	## PAOHVIS COMPATIBILITY ##	
	def export_to_paohvis(self, file_name, meta_tstart, meta_tend, aggregatingType):
		# METADATA
		jsondata = {}
		jsondata["metadata"] = {
			"name": "Person%20OR%20Location",
			"ts_start": meta_tstart,
			"ts_end": meta_tend,
			"ts_step": "0001/00/00",
			"hyperedge_meta": "publication",
			"node_meta": "author",
			"group_meta": "team",
			"ts_meta": aggregatingType
		}
		
		# NODES
		export_nodes = []
		communities = copy.deepcopy(self.node_types)
		communities.remove("time")
		for n in self.nodes:
			if n.type == "time":
				continue
			new_node = {}
			new_node["id"] = n.id
			new_node["name"] = n.name
			new_node["community"] = n.type
			new_node["data"] = []
			n_values = 0
			for n_edge_id in n.related_edges:
				n_edge = self.get_edge_id(n_edge_id)
				n_communities = 0
				for c in communities:
					n_communities += len(n_edge.node_types_ids[c])
				if n_communities <2:
					continue
				new_data = {}
				# ASSUMING EACH EDGE HAS ONLY ONE TIME
				metatime = n_edge.node_types_ids["time"][0]
				metatime = (self.get_node_id(metatime)).name
				new_data["ts"] = metatime
				new_data["value"] = [n_values]
				new_data["community"] = [n.type]
				new_node["data"].append(new_data)
				n_values += 1
			export_nodes.append(new_node)
		jsondata["nodes"] = export_nodes
		
		# EDGES
		export_edges = {}
		for e in self.edges:
			# ASSUMING EACH EDGE HAS ONLY ONE TIME
			edge_time = e.node_types_ids["time"][0]
			edge_time = self.get_node_id(edge_time)
			edge_time = edge_time.name

			n_communities = 0
			for c in communities:
				n_communities += len(e.node_types_ids[c])
			if n_communities < 2:
				continue

			if edge_time not in export_edges:
				export_edges[edge_time] = {}
				export_edges[edge_time]["ts"] = edge_time
				export_edges[edge_time]["list"] = []
			new_edge = {}
			new_edge["id"] = int(e.id)
			new_edge["w"] = 1
			new_edge["meta"] = {"name": "A Name"}
			new_edge["ids"] = []
			for c in communities:
				for node_id in e.node_types_ids[c]:
					new_edge["ids"].append(node_id)
			export_edges[edge_time]["list"].append(new_edge)
		jsondata["edges"] = list(export_edges.values())
		
		data_json = jsonpickle.encode(jsondata)
		with open(file_name, 'w') as outfile:
			outfile.write(data_json)

	@staticmethod
	def data_from_paohvis(filename):
		with open(filename) as f:
			data = json.load(f)
		nodes_dict = {}
		entity_types = ['time']
		for n in data['nodes']:
			n['community'] = n['community']
			nodes_dict[n['id']] = n
			if n['community'] not in entity_types:
				entity_types.append(n['community'])
		dims = {}
		for c in entity_types:
			if c == 'time':
				dims[c] = {}
				dims[c]['available'] = ['x', 'z']
				dims[c]['default'] = 'x'
			else:
				dims[c] = {}
				dims[c]['available'] = ['x', 'y', 'z']
				if c == 'people':
					dims[c]['default'] = 'y'
				else:
					dims[c]['default'] = 'z'

		entity_types = dims

		sources = []
		times_list = []
		for ts_edge in data['edges']:
			times_list.append(ts_edge['ts'])
		times_dict = Graph.time_compatibility(times_list)
		for ts_edge in data['edges']:
			for i, edge in enumerate(ts_edge['list']):
				pub = {}
				pub['publication_id'] = str(ts_edge) + "_" + str(i)
				for t in entity_types:
					pub[t] = []
				time = times_dict[ts_edge['ts']]
				pub['time'].append(time)
				for n in edge['ids']:
					if n not in nodes_dict:
						continue
					node = nodes_dict[n]
					pub[node['community']].append(node['name'])
				sources.append(pub)
		return (sources, entity_types)

	def time_compatibility(times_list):
		times_dict = {}
		times_list.sort()
		for index, time in enumerate(times_list):
			semester_raw = time[-2]
			print(semester_raw)
			semester = '-01'
			if semester_raw == 'I':
				semester = '-06'
			times_dict[time] = time[0:4] + semester + '-01'
		return times_dict

		
	
	

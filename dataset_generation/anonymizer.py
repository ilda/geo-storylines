import json
from faker import Faker
from faker.providers.person.en import Provider

from datetime import datetime
from random import randrange
from datetime import timedelta

def random_date(start, end):
    """
    This function will return a random datetime between two datetime 
    objects.
    """
    start = datetime.strptime(start, '%Y-%m-%d')
    end = datetime.strptime(end, '%Y-%m-%d')
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    new_date = start + timedelta(seconds=random_second)
    return new_date

def anonymize_dataset(dataset_name, map_name, locations_dict, node_A, node_B):
    #dataset_name = 'articles_node_36_and_node_610_1.json'
    #map_name = 'africa_2c_provinces_1.json'
    #locations_dict = small_locations
    #node_A = 'node_36'
    #node_B = 'node_610'

    with open('data/study_2021/' + dataset_name) as f:
        data = json.load(f)

    ###########################
    # Create new names
    faker = Faker()
    names = []
    names_to_ignore = ['Dr.', 'Mr.', 'Ms.', 'Mrs.']
    while len(names) < 10:
        a_name = faker.name()
        index = a_name.index(' ')
        a_name = a_name[0:index]
        if a_name not in names and a_name not in names_to_ignore:
            names.append(a_name)

    ###########################
    # Create new dates
    date_names = []
    for n in data['nodes']:
        if n['type'] == 'time':
            date_names.append(n['name'])
    date_names.sort()
    seed_date = random_date('2000-01-01','2021-08-17')

    new_dates = {}
    for i, date_name in enumerate(date_names):
        new_date = seed_date + timedelta(i)
        new_date = datetime.strftime(new_date, '%Y-%m-%d')
        new_dates[date_name] = new_date

    n_names = 0
    for n in data['nodes']:
        # Anonymize locations
        if n['type'] == 'locations':
            n['name'] = locations_dict[n['name']]
        # Anonymize names
        if n['type'] == 'people':
            n['name'] = names[n_names]
            n_names += 1
            if n['id'] == node_A:
                node_A_name = n['name']
            if n['id'] == node_B:
                node_B_name = n['name']
        if n['type'] == 'time':
            n['name'] = new_dates[n['name']]

    data['mapName'] = map_name

    with open('data/study_2021_annon/' + dataset_name, 'w') as f:
        json.dump(data, f)

    print('node_A = ' + node_A_name)
    print('node_B = ' + node_B_name)
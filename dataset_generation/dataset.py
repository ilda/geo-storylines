#!/usr/bin/env python
# coding: utf-8

import sys, os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from lib.hypergraph import Hypergraph 
import json
import random
from datetime import datetime
from datetime import timedelta

def create(map_name, trajectory, node_A, version, index_node_b, n_relations_a_b, n_relations_total, locations_to_ignore = []):

    #####################################################
    # GET INITIAL VALUES

    # 1.- GET LOCATIONS WITHIN AND OUT THE TRAJECTORY
    import geopandas as gpd
    map_data = gpd.read_file('maps/' + map_name)
    map_data = map_data[map_data['hierarchical_level'] == 'child']
    locations_all = set(map_data.name)
    locations_trajectory = set()
    for t_list in trajectory:
        for t in t_list:
            locations_trajectory.add(t)
    locations_no_trajectory = locations_all - locations_trajectory - set(locations_to_ignore)
    locations_no_trajectory = list(locations_no_trajectory)
    locations_trajectory = list(locations_trajectory)


    # 2.- GET NODE_B
    new_edges = []
    g2 = Hypergraph.import_from_json("./data/dataset_ori/all_dataset.json")
    most_related_with_A = {}
    types_to_inspect = ['people', 'locations', 'organizations']

    for e in g2.edges:
        if node_A in e.nodes_ids:
            for t in types_to_inspect:
                for n_id in e.node_types_ids[t]:
                    if n_id == node_A: continue
                    if n_id not in most_related_with_A:
                        most_related_with_A[n_id] = 0
                    most_related_with_A[n_id] += 1


    possible_B = {k: v for k, v in sorted(most_related_with_A.items(), key=lambda item: item[1], reverse=True)}
    node_B = list(possible_B.keys())[index_node_b]
    print('node_B is ' + node_B) # MAKE SURE IT'S THE SAME FOR THE 3 VERSIONS
    
    node_A_object = g2.get_node_id(node_A)
    print('Name of Node A is ' + node_A_object.name)
    
    node_B_object = g2.get_node_id(node_B)
    print('Name of Node B is ' + node_B_object.name)


    # 3.- Select the 10 relationships between A and B
    # 3.1.- SELECT ALL THE EDGES WITH THE LARGEST NUMBER OF NODES TO HAVE MORE CHANGES TO GET NODES WITH A LOT OF RELATED EDGES
    dates_edges = {}
    edge_n_nodes = {}

    for e in g2.edges:
        if node_A in e.nodes_ids and node_B in e.nodes_ids:
            edge_n_nodes[e.id] = len(e.nodes_ids) - 1 # ignore the date, although it's irrelevant

    most_related_edges = {k: v for k, v in sorted(edge_n_nodes.items(), key=lambda item: item[1], reverse=True)}
    
    # 3.2.- SELECT THE 10 THAT INCLUDE THE LARGEST NUMBER OF NODES
    n_relationships = 0
    all_edges = []
    all_dates = []

    for edge_id in most_related_edges:
        if n_relationships > n_relations_a_b - 1: break
        e = g2.get_edge_id(edge_id)
        date_id = e.node_types_ids["time"][0]
        date_node = g2.get_node_id(date_id)
        date = date_node.name
        if date not in dates_edges:
            dates_edges[date] = []
            all_dates.append(date)
        dates_edges[date].append(e)
        n_relationships += 1
        all_edges.append(edge_id)

    ordered_dates = sorted(dates_edges.keys())
    
    print('There are ' + str(n_relationships) + ' relationships')
    
    n_extra_dates = n_relations_total - len(all_dates)


    # 4.- ADD EXTRA EDGES UNTIL HAVING 35 TIME STEPS
    # 4.1.- GET THE OTHER NODES RELATED WITH A AND B
    other_nodes = {}
    all_other_edges = []
    for date in ordered_dates:
        edges = dates_edges[date]
        for e in edges:
            for n_id in e.nodes_ids:
                node = g2.get_node_id(n_id)
                if node.type == 'time': continue
                if n_id == node_A or n_id == node_B: continue
                if n_id not in other_nodes:
                    node = g2.get_node_id(n_id)
                    other_nodes[n_id] = node.related_edges
                    all_other_edges = all_other_edges + node.related_edges

    all_other_edges = list(set(all_other_edges))

    # 4.2.- RAISE AN ERROR IF I HAVE LESS THAN 25 RELATIONSHIPS
    if len(all_other_edges) < n_extra_dates:
        raise ValueError('THERE ARE NOT ENOUGH RELATIONSHIPS!')
        exit(0)


    # 4.3.- SELECT n_relations_a_b - 2 RANDOM NODES
    keys = list(other_nodes.keys())
    extra_8_nodes = {}
    all_other_edges = []
    n_nodes = 0

    import random
    
    # This is scary
    while True:
        if n_nodes > n_relations_a_b - 3: break
        r = random.choice(keys)
        if r not in extra_8_nodes:
            extra_8_nodes[r] = other_nodes[r]
            all_other_edges = all_other_edges + other_nodes[r]
            n_nodes += 1

    all_other_edges = list(set(all_other_edges))

    # 4.4.- RAISE AN ERROR IF I HAVE LESS THAN 25 RELATIONSHIPS FOR THE 8 SELECTED NODES
    if len(all_other_edges) < n_extra_dates:
        raise ValueError('THERE ARE NOT ENOUGH RELATIONSHIPS!')
        exit(0)


    # 4.5.- GET AT LEAST 25 MORE TIME STEPS. ONE RELATIONSHIP PER EXTRA DATE
    other_edges = {}
    while len(all_dates) < n_relations_total:
        for node_id in extra_8_nodes:
            node = g2.get_node_id(node_id)
            edges = node.related_edges
            if len(edges) == 0: continue
            r = random.choice(edges)
            if r not in all_edges:
                all_edges.append(r)
                edge = g2.get_edge_id(r)
                date_id = edge.node_types_ids["time"][0]
                date_node = g2.get_node_id(date_id)
                date = date_node.name
                other_edges[date] = edge
                if date not in all_dates:
                    all_dates.append(date)
                    if len(all_dates) == n_relations_total: break
            edges.remove(r)
            node.related_edges = edges

    all_dates.sort()
    
    #####################################################
    # CREATE DATASET

    # 1.- TRANSFORM ALL NODES TO BE PEOPLE
    for n in g2.nodes:
        if n.type != "time":
            n.type = "people"


    # 2.- Create a new node for each unique location in the trajectory
    new_nodes = []

    unique_locations = {}
    for t in trajectory:
        for location_name in t:
            # If I already create a node, I continue
            if location_name in unique_locations: continue        
            # Get the last id of the nodes in the hypergraph and add one to get a new id
            new_id = g2.get_last_node_id() + 1
            new_node = g2.get_empty_node(new_id, location_name, "locations")
            # Create a new node and add it to the hypergraph but don't link it with any edge
            g2.add_new_node(new_node)
            # dictionary to have an easy access to the node later
            unique_locations[location_name] = new_node

    all_nodes = []

    # 3.- ADD THE 10 BASE EDGES
    new_edges = []    
    relationship_index = 0

    for ord_index, date in enumerate(ordered_dates):

        # edge I'm working with
        edges = dates_edges[date]
        
        for e in edges:
            # as I impose that all my nodes will be of type people, 
            # I link all the nodes of type locations to the edge but
            # now to the type people. 
            # I ignore the nodes of types organizations...Just because. 
            # We can add them later if we want
            e.node_types_ids['people'] = e.node_types_ids['people'] + e.node_types_ids['locations'] + e.node_types_ids['organizations']
            all_nodes.extend(e.node_types_ids['people'])

            # I remove all the nodes of type locations to fill it with the ones of the trajectory
            e.node_types_ids['locations'] = []
            e.node_types_ids['organizations'] = []

            # Thisshould not be necessary
            if relationship_index < len(trajectory):    
                # I add each of the locations from the ith trajectory to the edge
                for location_name in trajectory[relationship_index]:
                    e.node_types_ids['locations'].append(unique_locations[location_name].id)
                    e.nodes_ids.append(unique_locations[location_name].id)
            relationship_index += 1
            new_edges.append(e)


    # 4.- ADD THE EXTRA EDGES
    nodes_extra_locations = {}
    
    # 4.1.- LOCS_CONFIG CONTAIN THE CONFIG OF LOCATIONS OF THE EXTRA EDGES
    locs_config = []
    n_max_extra_locs_per_node = 2
    n_max_extra_rels_per_node = 3
    
    # SELECT THE EXTRA LOCATIONS OF A OUTSIDE THE TRAJECTORY
    n_locs_extra_A = random.randint(1, n_max_extra_locs_per_node)
    locs_extra_A = random.sample(locations_no_trajectory, n_locs_extra_A)  
    n_rels_extra_A = random.randint(1, n_max_extra_rels_per_node)
    # ADD SOME EXTRA RELS FOR A
    for i in range(0, n_rels_extra_A):
        locs_config.append('A')
    
    # SELECT THE EXTRA LOCATIONS FOR B
    n_locs_extra_B = random.randint(1, n_max_extra_locs_per_node)
    locs_for_B = list(set(locations_no_trajectory) - set(locs_extra_A))
    locs_extra_B = random.sample(locs_for_B, n_locs_extra_B)
    # ADD SOME EXTRA RELS FOR B
    n_rels_extra_B = random.randint(1, n_max_extra_rels_per_node)
    for i in range(0, n_rels_extra_B):
        locs_config.append('B')
    
    # FOR THE REST: JUST SELECT A RANDOM NUMBER OF LOCATIONS
    n_extra_locs = len(list(other_edges.keys())) - len(locs_config)
    for i in range(0, n_extra_locs):
        n_locations = random.randint(0, 4)
        locs_config.append(n_locations)
    
    # JUST BECAUSE I CAN
    for i in range(0, 5):
        random.shuffle(locs_config)
        
    import random
    for relationship_index, date in enumerate(other_edges):
        e = other_edges[date]
        e.node_types_ids['people'] = e.node_types_ids['people'] + e.node_types_ids['locations'] + e.node_types_ids['organizations']
        all_nodes.extend(e.node_types_ids['people'])
        e.node_types_ids['locations'] = []
        e.node_types_ids['organizations'] = [] 

        # add a random number of locations if the edge does not include node_A and node_B
        if node_A not in e.node_types_ids['people'] and node_B not in e.node_types_ids['people']:
            config = locs_config[relationship_index]
            if not isinstance(config, int):
                # REMOVE THEM JUST IN CASE
                if node_A in e.node_types_ids['people']:
                    e.node_types_ids['people'].remove(node_A)
                    e.nodes_ids.remove(node_A)
                if node_B in e.node_types_ids['people']:
                    e.node_types_ids['people'].remove(node_B)
                    e.nodes_ids.remove(node_B)                    
                # FIRST: ADD THE NODE TO THE EDGE
                n_locations = random.randint(1, n_max_extra_locs_per_node)
                if config == 'A':
                    node = node_A
                    locs = random.sample(locs_extra_A, n_locs_extra_A)
                else:
                    node = node_B                
                    locs = random.sample(locs_extra_B, n_locs_extra_B)
                if node not in e.nodes_ids:
                    e.nodes_ids.append(node)
                    e.node_types_ids['people'].append(node)
                for loc_name in locs:
                    if loc_name not in unique_locations:
                        new_id = g2.get_last_node_id() + 1
                        new_node = g2.get_empty_node(new_id, loc_name, "locations")
                        g2.add_new_node(new_node)
                        unique_locations[loc_name] = new_node
                    loc_id = unique_locations[loc_name].id
                    e.node_types_ids['locations'].append(loc_id)
                    e.nodes_ids.append(loc_id)                
            else:
                n_locations = config
                for i in range(0, n_locations):
                    # if i is even, choose a location from the trajectory
                    if i%2 == 0:
                        # scary
                        while True:
                            loc_name = random.choice(locations_trajectory)
                            loc_id = unique_locations[loc_name].id
                            if loc_id not in e.node_types_ids['locations']: break
                    # if i is odd, choose a location that is not in the trajectory
                    else:
                        while True:
                            loc_name = random.choice(locations_no_trajectory)
                            if loc_name not in unique_locations:
                                new_id = g2.get_last_node_id() + 1
                                new_node = g2.get_empty_node(new_id, loc_name, "locations")
                                g2.add_new_node(new_node)
                                unique_locations[loc_name] = new_node
                            loc_id = unique_locations[loc_name].id
                            if loc_id not in e.node_types_ids['locations']: break
                    e.node_types_ids['locations'].append(loc_id)
                    e.nodes_ids.append(loc_id)
        new_edges.append(e)
        
    # 5.- ENSURE THAT THE FIRST RELATIONSHIP BETWEEN A AND B IS EITHER IN THE 1st, 2nd OR 3rd IN THE TIMELINE    
    dates_10 = list(dates_edges.keys())
    dates_10.sort()
    oldest_date_10 = dates_10[0]
    oldest_date_10_position = all_dates.index(oldest_date_10)
    oldest_date = all_dates[0]
    
    if oldest_date_10_position > 2:
        position = random.randint(0, 2)
        shift = position - oldest_date_10_position
        for d_10 in dates_10:
            new_pos = all_dates.index(d_10) + shift
            all_dates.remove(d_10)
            all_dates = all_dates[0:new_pos] + [d_10] + all_dates[new_pos:]
            
    for n in g2.nodes:
        name = n.name
        if n.type == 'time' and name in all_dates:
            position = all_dates.index(name)
            new_date = datetime.strptime(oldest_date, '%Y-%m-%d') + timedelta(days=position) 
            n.name = datetime.strftime(new_date, '%Y-%m-%d')    

    # 6.- ASSIGN NEW EDGES
    g2.edges = new_edges
    # next 2 lines create internal relationships between nodes and edges
    # Always execute after modifying the relationships between nodes and edges
    g2.update_nodes_edges()
    g2.build_relation_matrix()

    # 7.- REMOVE UNNECESARY NODES
    all_nodes = list(set(all_nodes))
    all_nodes.remove(node_A)
    all_nodes.remove(node_B)
    for nid in extra_8_nodes:
        if nid in all_nodes:
            all_nodes.remove(nid)
    g2.remove_nodes(all_nodes)

    # 8.- BUILD INTERNAL RELATIONSHIPS
    g2.update_nodes_edges()
    g2.build_relation_matrix()

    # 9.- ASSIGN THE MAP
    g2.set_map(map_name)
    
    # 10.- EXPORT THE DATASET
    g2.export_to_json("./data/study_2021/articles_" + node_A + "_and_" + node_B + "_" + str(version) + ".json")

def is_consistent(dataset, node_A, node_B):
    with open('data/study_2021/' + dataset) as f:
        data = json.load(f)
    for e in data['edges']:
        if node_A in e['nodes_ids'] and node_B in e['nodes_ids']:
            if len(e['node_types_ids']['locations']) == 0:
                return False
    return True